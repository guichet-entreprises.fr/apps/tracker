<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">

    <modelVersion>4.0.0</modelVersion>

    <parent>
        <groupId>fr.ge.core</groupId>
        <artifactId>java-pom</artifactId>
        <version>2.16.1.4</version>
        <relativePath>../..</relativePath>
    </parent>

    <groupId>fr.ge.common.tracker</groupId>
    <artifactId>ge-tracker</artifactId>
    <packaging>pom</packaging>
    <version>2.16.1.2-SNAPSHOT</version>

    <name>GE Tracker</name>

    <scm>
        <connection>scm:git:https://gitlab.com/guichet-entreprises.fr/apps/tracker.git</connection>
        <developerConnection>scm:git:https://gitlab.com/guichet-entreprises.fr/apps/tracker.git</developerConnection>
        <url>https://gitlab.com/guichet-entreprises.fr/apps/tracker</url>
        <tag>HEAD</tag>
    </scm>

    <developers>
        <developer>
            <id>jpauchet</id>
            <name>Jonathan Pauchet</name>
            <email>jonathan.pauchet AT capgemini.com</email>
            <organization>Capgemini</organization>
        </developer>
        <developer>
            <id>ccougourdan</id>
            <name>Christian Cougourdan</name>
            <email>christian.cougourdan AT capgemini.com</email>
            <organization>Capgemini</organization>
        </developer>
    </developers>

    <distributionManagement>
        <site>
            <id>tracker-site</id>
            <name>Tracker Site</name>
            <url>http://pic.projet-ge.fr/tracker</url>
        </site>
    </distributionManagement>

    <properties>
        <swagger-v3.version>2.1.1</swagger-v3.version>
        <common-utils.version>2.15.2.1</common-utils.version>
        <cxf.version>3.3.5</cxf.version>

        <css-target-dir>${project.build.directory}/${project.name}/css</css-target-dir>
        <webapp-target-dir>${project.build.directory}/${project.build.finalName}</webapp-target-dir>
        <liasse-services.version>2.7.4.0</liasse-services.version>
        <log.appender.root>thirdParty</log.appender.root>
        <log.appender.func>func</log.appender.func>
        <log.appender.tech>tech</log.appender.tech>
        <log.appender.thirdParty>thirdParty</log.appender.thirdParty>
        <jacoco.skip>true</jacoco.skip>
    </properties>

    <modules>
        <module>modules/tracker-core</module>
        <module>modules/tracker-client</module>
        <module>modules/tracker-core-data</module>
        <module>modules/tracker-urssaf-adapter</module>
        <module>modules/tracker-server</module>
    </modules>

    <dependencyManagement>
        <dependencies>
            <dependency>
                <groupId>fr.ge.common.utils</groupId>
                <artifactId>common-utils</artifactId>
                <version>${common-utils.version}</version>
            </dependency>
            <dependency>
                <groupId>fr.ge.common.utils</groupId>
                <artifactId>common-web-utils</artifactId>
                <version>${common-utils.version}</version>
            </dependency>
            <dependency>
                <groupId>fr.ge.common.utils</groupId>
                <artifactId>common-test-utils</artifactId>
                <version>${common-utils.version}</version>
                <scope>test</scope>
            </dependency>
            <dependency>
                <groupId>fr.ge.common.tracker</groupId>
                <artifactId>ge-tracker-client</artifactId>
                <version>${project.version}</version>
            </dependency>
            <dependency>
                <groupId>fr.ge.common.tracker</groupId>
                <artifactId>ge-tracker-core</artifactId>
                <version>${project.version}</version>
            </dependency>
            <dependency>
                <groupId>fr.ge.common.tracker</groupId>
                <artifactId>ge-tracker-core-data</artifactId>
                <version>${project.version}</version>
            </dependency>
            <dependency>
                <groupId>fr.ge.common.tracker</groupId>
                <artifactId>ge-tracker-server</artifactId>
                <version>${project.version}</version>
            </dependency>
            <dependency>
                <groupId>fr.ge.common.tracker</groupId>
                <artifactId>ge-tracker-urssaf-adapter</artifactId>
                <version>${project.version}</version>
            </dependency>
            <dependency>
                <groupId>fr.ge.common.liasse</groupId>
                <artifactId>liasse-services</artifactId>
                <version>${liasse-services.version}</version>
            </dependency>
            <dependency>
                <groupId>fr.ge.common.liasse</groupId>
                <artifactId>liasse-services</artifactId>
                <type>tar.gz</type>
                <classifier>bdd</classifier>
                <scope>compile</scope>
                <version>${liasse-services.version}</version>
            </dependency>

            <dependency>
                <groupId>io.swagger.core.v3</groupId>
                <artifactId>swagger-core</artifactId>
                <version>${swagger-v3.version}</version>
            </dependency>
            <dependency>
                <groupId>io.swagger.core.v3</groupId>
                <artifactId>swagger-jaxrs2</artifactId>
                <version>${swagger-v3.version}</version>
            </dependency>
            <dependency>
                <groupId>io.swagger.core.v3</groupId>
                <artifactId>swagger-annotations</artifactId>
                <version>${swagger-v3.version}</version>
            </dependency>
        </dependencies>
    </dependencyManagement>

    <build>
        <pluginManagement>
            <plugins>
                <plugin>
                    <groupId>org.apache.maven.plugins</groupId>
                    <artifactId>maven-surefire-plugin</artifactId>
                    <version>2.22.2</version>
					<configuration>
						<argLine>-Dfile.encoding=${project.build.sourceEncoding} -Duser.language=fr -Duser.region=FR -Duser.timezone=Europe/Paris ${surefireArgLine}</argLine>
						<useSystemClassLoader>false</useSystemClassLoader>
						<useManifestOnlyJar>true</useManifestOnlyJar>
						<trimStackTrace>false</trimStackTrace>
					</configuration>
                </plugin>
                <plugin>
                    <groupId>org.apache.maven.plugins</groupId>
                    <artifactId>maven-javadoc-plugin</artifactId>
                </plugin>
                <plugin>
                    <groupId>org.codehaus.mojo</groupId>
                    <artifactId>cobertura-maven-plugin</artifactId>
                </plugin>
                <!-- <plugin> <groupId>org.apache.maven.plugins</groupId> <artifactId>maven-javadoc-plugin</artifactId> 
                    <version>2.10.3</version> <configuration> <includes>src/main/java/**/*.java</includes> 
                    <doclet>ch.raffael.doclets.pegdown.PegdownDoclet</doclet> <docletArtifact> 
                    <groupId>ch.raffael.pegdown-doclet</groupId> <artifactId>pegdown-doclet</artifactId> 
                    <version>1.3</version> </docletArtifact> <useStandardDocletOptions>true</useStandardDocletOptions> 
                    </configuration> </plugin> -->
                <plugin>
                    <groupId>org.apache.maven.plugins</groupId>
                    <artifactId>maven-war-plugin</artifactId>
                    <configuration>
                        <archive>
                            <manifest>
                                <addDefaultImplementationEntries>true</addDefaultImplementationEntries>
                                <addDefaultSpecificationEntries>true</addDefaultSpecificationEntries>
                            </manifest>
                            <manifestEntries>
                                <Version>${env.SVN_REVISION}</Version>
                                <Build-Date>${timestamp}</Build-Date>
                            </manifestEntries>
                        </archive>
                    </configuration>
                </plugin>
                <plugin>
                    <groupId>io.swagger.core.v3</groupId>
                    <artifactId>swagger-maven-plugin</artifactId>
                    <version>${swagger-v3.version}</version>
                </plugin>
            </plugins>
        </pluginManagement>
        <plugins>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-javadoc-plugin</artifactId>
                <configuration>
                    <skip>true</skip>
                </configuration>
            </plugin>
            <plugin>
                <groupId>org.codehaus.mojo</groupId>
                <artifactId>cobertura-maven-plugin</artifactId>
                <configuration>
                    <skip>true</skip>
                </configuration>
            </plugin>
            <plugin>
                <groupId>com.github.jeluard</groupId>
                <artifactId>plantuml-maven-plugin</artifactId>
                <configuration>
                    <sourceFiles>
                        <directory>${basedir}/src/site/uml</directory>
                        <includes>
                            <include>**/*.uml</include>
                        </includes>
                    </sourceFiles>
                    <outputDirectory>${project.build.directory}/site/images/uml</outputDirectory>
                </configuration>
                <executions>
                    <execution>
                        <id>uml-generate</id>
                        <phase>pre-site</phase>
                        <goals>
                            <goal>generate</goal>
                        </goals>
                    </execution>
                </executions>
                <dependencies>
                    <dependency>
                        <groupId>net.sourceforge.plantuml</groupId>
                        <artifactId>plantuml</artifactId>
                        <version>8043</version>
                    </dependency>
                </dependencies>
            </plugin>
        </plugins>
    </build>

    <profiles>
        <profile>
            <id>default</id>
            <activation>
                <activeByDefault>true</activeByDefault>
            </activation>
            <properties>
                <conf.jndi.db.datasource>@jndi.db.datasource@</conf.jndi.db.datasource>
                <conf.hibernate.dialect>org.hibernate.dialect.PostgreSQL82Dialect</conf.hibernate.dialect>
                <conf.hibernate.hbm2ddl.auto />
                <conf.hibernate.show_sql>false</conf.hibernate.show_sql>
            </properties>
        </profile>
        <profile>
            <id>sonar</id>
            <properties>
                <sonar.coverage.exclusions>**/bean/**,**/*Bean.java,**/formiz/**,**/*DataInit.java,**/*DataExport.java,**/com/dictao/**/*.java,
                    target/**</sonar.coverage.exclusions>
                <sonar.jdbc.url>jdbc:mysql://localhost:3306/sonar?useUnicode=true&amp;characterEncoding=utf8</sonar.jdbc.url>
                <sonar.jdbc.driver>com.mysql.jdbc.Driver</sonar.jdbc.driver>
                <sonar.jdbc.username>sonar</sonar.jdbc.username>
                <sonar.jdbc.password>SONAR_PASSWORD</sonar.jdbc.password>
            </properties>
        </profile>
        <profile>
            <id>m2e</id>
            <activation>
                <property>
                    <name>m2e.version</name>
                </property>
            </activation>
            <build>
                <resources>
                    <resource>
                        <filtering>false</filtering>
                        <directory>src/main/resources</directory>
                    </resource>
                    <resource>
                        <filtering>true</filtering>
                        <directory>src/main/config</directory>
                    </resource>
                </resources>
            </build>
            <properties>
                <css-target-dir>${project.build.directory}/m2e-wtp/web-resources/css</css-target-dir>
                <webapp-target-dir>${project.build.directory}/m2e-wtp/web-resources</webapp-target-dir>

                <conf.jndi.db.datasource>jdbc/tracker</conf.jndi.db.datasource>

                <!-- <conf.hibernate.dialect>org.hibernate.dialect.H2Dialect</conf.hibernate.dialect> -->
                <conf.hibernate.dialect>org.hibernate.dialect.PostgreSQL82Dialect</conf.hibernate.dialect>
                <conf.hibernate.hbm2ddl.auto />
                <conf.hibernate.show_sql>true</conf.hibernate.show_sql>
                <conf.url.service.tracker>http://localhost:9080/tracker-server/api/v1</conf.url.service.tracker>
                <conf.mas.tracker.service.url>http://localhost:9080/tracker-server/api</conf.mas.tracker.service.url>

                <log.file.technical>${project.build.directory}/logs/tech.log</log.file.technical>
                <log.file.functional>${project.build.directory}/logs/func.log</log.file.functional>
                <log.file.thirdParty>${project.build.directory}/logs/thirdParty.log</log.file.thirdParty>
                <log.max_size>1 MB</log.max_size>
                <log.max_index>20</log.max_index>
                <log.level>warn</log.level>
                <log.level_fwk>warn</log.level_fwk>
                <log.level_app>debug</log.level_app>

                <log.appender.root>console</log.appender.root>
                <log.appender.func>console</log.appender.func>
                <log.appender.tech>console</log.appender.tech>
                <log.appender.thirdParty>console</log.appender.thirdParty>
            </properties>
        </profile>
    </profiles>

</project>
