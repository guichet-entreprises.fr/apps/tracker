# Configurer la datasource (JNDI)

- Mettre postgresql-9.4.1208.jar (la même version que dans les dépendances Maven) dans le répertoire "lib" de Tomcat.

- Mettre la dataSource dans le context.xml de Tomcat :

	<Resource name="jdbc/tracker"
		auth="Container"
		type="javax.sql.DataSource"
		username="tracker_usr"
		password="tracker_pwd"
		url="jdbc:postgresql://localhost:5432/tracker"
		driverClassName="org.postgresql.Driver"
		initialSize="20"
		maxWaitMillis="15000"
		maxTotal="75"
		maxIdle="20"
		maxAge="7200000"
		testOnBorrow="true"
		validationQuery="select 1"
	/>
