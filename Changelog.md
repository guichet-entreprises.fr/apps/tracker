# Changelog

Ce fichier contient les modifications techniques du module.

- Projet : [Tracker](https://tools.projet-ge.fr/gitlab/minecraft/ge-tracker)


## [2.12.5.1] - 2019-11-25

Mise à disposition des API classées "managed"

### Ajout 

- Nouveau fichier _Changelog.md_


## [2.11.2.0] - 2019-05-06


### Ajout 

- Nouveau fichier _Changelog.md_

###  Evolution
- rework des message tracker : DEST-805

