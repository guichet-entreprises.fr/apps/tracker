package fr.ge.tracker.ws.v2.model;

import java.time.Instant;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;

/**
 *
 * @author Christian Cougourdan
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class Message {

    private String key;

    private String user;

    private Instant created;

    private String content;

    /**
     * @return the key
     */
    public String getKey() {
        return this.key;
    }

    /**
     * @param key
     *            the key to set
     * @return current instance
     */
    public Message setKey(final String key) {
        this.key = key;
        return this;
    }

    /**
     * @return the user
     */
    public String getUser() {
        return this.user;
    }

    /**
     * @param user
     *            the user to set
     * @return current instance
     */
    public Message setUser(final String user) {
        this.user = user;
        return this;
    }

    /**
     * @return the created
     */
    public Instant getCreated() {
        return this.created;
    }

    /**
     * @param created
     *            the created to set
     * @return current instance
     */
    public Message setCreated(final Instant created) {
        this.created = created;
        return this;
    }

    /**
     * @return the content
     */
    public String getContent() {
        return this.content;
    }

    /**
     * @param content
     *            the content to set
     * @return current instance
     */
    public Message setContent(final String content) {
        this.content = content;
        return this;
    }

}
