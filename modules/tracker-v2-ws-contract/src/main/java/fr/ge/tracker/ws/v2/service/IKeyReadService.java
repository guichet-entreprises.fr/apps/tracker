package fr.ge.tracker.ws.v2.service;

import java.util.List;

import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import fr.ge.common.utils.bean.search.SearchQuery;
import fr.ge.common.utils.bean.search.SearchQueryOrder;
import fr.ge.common.utils.bean.search.SearchResult;
import fr.ge.tracker.ws.v2.model.KeyInfo;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.ExampleObject;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;

/**
 *
 * @author Christian Cougourdan
 */
@Path("/v2")
public interface IKeyReadService {

    @GET
    @Path("/key/{key}")
    @Produces({ MediaType.APPLICATION_JSON })
    @Operation(summary = "Informations about a reference key.", tags = { "Key Read Service" })
    @ApiResponses(value = { //
            @ApiResponse( //
                    responseCode = "200", //
                    description = "Successfull operation", //
                    content = @Content( //
                            mediaType = MediaType.APPLICATION_JSON, //
                            schema = @Schema(implementation = KeyInfo.class), //
                            examples = @ExampleObject( //
                                    name = "simple", //
                                    value = "{\"key\":\"U78010019435\",\"user\":\"Guichet Entreprises\",\"created\":1463738071235,\"updated\":1463738071235,\"subKeys\":[]}" //
                            ) //
                    ) //
            ), //
            @ApiResponse(responseCode = "202", description = "No content found") //
    })
    KeyInfo load(@PathParam("key") String key);

    /**
     * Search for keys.
     *
     * * Searches and returns the matching keys. Search criterias are cumulatives. *
     * If search value is "empty-mode:true", search will be perforrmed to check if
     * the column which name was provided is empty.
     *
     * @param startIndex
     *            start index
     * @param maxResults
     *            max results returned
     * @param orders
     *            result orders
     * @return search result
     */
    @GET
    @Path("/key")
    @Produces({ MediaType.APPLICATION_JSON })
    @Operation( //
            summary = "Search for keys.", //
            description = "* Searches and returns the matchings keys. Search criterias are cumulative.\n" + //
                    "* If search value is \"empty-mode:true\", search will be performed to check if the column which name was provided is empty.\n" + //
                    "* If search value is like \"date-range-mode:DD/MM/YYYY:DD/MM/YYYY\", search will be performed to check if the column which name was provided is a date between the two provided.\n"
                    + //
                    "* In other cases, search will be performed to check if the column which name was provided contains the provided string.", //
            tags = { "Key Read Service" } //
    )
    @ApiResponses(value = { //
            @ApiResponse( //
                    responseCode = "200", //
                    description = "Successfull operation", //
                    content = @Content( //
                            mediaType = MediaType.APPLICATION_JSON, //
                            schema = @Schema(ref = "#/components/schemas/SearchResultKeyInfo"), //
                            examples = @ExampleObject( //
                                    name = "simple", //
                                    value = "{\"startIndex\":0,\"maxResults\":2,\"totalResults\":11,\"content\":[" //
                                            + "{\"key\":\"2016-07-KCH-ZNY-48\",\"user\":\"Guichet Entreprises\",\"created\":1467619509662,\"updated\":1467619509662,\"subKeys\":[]}," //
                                            + "{\"key\":\"2016-06-BKP-CCX-48\",\"user\":\"Guichet Entreprises\",\"created\":1467619509762,\"updated\":1467619509762,\"subKeys\":[]}" //
                                            + "]}" //
                            ) //
                    ) //
            ) //
    })
    SearchResult<KeyInfo> search( //
            @QueryParam("startIndex") @DefaultValue(SearchQuery.DEFAULT_START_INDEX) long startIndex, //
            @QueryParam("maxResults") @DefaultValue(SearchQuery.DEFAULT_MAX_RESULTS) long maxResults, //
            @QueryParam("order") List<SearchQueryOrder> orders //
    );

}
