package fr.ge.tracker.ws.v2.service;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import io.swagger.v3.oas.annotations.Operation;

/**
 *
 * @author Christian Cougourdan
 */
@Path("/v2")
public interface IMessageWriteService {

    @POST
    @Path("/key/{key}/msg")
    @Consumes(MediaType.TEXT_PLAIN)
    @Operation(summary = "Add a new message associated to reference key.", tags = { "Message Write Service" })
    void addMessage(@PathParam("key") String key, String message, @QueryParam("user") String user);

}
