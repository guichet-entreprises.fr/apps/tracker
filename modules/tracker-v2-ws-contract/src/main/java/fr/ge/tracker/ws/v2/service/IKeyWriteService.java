package fr.ge.tracker.ws.v2.service;

import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import io.swagger.v3.oas.annotations.Operation;

/**
 *
 * @author Christian Cougourdan
 */
@Path("/v2")
public interface IKeyWriteService {

    @POST
    @Path("/key")
    @Produces({ MediaType.TEXT_PLAIN })
    @Operation(summary = "Create a new reference key.", tags = { "Key Write Service" })
    String create(@QueryParam("user") String user);

    @POST
    @Path("/key/{key}/sub")
    @Produces({ MediaType.TEXT_PLAIN })
    @Operation(summary = "Link a new reference key to an existing one.", tags = { "Key Write Service" })
    boolean addSubKey(@PathParam("parent") String parentKey, @QueryParam("key") String subKey, @QueryParam("user") String user);

}
