package fr.ge.tracker.ws.v2.service;

import java.util.List;

import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import fr.ge.common.utils.bean.search.SearchQuery;
import fr.ge.common.utils.bean.search.SearchQueryOrder;
import fr.ge.common.utils.bean.search.SearchResult;
import fr.ge.tracker.ws.v2.model.Message;
import io.swagger.v3.oas.annotations.Operation;

/**
 *
 * @author Christian Cougourdan
 */
@Path("/v2")
public interface IMessageReadService {

    @GET
    @Path("/key/{key}/msg")
    @Produces({ MediaType.APPLICATION_JSON })
    @Operation(summary = "Search for message associated to a reference key.", tags = { "Message Read Service" })
    SearchResult<Message> searchMessage( //
            @PathParam("key") String key, //
            @QueryParam("startIndex") @DefaultValue(SearchQuery.DEFAULT_START_INDEX) long startIndex, //
            @QueryParam("maxResults") @DefaultValue(SearchQuery.DEFAULT_MAX_RESULTS) long maxResults, //
            @QueryParam("order") List<SearchQueryOrder> orders //
    );

}
