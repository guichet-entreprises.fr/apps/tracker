package fr.ge.tracker.ws.v2.model;

import java.time.Instant;
import java.util.Collection;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;

/**
 *
 * @author Christian Cougourdan
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class KeyInfo {

    private String key;

    private String user;

    private Instant created;

    private Instant updated;

    private Collection<KeyInfo> subKeys;

    /**
     * @return the key
     */
    public String getKey() {
        return this.key;
    }

    /**
     * @param key
     *            the key to set
     * @return current instance
     */
    public KeyInfo setKey(final String key) {
        this.key = key;
        return this;
    }

    /**
     * @return the user
     */
    public String getUser() {
        return this.user;
    }

    /**
     * @param user
     *            the user to set
     * @return current instance
     */
    public KeyInfo setUser(final String user) {
        this.user = user;
        return this;
    }

    /**
     * @return the created
     */
    public Instant getCreated() {
        return this.created;
    }

    /**
     * @param created
     *            the created to set
     * @return current instance
     */
    public KeyInfo setCreated(final Instant created) {
        this.created = created;
        return this;
    }

    /**
     * @return the updated
     */
    public Instant getUpdated() {
        return this.updated;
    }

    /**
     * @param updated
     *            the updated to set
     * @return current instance
     */
    public KeyInfo setUpdated(final Instant updated) {
        this.updated = updated;
        return this;
    }

    /**
     * @return the subKeys
     */
    public Collection<KeyInfo> getSubKeys() {
        return this.subKeys;
    }

    /**
     * @param subKeys
     *            the subKeys to set
     * @return current instance
     */
    public KeyInfo setSubKeys(final Collection<KeyInfo> subKeys) {
        this.subKeys = subKeys;
        return this;
    }

}
