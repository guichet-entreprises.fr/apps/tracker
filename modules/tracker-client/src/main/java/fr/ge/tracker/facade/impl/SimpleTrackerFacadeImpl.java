/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.ge.tracker.facade.impl;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;

import fr.ge.tracker.bean.Reference;
import fr.ge.tracker.bean.UidNfo;
import fr.ge.tracker.facade.ITrackerFacade;
import fr.ge.tracker.service.v1.IUidRestService;
import fr.ge.tracker.service.v2.IReferenceService;

/**
 * The Class SimpleTrackerFacadeImpl.
 *
 * @author Christian Cougourdan
 */
public class SimpleTrackerFacadeImpl implements ITrackerFacade {

    /** Logger. */
    private static final Logger LOGGER = LoggerFactory.getLogger(SimpleTrackerFacadeImpl.class);

    /** The Constant MISSING_CONF. */
    private static final String MISSING_CONF = "Missing Tracker configuration, '{}' operation aborted";

    /** The uid rest service. */
    private IUidRestService uidRestService;

    private IReferenceService referenceService;

    /** The uid rest service URL. */
    @Value("${mas.tracker.service.url:}")
    private String uidRestServiceUrl;

    /** The author. */
    private String author;

    /**
     * {@inheritDoc}
     */
    @Override
    public String link(final String newReference, final String existingReference) {
        if (StringUtils.isEmpty(this.uidRestServiceUrl)) {
            LOGGER.debug(MISSING_CONF, "link");
            return null;
        }
        return this.uidRestService.addReference(existingReference, newReference, this.author);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String post(final String reference, final String message) {
        if (StringUtils.isEmpty(this.uidRestServiceUrl)) {
            LOGGER.debug(MISSING_CONF, "post");
            return null;
        }
        return this.uidRestService.addMessage(reference, message, this.author);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public UidNfo getUid(final String value) {
        if (StringUtils.isEmpty(this.uidRestServiceUrl)) {
            LOGGER.debug(MISSING_CONF, "getUid");
            return null;
        }
        return this.uidRestService.getUid(value);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Reference getReference(final String value) {
        if (null == this.referenceService) {
            LOGGER.debug(MISSING_CONF, "getReference");
            return null;
        }
        return this.referenceService.load(value);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String createUid() {
        if (StringUtils.isEmpty(this.uidRestServiceUrl)) {
            LOGGER.debug(MISSING_CONF, "createUid");
            return null;
        }
        return this.uidRestService.createUid(this.author);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getOrCreate(final String value) {
        if (StringUtils.isEmpty(this.uidRestServiceUrl)) {
            LOGGER.debug(MISSING_CONF, "getOrCreate");
            return null;
        }
        return this.uidRestService.getOrCreateUid(value, this.author);
    }

    /**
     * Sets the uid rest service.
     *
     * @param uidRestService
     *            the uidRestService to set
     */
    public void setUidRestService(final IUidRestService uidRestService) {
        this.uidRestService = uidRestService;
    }

    /**
     * Gets the uid rest service url.
     *
     * @return the uid rest service url
     */
    public String getUidRestServiceUrl() {
        return this.uidRestServiceUrl;
    }

    /**
     * Sets the uid rest service url.
     *
     * @param uidRestServiceUrl
     *            the new uid rest service url
     */
    public void setUidRestServiceUrl(final String uidRestServiceUrl) {
        this.uidRestServiceUrl = uidRestServiceUrl;
    }

    /**
     * Sets the author.
     *
     * @param author
     *            the author to set
     */
    public void setAuthor(final String author) {
        this.author = author;
    }

    /**
     * Sets the reference (v2) service.
     *
     * @param referenceService
     *            the reference service to set
     */
    public void setReferenceService(final IReferenceService referenceService) {
        this.referenceService = referenceService;
    }

}
