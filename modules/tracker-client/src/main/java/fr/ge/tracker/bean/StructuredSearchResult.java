/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.ge.tracker.bean;

import java.util.Arrays;
import java.util.List;

/**
 * The Class StructuredSearchResult.
 *
 * @author Christian Cougourdan
 * @param <T>
 *            the generic type
 */
public class StructuredSearchResult<T> {

    /** Draw counter. */
    private int draw;

    /** Total records, before filtering. */
    private int recordsTotal;

    /** Total records, after filtering. */
    private int recordsFiltered;

    /** Data to be displayed. */
    private List<T> data;

    /**
     * Instantiates a new structured search result.
     */
    public StructuredSearchResult() {
        // Nothing to do
    }

    /**
     * Instantiates a new structured search result.
     *
     * @param draw
     *            the draw
     */
    public StructuredSearchResult(final int draw) {
        this.draw = draw;
    }

    /**
     * Gets the draw.
     *
     * @return the draw
     */
    public int getDraw() {
        return this.draw;
    }

    /**
     * Sets the draw.
     *
     * @param draw
     *            the draw to set
     */
    public void setDraw(final int draw) {
        this.draw = draw;
    }

    /**
     * Gets the records total.
     *
     * @return the recordsTotal
     */
    public int getRecordsTotal() {
        return this.recordsTotal;
    }

    /**
     * Sets the records total.
     *
     * @param recordsTotal
     *            the recordsTotal to set
     */
    public void setRecordsTotal(final int recordsTotal) {
        this.recordsTotal = recordsTotal;
    }

    /**
     * Gets the records filtered.
     *
     * @return the recordsFiltered
     */
    public int getRecordsFiltered() {
        return this.recordsFiltered;
    }

    /**
     * Sets the records filtered.
     *
     * @param recordsFiltered
     *            the recordsFiltered to set
     */
    public void setRecordsFiltered(final int recordsFiltered) {
        this.recordsFiltered = recordsFiltered;
    }

    /**
     * Gets the data.
     *
     * @return the data
     */
    public List<T> getData() {
        return this.data;
    }

    /**
     * Sets the data.
     *
     * @param data
     *            the data to set
     */
    public void setData(final List<T> data) {
        this.data = data;
    }

    /**
     * The Class Builder.
     *
     * @param <T>
     *            the generic type
     */
    public static class Builder<T> {

        /** The draw. */
        private int draw;

        /** The records total. */
        private int recordsTotal;

        /** The records filtered. */
        private int recordsFiltered;

        /** The data. */
        private List<T> data;

        /**
         * Draw.
         *
         * @param value
         *            the value
         * @return the builder
         */
        public Builder<T> draw(final int value) {
            this.draw = value;
            return this;
        }

        /**
         * Records total.
         *
         * @param value
         *            the value
         * @return the builder
         */
        public Builder<T> recordsTotal(final int value) {
            this.recordsTotal = value;
            return this;
        }

        /**
         * Records filtered.
         *
         * @param value
         *            the value
         * @return the builder
         */
        public Builder<T> recordsFiltered(final int value) {
            this.recordsFiltered = value;
            return this;
        }

        /**
         * Data.
         *
         * @param value
         *            the value
         * @return the builder
         */
        public Builder<T> data(final T... value) {
            this.data = Arrays.asList(value);
            return this;
        }

        /**
         * Builds the.
         *
         * @return the structured search result
         */
        public StructuredSearchResult<T> build() {
            final StructuredSearchResult<T> obj = new StructuredSearchResult<>();
            obj.setDraw(this.draw);
            obj.setRecordsTotal(this.recordsTotal);
            obj.setRecordsFiltered(this.recordsFiltered);
            obj.setData(this.data);
            return obj;
        }
    }
}
