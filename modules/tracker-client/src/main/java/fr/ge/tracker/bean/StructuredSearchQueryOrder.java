/**
 *
 */
package fr.ge.tracker.bean;

/**
 * The Class StructuredSearchQueryOrder.
 *
 * @author $Author: jpauchet $
 * @version $Revision: 0 $
 */
public class StructuredSearchQueryOrder {

    /** Columns to which ordering should be applied. */
    private Integer column;

    /** Ordering direction for this column. */
    private String dir;

    /**
     * Constructor.
     */
    public StructuredSearchQueryOrder() {
        // Nothing to do.
    }

    /**
     * Constructor.
     *
     * @param column
     *            the column
     * @param dir
     *            the dir
     */
    public StructuredSearchQueryOrder(final Integer column, final String dir) {
        this.column = column;
        this.dir = dir;
    }

    /**
     * Gets the column.
     *
     * @return the column
     */
    public Integer getColumn() {
        return this.column;
    }

    /**
     * Sets the column.
     *
     * @param column
     *            the column to set
     */
    public void setColumn(final Integer column) {
        this.column = column;
    }

    /**
     * Gets the dir.
     *
     * @return the dir
     */
    public String getDir() {
        return this.dir;
    }

    /**
     * Sets the dir.
     *
     * @param dir
     *            the dir to set
     */
    public void setDir(final String dir) {
        this.dir = dir;
    }

    /**
     * The Class Builder.
     */
    public static class Builder {

        /** The column. */
        private Integer column;

        /** The dir. */
        private String dir;

        /**
         * Column.
         *
         * @param value
         *            the value
         * @return the builder
         */
        public Builder column(final Integer value) {
            this.column = value;
            return this;
        }

        /**
         * Dir.
         *
         * @param value
         *            the value
         * @return the builder
         */
        public Builder dir(final String value) {
            this.dir = value;
            return this;
        }

        /**
         * Builds the.
         *
         * @return the structured search query order
         */
        public StructuredSearchQueryOrder build() {
            final StructuredSearchQueryOrder obj = new StructuredSearchQueryOrder();
            obj.setColumn(this.column);
            obj.setDir(this.dir);
            return obj;
        }

    }

}
