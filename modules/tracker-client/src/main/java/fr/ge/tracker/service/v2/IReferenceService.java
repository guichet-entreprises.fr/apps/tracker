package fr.ge.tracker.service.v2;

import java.util.List;

import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.xml.crypto.dsig.keyinfo.KeyInfo;

import fr.ge.common.utils.bean.search.SearchQuery;
import fr.ge.common.utils.bean.search.SearchQueryFilter;
import fr.ge.common.utils.bean.search.SearchQueryOrder;
import fr.ge.common.utils.bean.search.SearchResult;
import fr.ge.tracker.bean.Reference;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.ExampleObject;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;

/**
 *
 * @author Christian Cougourdan
 */
@Path("/v2")
public interface IReferenceService {

    @GET
    @Path("/ref/{key}")
    @Produces({ MediaType.APPLICATION_JSON })
    @Operation(summary = "Informations about a reference key.", tags = { "Reference Services [v2]" })
    @ApiResponses(value = { //
            @ApiResponse( //
                    responseCode = "200", //
                    description = "Successfull operation", //
                    content = @Content( //
                            mediaType = MediaType.APPLICATION_JSON, //
                            schema = @Schema(implementation = KeyInfo.class), //
                            examples = @ExampleObject( //
                                    name = "simple", //
                                    value = "{\"value\":\"U78010019435\",\"client\":\"Guichet Entreprises\",\"created\":1463738071235,\"updated\":1463738071235,\"references\":[]}" //
                            ) //
                    ) //
            ), //
            @ApiResponse(responseCode = "202", description = "No content found") //
    })
    Reference load( //
            @Parameter(description = "") //
            @PathParam("key") String key //
    );

    /**
     * Search for keys.
     *
     * * Searches and returns the matching keys. Search criterias are cumulatives. *
     * If search value is "empty-mode:true", search will be perforrmed to check if
     * the column which name was provided is empty.
     *
     * @param startIndex
     *            start index
     * @param maxResults
     *            max results returned
     * @param filters
     *            search filters
     * @param orders
     *            result orders
     * @return search result
     */
    @GET
    @Path("/ref")
    @Produces({ MediaType.APPLICATION_JSON })
    @Operation( //
            summary = "Search for references.", //
            description = "Multiple filters can be specified using pattern `<fieldName><op><value>`, where `op` can be :\n" + //
                    "- `:`  - equals\n" + //
                    "- `>`  - greater than\n" + //
                    "- `>=` - greater than or equals\n" + //
                    "- `<`  - less than\n" + //
                    "- `<=` - less than or equals", //
            tags = { "Reference Services [v2]" } //
    )
    @ApiResponses(value = { //
            @ApiResponse( //
                    responseCode = "200", //
                    description = "Successfull operation", //
                    content = @Content( //
                            mediaType = MediaType.APPLICATION_JSON, //
                            schema = @Schema(ref = "#/components/schemas/SearchResultMessage"), //
                            examples = @ExampleObject( //
                                    name = "simple", //
                                    value = "{\"startIndex\":0,\"maxResults\":2,\"totalResults\":11,\"content\":[" //
                                            + "{\"value\":\"2016-07-KCH-ZNY-48\",\"client\":\"Guichet Entreprises\",\"created\":1467619509662,\"updated\":1467619509662,\"references\":[]}," //
                                            + "{\"value\":\"2016-06-BKP-CCX-48\",\"client\":\"Guichet Entreprises\",\"created\":1467619509762,\"updated\":1467619509762,\"references\":[]}" //
                                            + "]}" //
                            ) //
                    ) //
            ) //
    })
    SearchResult<Reference> search( //
            @Parameter(description = "First element offset.") //
            @QueryParam("startIndex") @DefaultValue(SearchQuery.DEFAULT_START_INDEX) long startIndex, //
            @Parameter(description = "Max elements per page.") //
            @QueryParam("maxResults") @DefaultValue(SearchQuery.DEFAULT_MAX_RESULTS) long maxResults, //
            @Parameter(description = "Query filters as `<fieldName><op><value>`") //
            @QueryParam("filter") List<SearchQueryFilter> filters, //
            @Parameter(description = "Sort orders as `<fieldname>:<asc|desc>`") //
            @QueryParam("order") List<SearchQueryOrder> orders //
    );

}
