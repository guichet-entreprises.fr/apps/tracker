/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.ge.tracker.service.v1;

import java.util.List;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import fr.ge.tracker.bean.StructuredSearchResult;
import fr.ge.tracker.bean.UidNfo;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.ExampleObject;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;

/**
 * Goal of the application.
 *
 * <p>
 * Guichet Entrepises uses numerous ids to refer to the same business creator.
 * For example, Guichet Entreprises can attach an id beginning with "H" and
 * another id beginning with "U" to the file of the business creator. Then, the
 * file is automatically sent to a partner, who creates yet another id, and so
 * on. Tracker allows several client applications, like Guichet Entreprises and
 * its partners, to create links between all those ids. A partner just has to
 * use this API to link its id to the provided one. It allows Guichet
 * Entreprises to follow the workflow of how the file is being processed, and
 * eventually make statistics on the overall files processing.
 * </p>
 *
 * <h2>Manipulated data</h2>
 *
 * <p>
 * This service interface manipulates the <strong>UIDs</strong>,
 * <strong>messages</strong> and <strong>clients</strong> data types.
 * </p>
 *
 * <p>
 * An <strong>UID</strong> is either a random generated alphanumeric character
 * string or a character string provided by a client. The format of generated
 * <strong>UIDs</strong> is "<code>YYYY-MM-XXX-XXX-CC</code>", where :
 * </p>
 *
 * <ul>
 * <li><strong>YYYY</strong> is the current year</li>
 * <li><strong>MM</strong> is the current month</li>
 * <li><strong>XXX-XXX</strong> are random letters except 'I', 'O' and 'U'</li>
 * <li><strong>CC</strong> is a checksum</li>
 * </ul>
 *
 * <p>
 * Checksum is obtained from <i>UID</i> first part and computed as
 * <code>s[0]*31^(n-1) + s[1]*31^(n-2) + ... + s[n-1]</code> where s[i] is the
 * ith character of the whole <i>UID</i> until the last 'X', n is the number of
 * characters until the last 'X' in the <i>UID</i> pattern. Resulting value is
 * reduced to 2 digits by combining digits of string representation of the
 * previous formula.
 * </p>
 *
 * <p>
 * A <strong>message</strong> is a short text describing a processing that just
 * happened for a given <strong>UID</strong>.
 * </p>
 *
 * <p>
 * A <strong>client</strong> is an application using Tracker. It contains the
 * author name and may be used for security purposes. Each <strong>UID</strong>
 * and <strong>message</strong> has an author.
 * </p>
 *
 * <h2>How it works</h2>
 *
 * <p>
 * When an <strong>UID</strong> is linked to another <strong>UID</strong>, it is
 * a <strong>reference</strong>. Therefore, a <strong>reference</strong> is an
 * <strong>UID</strong> which has a parent. The terms <strong>UID</strong> and
 * <strong>reference</strong> refer to the same concept (same data structure). A
 * possible hierarchy of references would be :
 * </p>
 *
 * <img src="../images/uml/references-hierarchy.png" alt="References hierarchy"
 * />
 *
 * <p>
 * A <strong>message</strong> can be posted on any <strong>UID</strong> or
 * <strong>reference</strong>. Some technical <strong>messages</strong> are also
 * automatically posted. It is then possible to find all
 * <strong>messages</strong> posted on a hierarchy of
 * <strong>references</strong>.
 * </p>
 *
 * <p>
 * Whenever an <strong>UID</strong> or a <strong>message</strong> is created,
 * the <strong>client</strong> name must be provided for traceability.
 * </p>
 *
 * <p>
 * Each operation is performed calling the Tracker server. Java applications
 * should use the Tracker client, which will call the Tracker server
 * automatically :
 * </p>
 *
 * <img src="../images/uml/tracker-components.png" alt="Tracker components" />
 *
 * <h2>Errors handling</h2>
 *
 * <p>
 * Errors are handled silently. It means that services always answer with the
 * following HTTP codes :
 * </p>
 *
 * <ul>
 * <li>2** when the service exists</li>
 * <li>4** when the service does not exist</li>
 * </ul>
 *
 * <p>
 * Services may include functional error codes in their answers, as explained in
 * each service description.
 * </p>
 *
 * <h2>HTTP protocol</h2>
 *
 * <p>
 * All the following API methods must be invoked using HTTP methods : PUT, POST,
 * GET, ... To sum up :
 * </p>
 *
 * <ul>
 * <li>PUT is used to create data</li>
 * <li>POST is used to update data</li>
 * <li>GET is used to retrieve data</li>
 * </ul>
 *
 * <p>
 * Further description :
 * <a href="https://www.w3.org/Protocols/" target="_blank">HTTP protocol</a>
 * </p>
 *
 * @author $Author: jpauchet $
 * @version $Revision: 0 $
 */
@Path("/v1/uid")
public interface IUidRestService {
    /**
     * Creates and returns a new uid.
     *
     * <h3>Response</h3>
     * <ul>
     * <li><strong>Status 200</strong> - text/plain - UID has been created</li>
     * </ul>
     *
     * <h3>Example</h3>
     *
     * <pre class="prettyprint">
     * Request : PUT http://tracker-server-url/api/v1/uid?author=Guichet Entreprises
     * Answer : 2016-06-NFK-NZE-30
     * </pre>
     *
     * <h3>Behaviour</h3>
     * <ul>
     * <li><strong>Status 400</strong> If no <strong>author</strong> is
     * provided, returned with message "Missing author"</li>
     * </ul>
     *
     * @param author
     *            the identity of the entity sending this request
     * @return the response
     */
    @PUT
    @Path("/")
    @Produces(MediaType.TEXT_PLAIN)
    @Operation(summary = "Create a new UID", tags = { "UID REST Services" })
    @ApiResponses({ //
            @ApiResponse( //
                    responseCode = "200", //
                    description = "Successfull operation, return \"UID has been created\"", //
                    content = @Content( //
                            mediaType = MediaType.TEXT_PLAIN //
                    ) //
            ), //
            @ApiResponse( //
                    responseCode = "400", //
                    description = "No author provided, return \"Missing author\"", //
                    content = @Content( //
                            mediaType = MediaType.TEXT_PLAIN //
                    ) //
            ) //
    })
    String createUid( //
            @Parameter(description = "Identity of the entity sending the request.", required = true) //
            @QueryParam("author") String author //
    );

    /**
     * Retrieve an UID or create it.
     *
     * <h3>Response</h3>
     * <ul>
     * <li><strong>Status 200</strong> - text/plain - UID has been found or
     * created</li>
     * </ul>
     *
     * <h3>Example</h3>
     *
     * <pre class="prettyprint">
     * Request : PUT http://tracker-server-url/api/v1/uid/john_doe?author=Guichet Entreprises
     * Answer : 2016-06-NFK-NZE-30
     * </pre>
     *
     * <h3>Behaviour</h3>
     * <ul>
     * <li><strong>Status 400</strong> If no <strong>author</strong> is
     * provided, returned with message "Missing author"</li>
     * </ul>
     *
     * @param ref
     *            the UID or reference to retrieve or create
     * @param author
     *            the identity of the entity sending this request
     * @return the response
     */
    @PUT
    @Path("/{reference}")
    @Produces(MediaType.TEXT_PLAIN)
    @Operation(summary = "Retrieve an UID or create it if not exists.", tags = { "UID REST Services" })
    @ApiResponses({ //
            @ApiResponse( //
                    responseCode = "200", //
                    description = "Successfull operation, UID found or has been created.", //
                    content = @Content( //
                            mediaType = MediaType.TEXT_PLAIN, //
                            examples = @ExampleObject("2020-03-REC-ORD-42") //
                    ) //
            ), //
            @ApiResponse( //
                    responseCode = "400", //
                    description = "Author was not provided, return \"Missing author\".", //
                    content = @Content( //
                            mediaType = MediaType.TEXT_PLAIN //
                    ) //
            ), //
    })
    String getOrCreateUid( //
            @Parameter(description = "UID or reference to retrieve or create.") //
            @PathParam("reference") String ref, //
            @Parameter(description = "Identity of the entity sending the request.", required = true) //
            @QueryParam("author") String author //
    );

    /**
     * Searches UID data.
     *
     * @param uriInfo
     *            the uri info
     * @return search result object
     */
    @GET
    @Path("/search/data")
    @Produces(MediaType.APPLICATION_JSON)
    @Operation( //
            summary = "Search UID data.", //
            description = "**Behaviour** :\n" + //
                    "- Searches and returns the matching UIDs. The search criteria are cumulative.\n" + //
                    "- If search value is \"empty-mode:true\"\n" + //
                    "  - The search will be performed to check if the column which name was provided is empty.\n" + //
                    "- If search value is like \"date-range-mode:DD/MM/YYYY:DD/MM/YYYY\"\n" + //
                    "  - The search will be performed to check if the column which name was provided is a date between the 2 provided dates\n" + //
                    "- In other cases\n" + //
                    "  - The search will be performed to check if the column which name was provided contains the provided string\n" + //
                    "\n" + //
                    "**Example request** :\n" + //
                    "\n" + //
                    "    GET http://tracker-server-url/api/v1/uid/search/data\n" + //
                    "        &columns[0][data]=value   &columns[0][search][value]=48\n" + //
                    "        &columns[1][data]=parent  &columns[1][search][value]=empty-mode:true\n" + //
                    "        &columns[2][data]=created &columns[2][search][value]=date-range-mode:10/06/2016:06/07/2016\n" + //
                    "        &draw=6                   &length=10\n" + //
                    "        &order[0][column]=2       &order[0][dir]=desc\n" + //
                    "        &start=0\n", //
            tags = { "UID REST Services" } //
    )
    @ApiResponses({ //
            @ApiResponse( //
                    responseCode = "200", //
                    description = "UID found.", //
                    content = @Content( //
                            mediaType = MediaType.APPLICATION_JSON, //
                            examples = @ExampleObject("{ \"draw\": 6, \"recordsTotal\": 2, \"recordsFiltered\": 2, \"data\": [ { \"value\": \"2016-07-KCH-ZNY-48\", \"client\": \"Guichet Entreprises\", \"created\": 1467619509662, \"references\": [], \"parent\": \"\", \"messages\": null }, { \"value\": \"2016-06-BKP-CCX-48\", \"client\": \"Guichet Entreprises\", \"created\": 1467619509762, \"references\": [], \"parent\": \"\", \"messages\": null } ] }") //
                    ) //
            ) //
    })
    StructuredSearchResult<? extends Object> searchUidData(@Context UriInfo uriInfo);

    /**
     * Gets all the data for an uid.
     *
     * @param reference
     *            the reference id
     * @return the response
     */
    @GET
    @Path("/{reference}")
    @Produces({ MediaType.APPLICATION_JSON, MediaType.TEXT_XML })
    @Operation(summary = "Gets all data relative to specified UID.", tags = { "UID REST Services" })
    @ApiResponses({ //
            @ApiResponse( //
                    responseCode = "200", //
                    description = "Successfull operation, return reference informations.", //
                    content = @Content( //
                            mediaType = MediaType.APPLICATION_JSON, //
                            examples = @ExampleObject("{\"value\":\"U78010019435\",\"created\":1463738071235,\"client\":{\"created\":1463673051028,\"name\":\"Guichet entreprises\"},\"parent\":null,\"references\":[{\"value\":\"2016-04-AAA-AAA-69\",\"created\":1463739523361,\"client\":{\"created\":1463673051028,\"name\":\"Guichet entreprises\"},\"parent\":\"2016-04-AAA-AAA-69\",\"references\":[],\"messages\":[]},{\"value\":\"2016-04-BBB-BBB-42\",\"created\":1463739523362,\"client\":{\"created\":1463673051028,\"name\":\"Guichet entreprises\"},\"parent\":\"2016-04-AAA-AAA-69\",\"references\":[],\"messages\":[]}],\"messages\":[{\"created\":1463739545898,\"content\":\"UID has been created\",\"client\":{\"created\":1463673051028,\"name\":\"tracker\"}},{\"created\":1463739545899,\"content\":\"Record step 1 has been validated\",\"client\":{\"created\":1463673051029,\"name\":\"form-manager\"}}]}") //
                    ) //
            ), //
            @ApiResponse( //
                    responseCode = "202", //
                    description = "Requested UID does not exist, return \"No matching data found\".", //
                    content = @Content( //
                            mediaType = MediaType.APPLICATION_JSON //
                    ) //
            ) //
    })
    UidNfo getUid( //
            @Parameter(description = "UID or reference to retrieve information from.") //
            @PathParam("reference") String reference //
    );

    /**
     * Gets all the data of the parent uid for a reference.
     *
     * @param value
     *            the value
     * @return the response
     */
    @GET
    @Path("/{value}/parent")
    @Produces({ MediaType.APPLICATION_JSON, MediaType.TEXT_XML })
    @Operation(summary = "Gets all data relative to the parent of the specified UID.", tags = { "UID REST Services" })
    @ApiResponses({ //
            @ApiResponse( //
                    responseCode = "200", //
                    description = "Successfull operation, return reference informations.", //
                    content = @Content( //
                            mediaType = MediaType.APPLICATION_JSON, //
                            examples = @ExampleObject("{\"value\":\"U78010019435\",\"created\":1463738071235,\"client\":{\"created\":1463673051028,\"name\":\"Guichet entreprises\"},\"parent\":null,\"references\":[{\"value\":\"2016-04-AAA-AAA-69\",\"created\":1463739523361,\"client\":{\"created\":1463673051028,\"name\":\"Guichet entreprises\"},\"parent\":\"2016-04-AAA-AAA-69\",\"references\":[],\"messages\":[]},{\"value\":\"2016-04-BBB-BBB-42\",\"created\":1463739523362,\"client\":{\"created\":1463673051028,\"name\":\"Guichet entreprises\"},\"parent\":\"2016-04-AAA-AAA-69\",\"references\":[],\"messages\":[]}],\"messages\":[{\"created\":1463739545898,\"content\":\"UID has been created\",\"client\":{\"created\":1463673051028,\"name\":\"tracker\"}},{\"created\":1463739545899,\"content\":\"Record step 1 has been validated\",\"client\":{\"created\":1463673051029,\"name\":\"form-manager\"}}]}") //
                    ) //
            ), //
            @ApiResponse( //
                    responseCode = "202", //
                    description = "Requested UID does not exist.", //
                    content = @Content( //
                            mediaType = MediaType.TEXT_PLAIN, //
                            examples = @ExampleObject("No matching data found")) //
            ) //
    })
    UidNfo getUidByRef( //
            @Parameter(description = "UID or reference to retrieve information from parent.") //
            @PathParam("value") String value //
    );

    /**
     * Link between references.
     *
     * <h3>Response</h3>
     * <ul>
     * <li><strong>Status 204</strong> - text/plain - References are linked</li>
     * </ul>
     *
     * <h3>Example</h3>
     *
     * <pre class="prettyprint">
     * Request : POST http://tracker-server-url/api/v1/uid/2016-06-NFK-NZE-30/ref/U78010019435?author=Guichet Entreprises
     * Answer : Reference linked to this UID
     *
     * Request : POST http://tracker-server-url/api/v1/uid/U78010019435/ref/H12345678900?author=Guichet Entreprises
     * Answer : Reference linked to this UID
     *
     * Request : POST http://tracker-server-url/api/v1/uid/U78010019435/ref/H12345678900?author=Guichet Entreprises
     * Answer : Reference already linked to this UID
     *
     * Request : POST http://tracker-server-url/api/v1/uid/2016-06-NFK-NZE-30/ref/H12345678900?author=Guichet Entreprises
     * Answer : Reference already linked to another UID and this UID exists
     *
     * Request : POST http://tracker-server-url/api/v1/uid/not existing UID/ref/H12345678900?author=Guichet Entreprises
     * Answer : Reference reversed linked to UID, which didn't exist and was created on the fly
     *
     * Request : POST http://tracker-server-url/api/v1/uid/not existing UID bis/ref/new ref?author=Guichet Entreprises
     * Answer : Reference linked to this UID, which didn't exist and was created on the fly
     * </pre>
     *
     * <h3>Behaviour</h3>
     * <ul>
     * <li>If no <strong>author</strong> is provided :
     * <ul>
     * <li><strong>Status 400</strong> is returned with message "Missing author"
     * and the algorithm stops here</li>
     * </ul>
     * </li>
     * <li>If <strong>ref</strong> does not exist and <strong>uid</strong> does
     * not exist :
     * <ul>
     * <li>An UID <strong>newUid</strong> is created</li>
     * <li>The reference <strong>uid</strong> is created and linked to the UID
     * <strong>newUid</strong></li>
     * <li>The reference <strong>ref</strong> is created and linked to the
     * reference <strong>uid</strong></li>
     * <li><strong>Status 200</strong> is returned with message "Reference
     * linked to this UID, which didn't exist and was created on the fly"</li>
     * </ul>
     * </li>
     * <li>If <strong>ref</strong> does not exist and <strong>uid</strong>
     * exists :
     * <ul>
     * <li>The reference <strong>ref</strong> is created and linked to the UID
     * <strong>uid</strong></li>
     * <li><strong>Status 200</strong> is returned with message "Reference
     * linked to this UID"</li>
     * </ul>
     * </li>
     * <li>If <strong>ref</strong> exists and <strong>uid</strong> does not
     * exist :
     * <ul>
     * <li>Reverse link between <strong>ref</strong> and
     * <strong>uid</strong></li>
     * <li><strong>Status 200</strong> is returned with message "Reference
     * reversed linked to UID, which didn't exist and was created on the
     * fly"</li>
     * </ul>
     * </li>
     * <li>If <strong>ref</strong> exists and <strong>uid</strong> exists,
     * <strong>ref</strong> is not linked to <strong>uid</strong> and has no
     * parent :
     * <ul>
     * <li>The reference <strong>ref</strong> is linked to the UID
     * <strong>uid</strong></li>
     * <li><strong>Status 200</strong> is returned with message "Reference
     * linked to this UID"</li>
     * </ul>
     * </li>
     * <li>If <strong>ref</strong> exists and <strong>uid</strong> exists but
     * <strong>ref</strong> is not linked to <strong>uid</strong> :
     * <ul>
     * <li><strong>Status 202</strong> is returned with message "Reference
     * already linked to another UID and this UID exists"</li>
     * </ul>
     * </li>
     * <li>If <strong>ref</strong> exists and <strong>uid</strong> exists and
     * <strong>ref</strong> is linked to <strong>uid</strong> :
     * <ul>
     * <li><strong>Status 200</strong> is returned with message "Reference
     * already linked to this UID"</li>
     * </ul>
     * </li>
     * </ul>
     *
     * &lt;img src="../images/uml/rest-api-link.png" alt="Link process"
     * /&gt;&lt;/img&gt;
     *
     * @param uid
     *            the UID or reference to link to
     * @param ref
     *            the UID or reference to link
     * @param author
     *            the identity of the entity sending this request
     * @return the response
     */
    @POST
    @Path("/{uid}/ref/{ref}")
    @Operation( //
            summary = "Link between references.", //
            tags = { "UID REST Services" } //
    )
    @ApiResponses({ //
            @ApiResponse( //
                    responseCode = "200", //
                    description = "Requested link already exists or newly created.", //
                    content = @Content( //
                            mediaType = MediaType.TEXT_PLAIN, //
                            examples = { //
                                    @ExampleObject( //
                                            name = "Unknown child and parent", //
                                            description = "Both **ref** and **uid** does not exist :\n" + //
                                                    "- An UID **newUid** is created\n" + //
                                                    "- The reference **uid** is created and linked to the UID **newUid**\n" + //
                                                    "- The reference **ref** is created and linked to the reference **uid**", //
                                            value = "Reference linked to this UID, which didn't exist and was created on the fly" //
                                    ), //
                                    @ExampleObject( //
                                            name = "Unknown child reference", //
                                            description = "**ref** does not exist and **uid** does : \n" + //
                                                    "- The reference **ref** is created and linked to the UID **uid**", //
                                            value = "Reference linked to this UID" //
                                    ), //
                                    @ExampleObject( //
                                            name = "Reverse link", //
                                            description = "**ref** exists and **uid** not :\n" + //
                                                    "- Revert link between **ref** and **uid**", //
                                            value = "Reference reversed linked to UID, which didn't exist and was created on the fly" //
                                    ), //
                                    @ExampleObject( //
                                            name = "No parent", //
                                            description = "Both **ref** and **uid** exists, **ref** has not parent :\n" + //
                                                    "- The reference **ref** is linked to **uid**", //
                                            value = "Reference linked to this UID" //
                                    ), //
                                    @ExampleObject( //
                                            name = "Already linked", //
                                            description = "Both **ref** and **uid** exists and **ref** is already linked to **uid**.", //
                                            value = "Reference already linked to this UID" //
                                    ), //
                            } //
                    ) //
            ), //
            @ApiResponse( //
                    responseCode = "202", //
                    description = "Reference to link is already associated to another one.", //
                    content = @Content( //
                            mediaType = MediaType.TEXT_PLAIN, //
                            examples = @ExampleObject( //
                                    description = "Both **ref** and **uid** exist but **ref** is not linked to **uid**.", //
                                    value = "Reference already linked to another UID and this UID exists") //
                    ) //
            ), //
            @ApiResponse( //
                    responseCode = "400", //
                    description = "Author was not provided, return \"Missing author\".", //
                    content = @Content( //
                            mediaType = MediaType.TEXT_PLAIN, //
                            examples = @ExampleObject(name = "Missing author parameter", value = "Missing author") //
                    ) //
            ) //
    })
    String addReference( //
            @Parameter(description = "UID or reference to link to") //
            @PathParam("uid") String uid, //
            @Parameter(description = "UID or reference to link") //
            @PathParam("ref") String ref, //
            @Parameter(description = "Identity of the entity sending the request.", required = true) //
            @QueryParam("author") String author //
    );

    /**
     * Gets the list of all references linked to an uid.
     *
     * @param uid
     *            the uid
     * @return the response
     */
    @GET
    @Path("/{uid}/refs")
    @Produces({ MediaType.APPLICATION_JSON, MediaType.TEXT_XML })
    @Operation(summary = "Gets the list of all references linked to an uid.", tags = { "UID REST Services" })
    List<String> getReferences( //
            @Parameter(description = "UID to retrieve linked references.") //
            @PathParam("uid") String uid //
    );

    /**
     * Post a new message.
     *
     * **Response** :
     *
     * - **Status 204** - application/json - Message created and linked with
     * specific UID reference.
     *
     * **Example**
     *
     * Request : POST
     * http://tracker-server-url/api/v1/uid/U78010019435/msg?content=File sent
     * to partner&amp;author=Guichet Entreprises Answer : Message created and
     * linked to this reference
     *
     * Request : POST http://tracker-server-url/api/v1/uid/not existing
     * UID/msg?content=File sent to partner&amp;author=Guichet Entreprises
     * Answer : Message created and linked to this reference, which didn't exist
     * and was created on the fly
     *
     * **Behaviour** :
     *
     * * If no **content** is provided : * **Status 400** is returned with
     * message "Missing content" and the algorithm stops here
     *
     *
     * * If no **author** is provided : * **Status 400** is returned with
     * message "Missing author" and the algorithm stops here
     *
     *
     * * If **ref** does not exist : * An UID **uid** is created * The reference
     * **ref** is created and linked to the UID **uid** * A message is created
     * and linked to the reference **ref** * **Status 200** is returned with
     * message "Message created and linked to this reference, which didn't exist
     * and was created on the fly"
     *
     *
     * * If **ref** exists : * A message is created and linked to the reference
     * **ref** * **Status 200** is returned with message "Message created and
     * linked to this reference"
     *
     *
     * ![alt text](../images/uml/rest-api-post-message.png "Link process")
     *
     * @param ref
     *            the UID or reference to link the message to
     * @param content
     *            content of the message
     * @param author
     *            the identity of the entity sending this request
     * @return the response
     */
    @POST
    @Path("/{ref}/msg")
    @Operation(summary = "Post a new message.", tags = { "UID REST Services" })
    @ApiResponses({ //
            @ApiResponse( //
                    responseCode = "200", //
                    description = "Message is created.", //
                    content = @Content( //
                            mediaType = MediaType.TEXT_PLAIN, //
                            examples = { //
                                    @ExampleObject( //
                                            name = "Unknown reference", //
                                            description = "- An UID **uid** is created\n" + //
                                                    "- The reference **ref** is created and linked to the UID **uid**\n" + //
                                                    "- A message is created and linked to the reference **ref**", //
                                            value = "Message created and linked to this reference, which didn't exist and was created on the fly" //
                                    ), //
                                    @ExampleObject( //
                                            name = "Existing reference", //
                                            description = "- A message is created and linked to the reference **ref**", //
                                            value = "Message created and linked to this reference" //
                                    ) //
                            } //
                    ) //
            ), //
            @ApiResponse( //
                    responseCode = "400", //
                    description = "Mandatory parameter is missing.", //
                    content = @Content( //
                            mediaType = MediaType.TEXT_PLAIN, //
                            examples = { //
                                    @ExampleObject( //
                                            name = "Missing content", //
                                            value = "Missing content" //
                                    ), //
                                    @ExampleObject( //
                                            name = "Missing author", //
                                            value = "Missing author" //
                                    ), //
                                    @ExampleObject( //
                                            name = "Missing reference", //
                                            value = "Missing reference" //
                                    ) //
                            } //
                    ) //
            ) })
    String addMessage( //
            @Parameter(description = "UID or reference to link the message to") //
            @PathParam("ref") String ref, //
            @Parameter(description = "Content of the message") //
            @QueryParam("content") String content, //
            @Parameter(description = "Identity of the entity sending the request.", required = true) //
            @QueryParam("author") String author //
    );

    /**
     * Remove messages for uids.
     *
     * @param uids
     *            the uids
     * @return the response
     */
    @DELETE
    @Path("/remove")
    @Operation(summary = "Remove messages for uids.", tags = { "UID REST Services" })
    Response remove( //
            @Parameter(description = "UIDs to remove.") //
            @QueryParam("uids") List<String> uids //
    );
}
