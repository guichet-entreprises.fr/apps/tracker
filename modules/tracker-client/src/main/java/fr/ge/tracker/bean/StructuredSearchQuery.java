/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.ge.tracker.bean;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;

/**
 * The Class StructuredSearchQuery.
 *
 * @author Christian Cougourdan
 */
public class StructuredSearchQuery {

    /** Draw counter. */
    private Integer draw;

    /** Paging first record indicator. */
    private Integer start;

    /** Number of records that the table can display. */
    private Integer length;

    /** Column's specific search value. */
    private StructuredSearchQuerySearch search;

    /** Columns to which ordering should be applied. */
    private List<StructuredSearchQueryOrder> order;

    /** Columns definition. */
    private List<StructuredSearchQueryColumn> columns;

    /** Custom searches. */
    private List<StructuredSearchQueryColumn> customSearches;

    /**
     * Instantiates a new structured search query.
     */
    public StructuredSearchQuery() {
        this.search = new StructuredSearchQuerySearch();
        this.order = new ArrayList<>();
        this.columns = new ArrayList<>();
        this.customSearches = new ArrayList<>();
    }

    /**
     * Converts the columns to a criteria map.
     * 
     * @return the criteria map
     */
    public Map<String, String> toCriteriaMap() {
        final Map<String, String> criteriaMap = new HashMap<>();
        if (this.columns != null) {
            for (final StructuredSearchQueryColumn column : this.columns) {
                if (column.getSearch() != null) {
                    final String columnName = column.getData();
                    final String columnSearchValue = column.getSearch().getValue();
                    if (StringUtils.isNotEmpty(columnSearchValue)) {
                        criteriaMap.put(columnName, columnSearchValue);
                    }
                }
            }
        }
        return criteriaMap;
    }

    /**
     * Finds a custom search value with a custom search column name.
     * 
     * @param columnName
     *            the column name
     * @return the custom search value
     */
    public String findCustomSearchValue(final String columnName) {
        if (this.customSearches != null) {
            for (final StructuredSearchQueryColumn col : this.customSearches) {
                if (columnName.equals(col.getData())) {
                    return col.getSearch().getValue();
                }
            }
        }
        return null;
    }

    /**
     * Gets the draw.
     *
     * @return the draw
     */
    public Integer getDraw() {
        return this.draw;
    }

    /**
     * Sets the draw.
     *
     * @param draw
     *            the draw to set
     */
    public void setDraw(final Integer draw) {
        this.draw = draw;
    }

    /**
     * Gets the start.
     *
     * @return the start
     */
    public Integer getStart() {
        return this.start;
    }

    /**
     * Sets the start.
     *
     * @param start
     *            the start to set
     */
    public void setStart(final Integer start) {
        this.start = start;
    }

    /**
     * Gets the length.
     *
     * @return the length
     */
    public Integer getLength() {
        return this.length;
    }

    /**
     * Sets the length.
     *
     * @param length
     *            the length to set
     */
    public void setLength(final Integer length) {
        this.length = length;
    }

    /**
     * Gets the search.
     *
     * @return the search
     */
    public StructuredSearchQuerySearch getSearch() {
        return this.search;
    }

    /**
     * Sets the search.
     *
     * @param search
     *            the search to set
     */
    public void setSearch(final StructuredSearchQuerySearch search) {
        this.search = search;
    }

    /**
     * Gets the order.
     *
     * @return the order
     */
    public List<StructuredSearchQueryOrder> getOrder() {
        return this.order;
    }

    /**
     * Sets the order.
     *
     * @param order
     *            the order to set
     */
    public void setOrder(final List<StructuredSearchQueryOrder> order) {
        this.order = order;
    }

    /**
     * Gets the columns.
     *
     * @return the columns
     */
    public List<StructuredSearchQueryColumn> getColumns() {
        return this.columns;
    }

    /**
     * Getter on attribute {@link #customSearches}.
     *
     * @return List&lt;StructuredSearchQueryColumn&gt; customSearches
     */
    public List<StructuredSearchQueryColumn> getCustomSearches() {
        return this.customSearches;
    }

    /**
     * Setter on attribute {@link #customSearches}.
     *
     * @param customSearches
     *            the new value of attribute customSearches
     */
    public void setCustomSearches(final List<StructuredSearchQueryColumn> customSearches) {
        this.customSearches = customSearches;
    }

    /**
     * Sets the columns.
     *
     * @param columns
     *            the columns to set
     */
    public void setColumns(final List<StructuredSearchQueryColumn> columns) {
        this.columns = columns;
    }

    /**
     * The Class Builder.
     */
    public static class Builder {

        /** The draw. */
        private Integer draw;

        /** The start. */
        private Integer start;

        /** The length. */
        private Integer length;

        /** The search. */
        private StructuredSearchQuerySearch search;

        /** The order. */
        private List<StructuredSearchQueryOrder> order;

        /** The columns. */
        private List<StructuredSearchQueryColumn> columns;

        /** Custom searches. */
        private List<StructuredSearchQueryColumn> customSearches;

        /**
         * Draw.
         *
         * @param value
         *            the value
         * @return the builder
         */
        public Builder draw(final Integer value) {
            this.draw = value;
            return this;
        }

        /**
         * Start.
         *
         * @param value
         *            the value
         * @return the builder
         */
        public Builder start(final Integer value) {
            this.start = value;
            return this;
        }

        /**
         * Length.
         *
         * @param value
         *            the value
         * @return the builder
         */
        public Builder length(final Integer value) {
            this.length = value;
            return this;
        }

        /**
         * Search.
         *
         * @param value
         *            the value
         * @return the builder
         */
        public Builder search(final StructuredSearchQuerySearch value) {
            this.search = value;
            return this;
        }

        /**
         * Order.
         *
         * @param value
         *            the value
         * @return the builder
         */
        public Builder order(final StructuredSearchQueryOrder... value) {
            this.order = Arrays.asList(value);
            return this;
        }

        /**
         * Columns.
         *
         * @param value
         *            the value
         * @return the builder
         */
        public Builder columns(final StructuredSearchQueryColumn... value) {
            this.columns = Arrays.asList(value);
            return this;
        }

        /**
         * Custom searches.
         *
         * @param value
         *            the value
         * @return the builder
         */
        public Builder customSearches(final StructuredSearchQueryColumn... value) {
            this.columns = Arrays.asList(value);
            return this;
        }

        /**
         * Builds the.
         *
         * @return the structured search query
         */
        public StructuredSearchQuery build() {
            final StructuredSearchQuery obj = new StructuredSearchQuery();
            obj.setDraw(this.draw);
            obj.setStart(this.start);
            obj.setLength(this.length);
            obj.setSearch(this.search);
            obj.setOrder(this.order);
            obj.setColumns(this.columns);
            obj.setCustomSearches(this.customSearches);
            return obj;
        }

    }

}
