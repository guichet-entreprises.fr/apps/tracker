package fr.ge.tracker.service.v2;

import java.util.List;

import javax.ws.rs.DELETE;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import fr.ge.common.utils.bean.search.SearchQuery;
import fr.ge.common.utils.bean.search.SearchQueryFilter;
import fr.ge.common.utils.bean.search.SearchQueryOrder;
import fr.ge.common.utils.bean.search.SearchResult;
import fr.ge.tracker.bean.Message;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;

/**
 *
 * @author Christian Cougourdan
 */
@Path("/v2")
public interface IMessageService {

    /**
     * Search for reference's messages.
     *
     * @param key
     *            reference
     * @param startIndex
     *            start index
     * @param maxResults
     *            max results
     * @param orders
     *            sort order
     * @return messages search result
     */
    // @GET
    // @Path("/ref/{key}/msg")
    // @Produces({ MediaType.APPLICATION_JSON })
    // @Operation(summary = "Search for message associated to a reference.",
    // tags = { "Message Services [v2]" })
    // SearchResult<Message> searchMessage( //
    // @Parameter(description = "Reference key which messages are attached to.")
    // //
    // @PathParam("key") String key, //
    // @Parameter(description = "First element offset.") //
    // @QueryParam("startIndex") @DefaultValue(SearchQuery.DEFAULT_START_INDEX)
    // long startIndex, //
    // @Parameter(description = "Max elements per page.") //
    // @QueryParam("maxResults") @DefaultValue(SearchQuery.DEFAULT_MAX_RESULTS)
    // long maxResults, //
    // @Parameter(description = "Sort orders as `<fieldname>:<asc|desc>`") //
    // @QueryParam("order") List<SearchQueryOrder> orders //
    // );

    /**
     * Search for messages.
     * 
     * @param startIndex
     *            start index
     * @param maxResults
     *            max results
     * @param filters
     *            filters
     * @param orders
     *            sort order
     * @return messages search result
     */
    @GET
    @Path("/msg")
    @Produces({ MediaType.APPLICATION_JSON })
    @Operation(summary = "Search for messages.", tags = { "Message Services [v2]" })
    SearchResult<Message> searchMessage( //
            @Parameter(description = "First element offset.") //
            @QueryParam("startIndex") @DefaultValue(SearchQuery.DEFAULT_START_INDEX) final long startIndex, //
            @Parameter(description = "Max elements per page.") //
            @QueryParam("maxResults") @DefaultValue(SearchQuery.DEFAULT_MAX_RESULTS) final long maxResults, // //
            @Parameter(description = "Query filters as `<fieldName><op><value>`") //
            @QueryParam("filters") final List<SearchQueryFilter> filters, //
            @Parameter(description = "Sort orders as `<fieldname>:<asc|desc>`") //
            @QueryParam("orders") final List<SearchQueryOrder> orders //
    );

    /**
     * Remove messages.
     * 
     * @param key
     *            The reference as key
     * @param terms
     *            The search terms
     * @return The removal status
     */
    @DELETE
    @Path("/ref/{key}/msg")
    @Operation(summary = "Delete messages associated to a reference.", tags = { "Message Services [v2]" })
    Response removeMessages( //
            @Parameter(description = "Reference key which messages are attached to.") //
            @PathParam("key") final String key, //
            @Parameter(description = "Terms") //
            @QueryParam("q") final String terms //
    );
}
