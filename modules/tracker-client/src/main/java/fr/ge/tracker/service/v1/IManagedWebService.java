/**
 * 
 */
package fr.ge.tracker.service.v1;

import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import fr.ge.tracker.bean.UidNfo;
import io.swagger.annotations.Api;

/**
 * Tracker managed API.
 * 
 * @author $Author: aolubi $
 * @version $Revision: 0 $
 */
@Api(value = "UID REST Services")
@Path("/v1/uid")
public interface IManagedWebService {
    /**
     * Creates and returns a new uid.
     *
     * <h3>Response</h3>
     * <ul>
     * <li><strong>Status 200</strong> - text/plain - UID has been created</li>
     * </ul>
     *
     * <h3>Example</h3>
     *
     * <pre class="prettyprint">
     * Request : PUT http://tracker-server-url/api/v1/uid?author=Guichet Entreprises
     * Answer : 2016-06-NFK-NZE-30
     * </pre>
     *
     * <h3>Behaviour</h3>
     * <ul>
     * <li><strong>Status 400</strong> If no <strong>author</strong> is
     * provided, returned with message "Missing author"</li>
     * </ul>
     *
     * @param author
     *            the identity of the entity sending this request
     * @return the response
     */
    @PUT
    @Path("/")
    @Produces(MediaType.TEXT_PLAIN)
    String createUid(@QueryParam("author") String author);

    /**
     * Gets all the data for an uid.
     *
     * @param reference
     *            the reference id
     * @return the response
     */
    @GET
    @Path("/{reference}")
    @Produces({ MediaType.APPLICATION_JSON, MediaType.TEXT_XML })
    UidNfo getUid(@PathParam("reference") String reference);

    /**
     * Link between references.
     *
     * <h3>Response</h3>
     * <ul>
     * <li><strong>Status 204</strong> - text/plain - References are linked</li>
     * </ul>
     *
     * <h3>Example</h3>
     *
     * <pre class="prettyprint">
     * Request : POST http://tracker-server-url/api/v1/uid/2016-06-NFK-NZE-30/ref/U78010019435?author=Guichet Entreprises
     * Answer : Reference linked to this UID
     *
     * Request : POST http://tracker-server-url/api/v1/uid/U78010019435/ref/H12345678900?author=Guichet Entreprises
     * Answer : Reference linked to this UID
     *
     * Request : POST http://tracker-server-url/api/v1/uid/U78010019435/ref/H12345678900?author=Guichet Entreprises
     * Answer : Reference already linked to this UID
     *
     * Request : POST http://tracker-server-url/api/v1/uid/2016-06-NFK-NZE-30/ref/H12345678900?author=Guichet Entreprises
     * Answer : Reference already linked to another UID and this UID exists
     *
     * Request : POST http://tracker-server-url/api/v1/uid/not existing UID/ref/H12345678900?author=Guichet Entreprises
     * Answer : Reference reversed linked to UID, which didn't exist and was created on the fly
     *
     * Request : POST http://tracker-server-url/api/v1/uid/not existing UID bis/ref/new ref?author=Guichet Entreprises
     * Answer : Reference linked to this UID, which didn't exist and was created on the fly
     * </pre>
     *
     * <h3>Behaviour</h3>
     * <ul>
     * <li>If no <strong>author</strong> is provided :
     * <ul>
     * <li><strong>Status 400</strong> is returned with message "Missing author"
     * and the algorithm stops here</li>
     * </ul>
     * </li>
     * <li>If <strong>ref</strong> does not exist and <strong>uid</strong> does
     * not exist :
     * <ul>
     * <li>An UID <strong>newUid</strong> is created</li>
     * <li>The reference <strong>uid</strong> is created and linked to the UID
     * <strong>newUid</strong></li>
     * <li>The reference <strong>ref</strong> is created and linked to the
     * reference <strong>uid</strong></li>
     * <li><strong>Status 200</strong> is returned with message "Reference
     * linked to this UID, which didn't exist and was created on the fly"</li>
     * </ul>
     * </li>
     * <li>If <strong>ref</strong> does not exist and <strong>uid</strong>
     * exists :
     * <ul>
     * <li>The reference <strong>ref</strong> is created and linked to the UID
     * <strong>uid</strong></li>
     * <li><strong>Status 200</strong> is returned with message "Reference
     * linked to this UID"</li>
     * </ul>
     * </li>
     * <li>If <strong>ref</strong> exists and <strong>uid</strong> does not
     * exist :
     * <ul>
     * <li>Reverse link between <strong>ref</strong> and
     * <strong>uid</strong></li>
     * <li><strong>Status 200</strong> is returned with message "Reference
     * reversed linked to UID, which didn't exist and was created on the
     * fly"</li>
     * </ul>
     * </li>
     * <li>If <strong>ref</strong> exists and <strong>uid</strong> exists,
     * <strong>ref</strong> is not linked to <strong>uid</strong> and has no
     * parent :
     * <ul>
     * <li>The reference <strong>ref</strong> is linked to the UID
     * <strong>uid</strong></li>
     * <li><strong>Status 200</strong> is returned with message "Reference
     * linked to this UID"</li>
     * </ul>
     * </li>
     * <li>If <strong>ref</strong> exists and <strong>uid</strong> exists but
     * <strong>ref</strong> is not linked to <strong>uid</strong> :
     * <ul>
     * <li><strong>Status 202</strong> is returned with message "Reference
     * already linked to another UID and this UID exists"</li>
     * </ul>
     * </li>
     * <li>If <strong>ref</strong> exists and <strong>uid</strong> exists and
     * <strong>ref</strong> is linked to <strong>uid</strong> :
     * <ul>
     * <li><strong>Status 200</strong> is returned with message "Reference
     * already linked to this UID"</li>
     * </ul>
     * </li>
     * </ul>
     *
     * &lt;img src="../images/uml/rest-api-link.png" alt="Link
     * process"&gt;&lt;/img&gt;
     *
     * @param uid
     *            the UID or reference to link to
     * @param ref
     *            the UID or reference to link
     * @param author
     *            the identity of the entity sending this request
     * @return the response
     */
    @POST
    @Path("/{uid}/ref/{ref}")
    String addReference(@PathParam("uid") String uid, @PathParam("ref") String ref, @QueryParam("author") String author);

    /**
     * Gets the list of all references linked to an uid.
     *
     * @param uid
     *            the uid
     * @return the response
     */
    @GET
    @Path("/{uid}/refs")
    @Produces({ MediaType.APPLICATION_JSON, MediaType.TEXT_XML })
    List<String> getReferences(@PathParam("uid") String uid);

    /**
     * Post a new message.
     *
     * **Response** :
     *
     * - **Status 204** - application/json - Message created and linked with
     * specific UID reference.
     *
     * **Example**
     *
     * Request : POST
     * http://tracker-server-url/api/v1/uid/U78010019435/msg?content=File&amp;author=nash
     * 
     * **Answer** : Message created and linked to this reference
     *
     * Request : POST
     * http://tracker-server-url/api/v1/uid/not_existing_UID/msg?content=File&amp;author=nash
     * 
     * ** Answer** : Message created and linked to this reference, which didn't
     * exist and was created on the fly
     *
     * **Behaviour** :
     *
     * * If no **content** is provided : * **Status 400** is returned with
     * message "Missing content" and the algorithm stops here
     *
     *
     * * If no **author** is provided : * **Status 400** is returned with
     * message "Missing author" and the algorithm stops here
     *
     *
     * * If **ref** does not exist : * An UID **uid** is created * The reference
     * **ref** is created and linked to the UID **uid** * A message is created
     * and linked to the reference **ref** * **Status 200** is returned with
     * message "Message created and linked to this reference, which didn't exist
     * and was created on the fly"
     *
     *
     * * If **ref** exists : * A message is created and linked to the reference
     * **ref** * **Status 200** is returned with message "Message created and
     * linked to this reference"
     *
     *
     * ![alt text](../images/uml/rest-api-post-message.png "Link process")
     *
     * @param ref
     *            the UID or reference to link the message to
     * @param content
     *            content of the message
     * @param author
     *            the identity of the entity sending this request
     * @return the response
     */
    @POST
    @Path("/{ref}/msg")
    String addMessage(@PathParam("ref") String ref, @QueryParam("content") String content, @QueryParam("author") String author);
}
