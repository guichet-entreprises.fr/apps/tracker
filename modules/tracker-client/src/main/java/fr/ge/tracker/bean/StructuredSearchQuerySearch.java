/**
 *
 */
package fr.ge.tracker.bean;

/**
 * The Class StructuredSearchQuerySearch.
 *
 * @author $Author: jpauchet $
 * @version $Revision: 0 $
 */
public class StructuredSearchQuerySearch {

    /** Global search value. */
    private String value;

    /** Flag to indicate if this search criteria is a regular expression. */
    private Boolean regex;

    /**
     * Constructor.
     */
    public StructuredSearchQuerySearch() {
        // Nothing to do.
    }

    /**
     * Constructor.
     *
     * @param value
     *            the value
     * @param regex
     *            the regex
     */
    public StructuredSearchQuerySearch(final String value, final Boolean regex) {
        this.value = value;
        this.regex = regex;
    }

    /**
     * Gets the value.
     *
     * @return the value
     */
    public String getValue() {
        return this.value;
    }

    /**
     * Sets the value.
     *
     * @param value
     *            the value to set
     */
    public void setValue(final String value) {
        this.value = value;
    }

    /**
     * Checks if is regex.
     *
     * @return the regex
     */
    public Boolean isRegex() {
        return this.regex;
    }

    /**
     * Sets the regex.
     *
     * @param regex
     *            the regex to set
     */
    public void setRegex(final Boolean regex) {
        this.regex = regex;
    }

    /**
     * The Class Builder.
     */
    public static class Builder {

        /** The value. */
        private String value;

        /** The regex. */
        private Boolean regex;

        /**
         * Value.
         *
         * @param value
         *            the value
         * @return the builder
         */
        public Builder value(final String value) {
            this.value = value;
            return this;
        }

        /**
         * Regex.
         *
         * @param value
         *            the value
         * @return the builder
         */
        public Builder regex(final Boolean value) {
            this.regex = value;
            return this;
        }

        /**
         * Builds the.
         *
         * @return the structured search query search
         */
        public StructuredSearchQuerySearch build() {
            final StructuredSearchQuerySearch obj = new StructuredSearchQuerySearch();
            obj.setValue(this.value);
            obj.setRegex(this.regex);
            return obj;
        }
    }

}
