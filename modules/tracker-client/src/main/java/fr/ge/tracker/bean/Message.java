/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.ge.tracker.bean;

import java.util.Calendar;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * The Class Message.
 */
@XmlRootElement(name = "msg")
@XmlAccessorType(XmlAccessType.FIELD)
public class Message {

    /** The content. */
    private String content;

    /** The uid. */
    private String uid;

    /** The created. */
    private Calendar created;

    /** The client. */
    private String client;

    /**
     * Gets the content.
     *
     * @return the content
     */
    public String getContent() {
        return this.content;
    }

    /**
     * Sets the content.
     *
     * @param content
     *            the content to set
     */
    public Message setContent(final String content) {
        this.content = content;
        return this;
    }

    /**
     * Getter on attribute {@link #uid}.
     *
     * @return String uid
     */
    public String getUid() {
        return this.uid;
    }

    /**
     * Setter on attribute {@link #uid}.
     *
     * @param uid
     *            the new value of attribute uid
     */
    public Message setUid(final String uid) {
        this.uid = uid;
        return this;
    }

    /**
     * Gets the created.
     *
     * @return the created
     */
    public Calendar getCreated() {
        return this.created;
    }

    /**
     * Sets the created.
     *
     * @param created
     *            the created to set
     */
    public Message setCreated(final Calendar created) {
        this.created = created;
        return this;
    }

    /**
     * Gets the client.
     *
     * @return the client
     */
    public String getClient() {
        return this.client;
    }

    /**
     * Sets the client.
     *
     * @param client
     *            the client to set
     */
    public Message setClient(final String client) {
        this.client = client;
        return this;
    }

}
