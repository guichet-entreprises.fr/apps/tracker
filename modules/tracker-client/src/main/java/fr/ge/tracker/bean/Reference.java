/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.ge.tracker.bean;

import java.util.Calendar;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * Represents a TRACKER reference.
 */
@XmlRootElement(name = "ref")
@XmlAccessorType(XmlAccessType.FIELD)
public class Reference {

    /** The id. */
    @XmlTransient
    @JsonIgnore
    private Long id;

    /** The value. */
    private String value;

    /** The client. */
    private String client;

    /** The created. */
    private Calendar created;

    /** The created. */
    private Calendar updated;

    /** The parent. */
    private String parent;

    /** The references. */
    private List<Reference> references;

    /**
     * @return the id
     */
    public Long getId() {
        return this.id;
    }

    /**
     * @param id
     *            the id to set
     */
    public void setId(final Long id) {
        this.id = id;
    }

    /**
     * @return the value
     */
    public String getValue() {
        return this.value;
    }

    /**
     * @param value
     *            the value to set
     */
    public void setValue(final String value) {
        this.value = value;
    }

    /**
     * @return the client
     */
    public String getClient() {
        return this.client;
    }

    /**
     * @param client
     *            the client to set
     */
    public void setClient(final String client) {
        this.client = client;
    }

    /**
     * @return the created
     */
    public Calendar getCreated() {
        return this.created;
    }

    /**
     * @param created
     *            the created to set
     */
    public void setCreated(final Calendar created) {
        this.created = created;
    }

    /**
     * @return the updated
     */
    public Calendar getUpdated() {
        return this.updated;
    }

    /**
     * @param updated
     *            the updated to set
     */
    public void setUpdated(final Calendar updated) {
        this.updated = updated;
    }

    /**
     * @return the parent
     */
    public String getParent() {
        return this.parent;
    }

    /**
     * @param parent
     *            the parent to set
     */
    public void setParent(final String parent) {
        this.parent = parent;
    }

    /**
     * @return the references
     */
    public List<Reference> getReferences() {
        return this.references;
    }

    /**
     * @param references
     *            the references to set
     */
    public void setReferences(final List<Reference> references) {
        this.references = references;
    }

}
