<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:wadl="http://wadl.dev.java.net/2009/02" xmlns:html="http://www.w3.org/1999/xhtml" xmlns="http://www.w3.org/1999/xhtml">

    <xsl:template match="wadl:application">
        <html>
            <head>
                <link rel="stylesheet" href="apache-maven-fluido-1.5.min.css" />
                <style type="text/css"><![CDATA[
                    .rest-service { border: 1px solid #CCC; margin: 1.2em 1.8em; }
                    .rest-service-header { background-color: #EEE; border-bottom: 1px solid #CCC; line-height: 1.8em; }
                    .rest-service-header > .rest-service-title { float: right; padding-right: .6em; }
                    .rest-service-header > .rest-service-method { background-color: #DDD; border-right: 1px solid #CCC; float: left; font-weight: bold; text-align: center; width: 4em; }
                    .rest-service-header > .rest-service-path { margin-left: 4em; padding-left: .6em; }
                    .rest-service-body { padding: .6em 1.2em; }
                ]]></style>
            </head>
            <body>
                <h2>Services</h2>
                <ul>
                    <xsl:apply-templates select="wadl:resources/wadl:resource/wadl:method" mode="toc"/>
                </ul>
                <xsl:apply-templates select="wadl:resources/wadl:resource" />
            </body>
        </html>
    </xsl:template>

    <xsl:template match="wadl:resource/wadl:method" mode="toc">
        <xsl:param name="basePath" select="''" />

        <li>
            <a>
                <xsl:attribute name="href">
                    <xsl:text>#</xsl:text>
                    <xsl:value-of select="generate-id(.)" />
                </xsl:attribute>
                <xsl:apply-templates select="wadl:doc" mode="title" />
            </a>
        </li>

        <xsl:apply-templates select="../wadl:resource/wadl:method" mode="toc">
            <xsl:with-param name="basePath" select="concat($basePath, @path)" />
        </xsl:apply-templates>
    </xsl:template>

    <xsl:template match="wadl:resource[wadl:method]">
        <xsl:param name="basePath" select="''" />

        <div class="rest-resource">
            <xsl:variable name="title">
                <xsl:apply-templates select="wadl:doc" mode="title" />
            </xsl:variable>

            <xsl:if test="string-length($title) > 0">
                <h2>
                    <xsl:value-of select="$title" />
                </h2>
            </xsl:if>

            <xsl:apply-templates select="wadl:doc" mode="detail" />
        </div>

        <div class="rest-services">
            <xsl:apply-templates select="wadl:method">
                <xsl:with-param name="basePath" select="concat($basePath, @path)" />
            </xsl:apply-templates>
        </div>

        <xsl:apply-templates select="wadl:resource">
            <xsl:with-param name="basePath" select="concat($basePath, @path)" />
        </xsl:apply-templates>
    </xsl:template>

    <xsl:template match="wadl:doc">
        <p>
            <xsl:value-of select="." disable-output-escaping="yes" />
        </p>
    </xsl:template>

    <xsl:template match="wadl:method">
        <xsl:param name="basePath" select="''" />

        <h2>
            <a>
                <xsl:attribute name="name">
                    <xsl:value-of select="generate-id(.)" />
                </xsl:attribute>
            </a>
            <xsl:apply-templates select="wadl:doc" mode="title" />
        </h2>

        <p>
            <strong><xsl:text>Request : </xsl:text></strong>
            <code>
                <xsl:value-of select="@name" />
                <xsl:value-of select="$basePath" />
            </code>
        </p>

        <xsl:apply-templates select="wadl:request" />
        <xsl:apply-templates select="wadl:response" />
        <xsl:apply-templates select="wadl:doc" mode="detail" />
    </xsl:template>

    <xsl:template match="wadl:request[wadl:param | ../../wadl:param]">
        <p>
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th>Parameter</th>
                        <th>Type</th>
                        <th>Description</th>
                    </tr>
                </thead>
                <tbody>
                    <xsl:for-each select="../../wadl:param | wadl:param">
                        <tr>
                            <td><xsl:value-of select="@name" /></td>
                            <td><xsl:call-template name="getType" /></td>
                            <td><xsl:value-of select="wadl:doc" disable-output-escaping="yes"/></td>
                        </tr>
                    </xsl:for-each>
                </tbody>
            </table>
        </p>
    </xsl:template>

    <xsl:template match="wadl:response">
<!--         <p> -->
<!--             <strong> -->
<!--                 <xsl:text>Response : </xsl:text> -->
<!--             </strong> -->
<!--             <ul> -->
<!--                 <xsl:for-each select="wadl:representation"> -->
<!--                 </xsl:for-each> -->
<!--             </ul> -->
<!--         </p> -->
    </xsl:template>

    <xsl:template name="getType">
        <xsl:value-of select="@type" />
        <xsl:choose>
            <xsl:when test="@style = 'template'">
                <xsl:text> (path)</xsl:text>
            </xsl:when>
            <xsl:otherwise>
                <xsl:text> (</xsl:text>
                <xsl:value-of select="@style" />
                <xsl:text>)</xsl:text>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <xsl:template name="getTitle" match="wadl:doc" mode="title">
        <xsl:value-of select="normalize-space(substring-before(., '.'))" disable-output-escaping="yes" />
    </xsl:template>

    <xsl:template name="getDetail" match="wadl:doc" mode="detail">
        <xsl:value-of select="normalize-space(substring-after(., '.'))" disable-output-escaping="yes" />
    </xsl:template>

</xsl:stylesheet>
