/**
 *
 */
package fr.ge.tracker.facade.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Calendar;

import org.apache.commons.lang3.StringUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import fr.ge.tracker.bean.Reference;
import fr.ge.tracker.bean.UidNfo;
import fr.ge.tracker.facade.AbstractRestTest;
import fr.ge.tracker.facade.ITrackerFacade;

/**
 * @author Christian Cougourdan
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:spring/test-context.xml", "classpath*:spring/uid-service-cxf3-context.xml" })
public class SimpleTrackerFacadeImplTest extends AbstractRestTest {

    private static final String UID = "2016-11-ABC-DEF-42";

    private static final String REF = "2016-11-GHI-JKL-69";

    @Value("${mas.tracker.service.author:unknown}")
    private String author;

    @Autowired
    private ITrackerFacade facade;

    @Before
    public void setUp() throws Exception {
        reset(this.uidRestService, this.referenceRestService);
    }

    @Test
    public void testGetUid() throws Exception {
        final UidNfo expected = this.buildUidNfo();

        when(this.uidRestService.getUid(any())).thenReturn(expected);

        final UidNfo actual = this.facade.getUid(UID);

        verify(this.uidRestService).getUid(UID);

        assertNotNull(actual);
        assertEquals(expected.getValue(), actual.getValue());
    }

    @Test(expected = IllegalArgumentException.class)
    public void testGetUidMissingConf() throws Exception {
        final UidNfo actual = this.facade.getUid(null);
        assertNull(actual);
    }

    @Test
    public void testGetReference() throws Exception {
        final Reference expected = this.buildReference();

        when(this.referenceRestService.load(any())).thenReturn(expected);

        final Reference actual = this.facade.getReference(UID);

        verify(this.referenceRestService).load(UID);

        assertNotNull(actual);
        assertEquals(expected.getValue(), actual.getValue());
    }

    @Test
    public void testGetReferenceMissingConf() throws Exception {
        final Reference actual = this.facade.getReference(UID);
        assertNull(actual);
    }

    @Test
    public void testPost() throws Exception {
        final String expected = "ack";
        final String msg = "a new message";
        when(this.uidRestService.addMessage(any(), any(), any())).thenReturn(expected);

        final String actual = this.facade.post(UID, msg);

        verify(this.uidRestService).addMessage(UID, msg, this.author);
        assertEquals(expected, actual);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testPostMissingConf() throws Exception {
        final String actual = this.facade.post(StringUtils.EMPTY, StringUtils.EMPTY);
        assertNull(actual);
    }

    @Test
    public void testLink() throws Exception {
        final String expected = "ack";
        when(this.uidRestService.addReference(any(), any(), any())).thenReturn(expected);

        final String actual = this.facade.link(UID, REF);

        verify(this.uidRestService).addReference(REF, UID, this.author);
        assertEquals(expected, actual);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testLinkMissingConf() throws Exception {
        final String actual = this.facade.link(StringUtils.EMPTY, StringUtils.EMPTY);
        assertNull(actual);
    }

    @Test
    public void testCreateUid() throws Exception {
        final String userId = "2017-01-JON-DOE-42";
        when(this.uidRestService.createUid(this.author)).thenReturn(userId);

        final String actual = this.facade.createUid();

        verify(this.uidRestService).createUid(this.author);
        assertEquals(userId, actual);
    }

    @Test
    public void testCreateUidMissingConf() throws Exception {
        final String actual = this.facade.createUid();
        assertNull(actual);
    }

    @Test
    public void testGetOrCreate() throws Exception {
        final String username = "john.doe";
        final String userId = "2017-01-JON-DOE-42";
        when(this.uidRestService.getOrCreateUid(username, this.author)).thenReturn(userId);

        final String actual = this.facade.getOrCreate(username);

        verify(this.uidRestService).getOrCreateUid(username, this.author);
        assertEquals(userId, actual);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testGetOrCreateMissingConf() throws Exception {
        final String actual = this.facade.getOrCreate(StringUtils.EMPTY);
        assertNull(actual);
    }

    private UidNfo buildUidNfo() {
        final Calendar now = Calendar.getInstance();

        final UidNfo nfo = new UidNfo();
        nfo.setId(42L);
        nfo.setValue(UID);
        nfo.setClient(this.author);
        nfo.setCreated(now);

        return nfo;
    }

    private Reference buildReference() {
        final Calendar now = Calendar.getInstance();

        final Reference ref = new Reference();
        ref.setId(42L);
        ref.setValue(UID);
        ref.setClient(this.author);
        ref.setCreated(now);

        return ref;
    }
}
