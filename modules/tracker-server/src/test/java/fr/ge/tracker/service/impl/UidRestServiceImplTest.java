/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.ge.tracker.service.impl;

import static org.fest.assertions.Assertions.assertThat;
import static org.junit.Assert.fail;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.stream.Collectors;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MultivaluedHashMap;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.apache.cxf.jaxrs.impl.UriInfoImpl;
import org.apache.cxf.message.Message;
import org.apache.cxf.message.MessageImpl;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import fr.ge.tracker.bean.StructuredSearchResult;
import fr.ge.tracker.bean.UidNfo;
import fr.ge.tracker.core.exception.FunctionalException;
import fr.ge.tracker.core.exception.MissingParameterException;
import fr.ge.tracker.data.bean.dto.UidBean;
import fr.ge.tracker.data.bean.transverse.SearchResult;
import fr.ge.tracker.data.facade.ReferenceServiceFacade;
import fr.ge.tracker.data.service.IClientService;
import fr.ge.tracker.data.service.IMessageService;
import fr.ge.tracker.data.service.IUidService;
import fr.ge.tracker.server.constant.enumeration.ApiCodeEnum;
import fr.ge.tracker.server.service.v1.impl.UidRestServiceImpl;

/**
 * Tests {@link UidRestServiceImpl}.
 *
 * @author $Author: jpauchet $
 * @version $Revision: 0 $
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:spring/tracker-server-root-test.xml", "classpath:spring/mock-uid-factory-context.xml" })
public class UidRestServiceImplTest {
    /**
    *
    */
    private static final String AUTHOR = "Guichet Entreprises";

    private static final String RECORD_UID = "2020-01-REC-ORD-01";

    @Autowired
    private IUidService uidService;

    @Autowired
    private IMessageService messageService;

    @Autowired
    private IClientService clientService;

    @Autowired
    private ReferenceServiceFacade referenceServiceFacade;

    @Autowired
    private UidRestServiceImpl uidRestService;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        reset(this.referenceServiceFacade);
        reset(this.uidService);
        reset(this.messageService);
        reset(this.clientService);
    }

    /**
     * Tests create an uid.
     * 
     * @throws Exception
     *             The technical exception
     */
    @Test
    public void testCreateUid() throws Exception {
        // prepare
        when(this.referenceServiceFacade.create(any(String.class))).thenReturn("uid");

        // call
        final String id = this.uidRestService.createUid(AUTHOR);

        // verify
        verify(this.referenceServiceFacade).create(eq(AUTHOR));

        assertThat(id).isEqualTo("uid");
    }

    @Test
    public void testCreateUidMissingParameter() throws Exception {
        when(this.referenceServiceFacade.create(any(String.class))).thenThrow(new MissingParameterException("Missing author"));

        try {
            this.uidRestService.createUid(AUTHOR);
            fail("WebApplicationException expected");
        } catch (final WebApplicationException ex) {
            assertThat(ex.getResponse().getStatus()).isEqualTo(Status.BAD_REQUEST.getStatusCode());
            assertThat(ex.getResponse().getEntity()).isEqualTo("Missing author");
        }
    }

    @Test
    public void testCreateUidFunctionalException() throws Exception {
        when(this.referenceServiceFacade.create(any(String.class))).thenThrow(new FunctionalException("An error occured"));

        try {
            this.uidRestService.createUid(AUTHOR);
            fail("WebApplicationException expected");
        } catch (final WebApplicationException ex) {
            final Response response = ex.getResponse();
            assertThat(response.getStatus()).isEqualTo(Status.ACCEPTED.getStatusCode());
            assertThat(response.getEntity()).isEqualTo("An error occured");
        }
    }

    @Test
    public void testGetOrCreateUid() throws Exception {
        final String ref = "a-new-ref";
        final String uid = "2016-12-NEW-UID-42";

        when(this.referenceServiceFacade.getOrCreate(any(String.class), eq(AUTHOR))).thenReturn(uid);

        final String id = this.uidRestService.getOrCreateUid(ref, AUTHOR);

        verify(this.referenceServiceFacade).getOrCreate(eq(ref), eq(AUTHOR));
        assertThat(id).isEqualTo(uid);
    }

    @Test
    public void testGetOrCreateUidMissingParameter() throws Exception {
        final String ref = "a-new-ref";

        when(this.referenceServiceFacade.getOrCreate(any(String.class), eq(AUTHOR))).thenThrow(new MissingParameterException("Missing author"));

        try {
            this.uidRestService.getOrCreateUid(ref, AUTHOR);
            fail("WebApplicationException expected");
        } catch (final WebApplicationException ex) {
            final Response response = ex.getResponse();
            assertThat(response.getStatus()).isEqualTo(Status.BAD_REQUEST.getStatusCode());
            assertThat(response.getEntity()).isEqualTo("Missing author");
        }
    }

    @Test
    public void testGetOrCreateUidFunctionalException() throws Exception {
        final String ref = "a-new-ref";

        when(this.referenceServiceFacade.getOrCreate(any(String.class), eq(AUTHOR))).thenThrow(new FunctionalException("An error occured"));

        try {
            this.uidRestService.getOrCreateUid(ref, AUTHOR);
            fail("WebApplicationException expected");
        } catch (final WebApplicationException ex) {
            final Response response = ex.getResponse();
            assertThat(response.getStatus()).isEqualTo(Status.ACCEPTED.getStatusCode());
            assertThat(response.getEntity()).isEqualTo("An error occured");
        }
    }

    /**
     * Tests get an uid.
     * 
     * @throws Exception
     *             The technical exception
     */
    @Test
    public void testGetUid() throws Exception {
        final UidNfo loadedUid = new UidNfo();
        loadedUid.setId(1L);
        loadedUid.setValue("uid");

        // prepare
        when(this.referenceServiceFacade.load(any(String.class), eq(UidNfo.class))).thenReturn(loadedUid);

        // call
        final UidNfo actual = this.uidRestService.getUid("uid");

        // verify
        verify(this.referenceServiceFacade).load(eq("uid"), eq(UidNfo.class));
        assertThat(actual).isNotNull();
        assertThat(actual.getValue()).isEqualTo("uid");
    }

    @Test
    public void testGetUidUnknown() throws Exception {
        when(this.referenceServiceFacade.load(any(String.class), eq(UidNfo.class))).thenReturn(null);

        try {
            this.uidRestService.getUid("uid");
            fail("WebApplicationException expected");
        } catch (final WebApplicationException ex) {
            assertThat(ex.getResponse().getStatus()).isEqualTo(Status.ACCEPTED.getStatusCode());
            assertThat(ex.getResponse().getEntity()).isEqualTo(ApiCodeEnum.API_CODE_002.toString());
        }

        // verify
        verify(this.referenceServiceFacade).load(eq("uid"), eq(UidNfo.class));
    }

    /**
     * Tests gets an uid by reference.
     */
    @Test
    public void testGetUidByRef() {
        final UidNfo parentUid = new UidNfo();
        parentUid.setId(1L);
        parentUid.setValue("parent");

        // prepare
        when(this.referenceServiceFacade.loadParent(eq("ref"), eq(UidNfo.class))).thenReturn(parentUid);

        // call
        final UidNfo actual = this.uidRestService.getUidByRef("ref");

        // verify
        verify(this.referenceServiceFacade).loadParent(eq("ref"), eq(UidNfo.class));
        assertThat(actual).isNotNull();
        assertThat(actual.getValue()).isEqualTo("parent");
    }

    @Test
    public void testGetUidByRefUnknown() throws Exception {
        when(this.referenceServiceFacade.loadParent(any(String.class), eq(UidNfo.class))).thenReturn(null);

        try {
            this.uidRestService.getUidByRef("uid");
            fail("WebApplicationException expected");
        } catch (final WebApplicationException ex) {
            assertThat(ex.getResponse().getStatus()).isEqualTo(Status.ACCEPTED.getStatusCode());
            assertThat(ex.getResponse().getEntity()).isEqualTo(ApiCodeEnum.API_CODE_003.toString());
        }

        // verify
        verify(this.referenceServiceFacade).loadParent(eq("uid"), eq(UidNfo.class));
    }

    /**
     * Tests add reference with a reference which does not exist.
     * 
     * @throws Exception
     *             The technical exception
     */
    @Test
    public void testAddReferenceRefDoesNotExistAndUidExists() throws Exception {
        // prepare
        when(this.referenceServiceFacade.linkReferences(any(String.class), any(String.class), any(String.class))).thenReturn("Reference linked to this UID");

        // call
        final String actual = this.uidRestService.addReference("uid", "ref", AUTHOR);

        // verify
        assertThat(actual).isEqualTo("Reference linked to this UID");
        verify(this.referenceServiceFacade).linkReferences(eq("ref"), eq("uid"), eq(AUTHOR));
    }

    /**
     * Tests add reference with an uid which does not exist.
     * 
     * @throws Exception
     *             The technical exception
     */
    @Test
    public void testAddReferenceRefDoesNotExistAndUidDoesNotExist() throws Exception {
        // prepare
        when(this.referenceServiceFacade.linkReferences(any(String.class), any(String.class), any(String.class)))
                .thenReturn("Reference linked to this UID, which didn't exist and was created on the fly");

        // call
        final String actual = this.uidRestService.addReference("uid", "ref", AUTHOR);

        // verify
        assertThat(actual).isEqualTo("Reference linked to this UID, which didn't exist and was created on the fly");
        verify(this.referenceServiceFacade).linkReferences(eq("ref"), eq("uid"), eq(AUTHOR));
    }

    @Test
    public void testAddReferenceBadRequest() throws Exception {
        when(this.referenceServiceFacade.linkReferences(any(String.class), any(String.class), any(String.class))).thenThrow(new MissingParameterException("Oups : missing parameter"));

        try {
            this.uidRestService.addReference("uid", "ref", AUTHOR);
            fail("WebApplicationException expected");
        } catch (final WebApplicationException ex) {
            assertThat(ex.getResponse().getStatus()).isEqualTo(Status.BAD_REQUEST.getStatusCode());
            assertThat(ex.getResponse().getEntity()).isEqualTo("Oups : missing parameter");
        }

        verify(this.referenceServiceFacade).linkReferences(eq("ref"), eq("uid"), eq(AUTHOR));
    }

    @Test
    public void testAddReferenceFunctionalException() throws Exception {
        when(this.referenceServiceFacade.linkReferences(any(String.class), any(String.class), any(String.class))).thenThrow(new FunctionalException("Oups : functional error"));

        try {
            this.uidRestService.addReference("uid", "ref", AUTHOR);
            fail("WebApplicationException expected");
        } catch (final WebApplicationException ex) {
            assertThat(ex.getResponse().getStatus()).isEqualTo(Status.ACCEPTED.getStatusCode());
            assertThat(ex.getResponse().getEntity()).isEqualTo("Oups : functional error");
        }

        verify(this.referenceServiceFacade).linkReferences(eq("ref"), eq("uid"), eq(AUTHOR));
    }

    /**
     * Tests get references.
     */
    @Test
    public void testGetReferences() {
        final UidBean reference1Uid = new UidBean.Builder().id(1L).value("ref1").build();
        final UidBean reference2Uid = new UidBean.Builder().id(1L).value("ref2").build();
        final UidBean parentUid = new UidBean.Builder().id(1L).value("uid").references(Arrays.asList(reference1Uid, reference2Uid)).build();

        // prepare
        when(this.uidService.load(eq("uid"))).thenReturn(parentUid);
        final List<String> references = this.uidRestService.getReferences("uid");

        // call
        verify(this.uidService).load(eq("uid"));

        // verify
        assertThat(references).hasSize(2);
        assertThat(references).contains("ref1", "ref2");
    }

    /**
     * Tests add messages with existing uid.
     * 
     * @throws Exception
     *             The technical exception
     */
    @Test
    public void testAddMessageUidExists() throws Exception {
        // prepare
        when(this.referenceServiceFacade.postMessage(any(String.class), any(String.class), any(String.class))).thenReturn("Message created and linked to this reference");

        // call
        final String actual = this.uidRestService.addMessage("uid", "content", AUTHOR);

        // verify
        assertThat(actual).isEqualTo("Message created and linked to this reference");
        verify(this.referenceServiceFacade).postMessage(eq("uid"), eq("content"), eq(AUTHOR));
    }

    /**
     * Tests add messages with not existing uid.
     * 
     * @throws Exception
     *             The technical exception
     */
    @Test
    public void testAddMessageUidDoesNotExist() throws Exception {
        // prepare
        when(this.referenceServiceFacade.postMessage(any(String.class), any(String.class), any(String.class)))
                .thenReturn("Message created and linked to this reference, which didn't exist and was created on the fly");

        // call
        final String actual = this.uidRestService.addMessage("uid", "content", AUTHOR);

        // verify
        assertThat(actual).isEqualTo("Message created and linked to this reference, which didn't exist and was created on the fly");
        verify(this.referenceServiceFacade).postMessage(eq("uid"), eq("content"), eq(AUTHOR));
    }

    @Test
    public void testAddMessageBadRequest() throws Exception {
        when(this.referenceServiceFacade.postMessage(any(String.class), any(String.class), any(String.class))).thenThrow(new MissingParameterException("Oups : missing parameter"));

        try {
            this.uidRestService.addMessage("uid", "content", AUTHOR);
            fail("WebApplicationException expected");
        } catch (final WebApplicationException ex) {
            assertThat(ex.getResponse().getStatus()).isEqualTo(Status.BAD_REQUEST.getStatusCode());
            assertThat(ex.getResponse().getEntity()).isEqualTo("Oups : missing parameter");
        }

        verify(this.referenceServiceFacade).postMessage(eq("uid"), eq("content"), eq(AUTHOR));
    }

    @Test
    public void testAddMessageFunctionalException() throws Exception {
        when(this.referenceServiceFacade.postMessage(any(String.class), any(String.class), any(String.class))).thenThrow(new FunctionalException("Oups : functional error"));

        try {
            this.uidRestService.addMessage("uid", "content", AUTHOR);
            fail("WebApplicationException expected");
        } catch (final WebApplicationException ex) {
            assertThat(ex.getResponse().getStatus()).isEqualTo(Status.ACCEPTED.getStatusCode());
            assertThat(ex.getResponse().getEntity()).isEqualTo("Oups : functional error");
        }

        verify(this.referenceServiceFacade).postMessage(eq("uid"), eq("content"), eq(AUTHOR));
    }

    /**
     * Tests search data.
     * 
     * @throws Exception
     *             The technical exception
     */
    @Test
    public void testSearchUidData() throws Exception {
        final List<String> sorts = Arrays.asList("parent.value:desc");
        final Map<String, String> criteria = new HashMap<>();
        final String messageSearchValue = null;

        final SearchResult<UidNfo> searchResult = new SearchResult<>();
        final UidNfo uid1 = new UidNfo();
        uid1.setId(1L);
        uid1.setValue("val1");
        final UidNfo uid2 = new UidNfo();
        uid2.setId(2L);
        uid2.setValue("val2");
        searchResult.setContent(Arrays.asList(uid1, uid2));

        final MultivaluedMap<String, String> paths = new MultivaluedHashMap<>();
        paths.put("draw", Arrays.asList("1"));
        paths.put("columns[0][data]", Arrays.asList("value"));
        paths.put("columns[0][name]", Arrays.asList(""));
        paths.put("columns[0][searchable]", Arrays.asList("true"));
        paths.put("columns[0][orderable]", Arrays.asList("true"));
        paths.put("columns[0][search][value]", Arrays.asList(""));
        paths.put("columns[0][search][regex]", Arrays.asList("false"));
        paths.put("columns[1][data]", Arrays.asList("parent.value"));
        paths.put("columns[1][name]", Arrays.asList(""));
        paths.put("columns[1][searchable]", Arrays.asList("true"));
        paths.put("columns[1][orderable]", Arrays.asList("true"));
        paths.put("columns[1][search][value]", Arrays.asList(""));
        paths.put("columns[1][search][regex]", Arrays.asList("false"));
        paths.put("columns[2][data]", Arrays.asList("created"));
        paths.put("columns[2][name]", Arrays.asList(""));
        paths.put("columns[2][searchable]", Arrays.asList("true"));
        paths.put("columns[2][orderable]", Arrays.asList("true"));
        paths.put("columns[2][search][value]", Arrays.asList(""));
        paths.put("columns[2][search][regex]", Arrays.asList("false"));
        paths.put("order[0][column]", Arrays.asList("1"));
        paths.put("order[0][dir]", Arrays.asList("desc"));
        paths.put("start", Arrays.asList("0"));
        paths.put("length", Arrays.asList("10"));
        paths.put("search[value]", Arrays.asList(""));
        paths.put("search[regex]", Arrays.asList("false"));
        paths.put("_", Arrays.asList("1466496131182"));

        final List<String> params = new ArrayList<>();
        for (final Entry<String, List<String>> entry : paths.entrySet()) {
            for (final String value : entry.getValue()) {
                params.add(String.format("%s=%s", entry.getKey(), value));
            }
        }
        final Message m = new MessageImpl();
        m.put(Message.QUERY_STRING, params.stream().collect(Collectors.joining("&")));

        // prepare
        when(this.referenceServiceFacade.search(0, 10, sorts, criteria, messageSearchValue, UidNfo.class)).thenReturn(searchResult);

        // call
        final StructuredSearchResult<UidNfo> actual = this.uidRestService.searchUidData(new UriInfoImpl(m));

        // verify
        verify(this.referenceServiceFacade).search(0, 10, sorts, criteria, messageSearchValue, UidNfo.class);

        final StructuredSearchResult<UidNfo> structuredSearchResult = new StructuredSearchResult<>();
        structuredSearchResult.setData(Arrays.asList(uid1, uid2));
        structuredSearchResult.setDraw(1);
        structuredSearchResult.setRecordsFiltered(0);
        structuredSearchResult.setRecordsTotal(0);

        assertThat(actual.getData()).isEqualTo(structuredSearchResult.getData());
        assertThat(actual.getDraw()).isEqualTo(structuredSearchResult.getDraw());
        assertThat(actual.getRecordsFiltered()).isEqualTo(structuredSearchResult.getRecordsFiltered());
        assertThat(actual.getRecordsTotal()).isEqualTo(structuredSearchResult.getRecordsTotal());

    }

    @Test
    public void testRemoveUids() throws Exception {
        // -->prepare
        when(this.uidService.remove(Arrays.asList(RECORD_UID))).thenReturn(1);

        // -->call
        final Response actual = this.uidRestService.remove(Arrays.asList(RECORD_UID));

        // -->verify
        assertThat(actual.getStatus()).isEqualTo(Status.OK.getStatusCode());
        verify(this.uidService).remove(eq(Arrays.asList(RECORD_UID)));
    }
}
