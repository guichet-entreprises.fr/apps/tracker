/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.ge.tracker.service.impl;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import fr.ge.tracker.data.bean.dto.ClientBean;
import fr.ge.tracker.data.service.IClientService;
import fr.ge.tracker.server.service.v1.impl.ClientRestServiceImpl;

/**
 * Tests {@link ClientRestServiceImpl}.
 *
 * @author $Author: jpauchet $
 * @version $Revision: 0 $
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:spring/tracker-server-root-test.xml", "classpath:spring/mock-uid-factory-context.xml" })
public class ClientRestServiceImplTest {
    /** The author. */
    private static final String AUTHOR = "Guichet Entreprises";

    @Autowired
    private IClientService clientService;

    @Autowired
    private ClientRestServiceImpl clientRestService;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        reset(this.clientService);
    }

    /**
     * Tests get or build.
     * 
     * @throws Exception
     *             The technical exception
     */
    @Test
    public void testGetOrBuildClient() throws Exception {
        // prepare
        final ClientBean client = new ClientBean.Builder().name(AUTHOR).apiKey("apiKey").build();
        when(this.clientService.load(any(String.class))).thenReturn(null);
        when(this.clientService.persist(any(ClientBean.class))).thenReturn(client);

        // call
        final String id = this.clientRestService.getOrBuildClient(AUTHOR);

        // verify
        verify(this.clientService).load(any(String.class));
        verify(this.clientService).persist(any(ClientBean.class));
        assertThat(id).isEqualTo("apiKey");
    }

    /**
     * Tests get or build client already existing.
     * 
     * @throws Exception
     *             The technical exception
     */
    @Test
    public void testGetOrBuildClientAlreadyExisting() throws Exception {
        // prepare
        final ClientBean client = new ClientBean.Builder().name(AUTHOR).apiKey("apiKey").build();
        when(this.clientService.load(any(String.class))).thenReturn(client);

        // call
        final String id = this.clientRestService.getOrBuildClient(AUTHOR);

        // verify
        verify(this.clientService).load(any(String.class));
        verify(this.clientService, never()).persist(any(ClientBean.class));
        assertThat(id).isEqualTo("apiKey");
    }
}
