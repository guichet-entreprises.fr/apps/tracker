/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.ge.tracker.util;
/**
 *
 */

import java.util.Arrays;

import javax.ws.rs.core.MultivaluedHashMap;
import javax.ws.rs.core.MultivaluedMap;

import org.junit.Test;

import fr.ge.tracker.bean.StructuredSearchQuery;
import fr.ge.tracker.server.util.Instantiator;

/**
 * @author $Author: jpauchet $
 * @version $Revision: 0 $
 */
public class InstantiatorTest {

    @Test
    public void testInstantiate() {
        final MultivaluedMap<String, String> paths = new MultivaluedHashMap<>();
        paths.put("draw", Arrays.asList("1"));
        paths.put("columns[0][data]", Arrays.asList("value"));
        paths.put("columns[0][name]", Arrays.asList(""));
        paths.put("columns[0][searchable]", Arrays.asList("true"));
        paths.put("columns[0][orderable]", Arrays.asList("true"));
        paths.put("columns[0][search][value]", Arrays.asList(""));
        paths.put("columns[0][search][regex]", Arrays.asList("false"));
        paths.put("columns[1][data]", Arrays.asList("parent.value"));
        paths.put("columns[1][name]", Arrays.asList(""));
        paths.put("columns[1][searchable]", Arrays.asList("true"));
        paths.put("columns[1][orderable]", Arrays.asList("true"));
        paths.put("columns[1][search][value]", Arrays.asList(""));
        paths.put("columns[1][search][regex]", Arrays.asList("false"));
        paths.put("columns[2][data]", Arrays.asList("created"));
        paths.put("columns[2][name]", Arrays.asList(""));
        paths.put("columns[2][searchable]", Arrays.asList("true"));
        paths.put("columns[2][orderable]", Arrays.asList("true"));
        paths.put("columns[2][search][value]", Arrays.asList(""));
        paths.put("columns[2][search][regex]", Arrays.asList("false"));
        paths.put("order[0][column]", Arrays.asList("1"));
        paths.put("order[0][dir]", Arrays.asList("desc"));
        paths.put("start", Arrays.asList("0"));
        paths.put("length", Arrays.asList("10"));
        paths.put("search[value]", Arrays.asList(""));
        paths.put("search[regex]", Arrays.asList("false"));
        paths.put("_", Arrays.asList("1466496131182"));
        Instantiator.instantiate(StructuredSearchQuery.class, paths);
    }

}
