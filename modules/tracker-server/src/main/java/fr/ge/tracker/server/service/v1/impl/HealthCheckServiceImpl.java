/**
 * 
 */
package fr.ge.tracker.server.service.v1.impl;

import javax.ws.rs.Path;

import fr.ge.common.utils.service.IHealthCheckService;
import io.swagger.annotations.Api;

/**
 * Tracker managed API.
 * 
 * @author $Author: aolubi $
 * @version $Revision: 0 $
 */
@Api(value = "Tracker health check services")
@Path("/")
public class HealthCheckServiceImpl implements IHealthCheckService {

}
