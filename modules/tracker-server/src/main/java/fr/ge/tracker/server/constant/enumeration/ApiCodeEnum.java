/**
 * 
 */
package fr.ge.tracker.server.constant.enumeration;

/**
 * API codes.
 * 
 * @author $Author: jpauchet $
 * @version $Revision: 0 $
 */
public enum ApiCodeEnum {

  /** API_CODE_001. */
  API_CODE_001(1, "Missing author"),

  /** API_CODE_002. */
  API_CODE_002(2, "No matching data found"),

  /** API_CODE_003. */
  API_CODE_003(3, "No matching data found"),

  /** API_CODE_004. */
  API_CODE_004(4, "Missing author"),

  /** API_CODE_005. */
  API_CODE_005(5, "Reference linked to this UID"),

  /** API_CODE_006. */
  API_CODE_006(6, "Reference linked to this UID, which didn't exist and was created on the fly"),

  /** API_CODE_007. */
  API_CODE_007(7, "Reference already linked to another UID and this UID doesn't exist"),

  /** API_CODE_008. */
  API_CODE_008(8, "Reference already linked to another UID and this UID exists"),

  /** API_CODE_009. */
  API_CODE_009(9, "Reference already linked to this UID"),

  /** API_CODE_010. */
  API_CODE_010(10, "Missing content"),

  /** API_CODE_011. */
  API_CODE_011(11, "Missing author"),

  /** API_CODE_012. */
  API_CODE_012(12, "Message created and linked to this reference"),

  /** API_CODE_013. */
  API_CODE_013(13, "Message created and linked to this reference, which didn't exist and was created on the fly");

  /** Code. */
  private int code;

  /** Message. */
  private String message;

  /**
   * Constructor of the class.
   * 
   * @param code
   *          the code
   * @param message
   *          the message
   */
  ApiCodeEnum(final int code, final String message) {
    this.code = code;
    this.message = message;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public String toString() {
    return message;
  }

}
