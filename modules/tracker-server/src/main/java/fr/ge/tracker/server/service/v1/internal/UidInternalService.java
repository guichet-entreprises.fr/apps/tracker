/**
 * 
 */
package fr.ge.tracker.server.service.v1.internal;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.ge.tracker.bean.Message;
import fr.ge.tracker.bean.UidNfo;
import fr.ge.tracker.core.exception.ApiPermissionException;
import fr.ge.tracker.core.exception.FunctionalException;
import fr.ge.tracker.core.exception.MissingParameterException;
import fr.ge.tracker.data.bean.dto.UidBean;
import fr.ge.tracker.data.facade.ReferenceServiceFacade;
import fr.ge.tracker.data.service.IUidService;
import fr.ge.tracker.server.constant.enumeration.ApiCodeEnum;

/**
 * @author $Author: aolubi $
 * @version $Revision: 0 $
 */
@Service
public class UidInternalService {

    /** The UID service. */
    @Autowired
    private IUidService uidService;

    /** The reference service facade. */
    @Autowired
    private ReferenceServiceFacade referenceServiceFacade;

    /**
     * Create an uid.
     * 
     * @param author
     *            The author
     * @return The uid
     * @throws WebApplicationException
     *             The web application exception
     */
    public String createUid(final String author) throws WebApplicationException {
        try {
            final String uid = this.referenceServiceFacade.create(author);
            return uid;
        } catch (final MissingParameterException ex) {
            throw new WebApplicationException(Response.status(Status.BAD_REQUEST).entity(ex.getMessage()).build());
        } catch (final ApiPermissionException ex) {
            throw new WebApplicationException(Response.status(Status.FORBIDDEN).entity(ex.getMessage()).build());
        } catch (final FunctionalException ex) {
            throw new WebApplicationException(Response.status(Status.ACCEPTED).entity(ex.getMessage()).build());
        }
    }

    /**
     * Gets an uid info.
     * 
     * @param value
     *            The reference as a value
     * @return the uid information
     */
    public UidNfo getUid(final String value) {
        final UidNfo bean = this.referenceServiceFacade.load(value, UidNfo.class);

        if (bean == null) {
            throw new WebApplicationException(Response.status(Status.ACCEPTED).entity(ApiCodeEnum.API_CODE_002.toString()).build());
        }

        bean.setMessages(this.referenceServiceFacade.loadMessages(bean.getId(), Message.class));

        return bean;
    }

    /**
     * Add a reference.
     * 
     * @param uid
     *            the uid identifier
     * @param ref
     *            the reference to link to
     * @param author
     *            the author
     * @return the uid for the reference
     * @throws WebApplicationException
     *             The web application exception
     */
    public String addReference(final String uid, final String ref, final String author) throws WebApplicationException {
        try {
            return this.referenceServiceFacade.linkReferences(ref, uid, author);
        } catch (final MissingParameterException ex) {
            throw new WebApplicationException(Response.status(Status.BAD_REQUEST).entity(ex.getMessage()).build());
        } catch (final ApiPermissionException ex) {
            throw new WebApplicationException(Response.status(Status.FORBIDDEN).entity(ex.getMessage()).build());
        } catch (final FunctionalException ex) {
            throw new WebApplicationException(Response.status(Status.ACCEPTED).entity(ex.getMessage()).build());
        }
    }

    /**
     * Gets the references from an uid.
     * 
     * @param uid
     *            the uid as identifier
     * @return the references as list
     */
    public List<String> getReferences(final String uid) {
        final List<String> references = new ArrayList<>();
        final UidBean uidBean = this.uidService.load(uid);
        if (uidBean != null) {
            for (final UidBean ref : uidBean.getReferences()) {
                references.add(ref.getValue());
            }
        }
        return references;
    }

    /**
     * Add a message.
     * 
     * @param uid
     *            The uid as a reference
     * @param content
     *            the message content
     * @param author
     *            the author
     * @return the result
     * @throws WebApplicationException
     *             The web application exception
     */
    public String addMessage(final String uid, final String content, final String author) throws WebApplicationException {
        try {
            return this.referenceServiceFacade.postMessage(uid, content, author);
        } catch (final MissingParameterException ex) {
            throw new WebApplicationException(Response.status(Status.BAD_REQUEST).entity(ex.getMessage()).build());
        } catch (final ApiPermissionException ex) {
            throw new WebApplicationException(Response.status(Status.FORBIDDEN).entity(ex.getMessage()).build());
        } catch (final FunctionalException ex) {
            throw new WebApplicationException(Response.status(Status.ACCEPTED).entity(ex.getMessage()).build());
        }
    }

    /**
     * Remove the references.
     * 
     * @param uids
     *            the references to remove
     * @return the removal status
     */
    public Response remove(final List<String> uids) {
        if (CollectionUtils.isEmpty(uids)) {
            return Response.noContent().build();
        }
        int deleted = this.uidService.remove(uids);
        if (deleted > 0) {
            return Response.ok().build();
        }

        return Response.notModified().build();
    }
}
