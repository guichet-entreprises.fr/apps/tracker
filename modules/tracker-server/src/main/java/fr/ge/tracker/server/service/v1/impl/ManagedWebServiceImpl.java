/**
 * 
 */
package fr.ge.tracker.server.service.v1.impl;

import java.util.List;

import javax.ws.rs.Path;

import org.springframework.beans.factory.annotation.Autowired;

import fr.ge.common.utils.service.IHealthCheckService;
import fr.ge.tracker.bean.UidNfo;
import fr.ge.tracker.server.service.v1.internal.UidInternalService;
import fr.ge.tracker.service.v1.IManagedWebService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

/**
 * Tracker managed API.
 * 
 * @author $Author: aolubi $
 * @version $Revision: 0 $
 */
@Api(value = "UID REST Services")
@Path("/v1/uid")
public class ManagedWebServiceImpl implements IManagedWebService, IHealthCheckService {

    @Autowired
    private UidInternalService internalService;

    /**
     * {@inheritDoc}
     */
    @Override
    @ApiOperation(value = "Create a new UID", notes = "**Response** :\n" + //
            "\n" + //
            "- **Status 200** - text/plain - UID has been created\n" + //
            "\n" + //
            "**Behaviour** :\n" + //
            "\n" + //
            "* If no **author** is provided :\n" + //
            "    * **Status 400** is returned with message \"Missing author\"")
    @ApiResponses({ //
            @ApiResponse(code = 200, message = "UID has been created"), //
            @ApiResponse(code = 400, message = "Missing author") //
    })
    public String createUid(@ApiParam(value = "identity of the entity sending this request", required = true) final String author) {
        return this.internalService.createUid(author);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @ApiOperation(value = "Information for a specific reference", notes = "**Response** :\n" + //
            "\n" + //
            "- **Status 200** - application/json - Information about a reference.\n" + //
            "\n" + //
            "**Behaviour** :\n" + //
            "\n" + //
            "* If no content is found :\n" + //
            "    * **Status 202** is returned with message \"No matching data found\"\n" + //
            "\n" + //
            "**Example**\n" + //
            "\n" + //
            "    Request : GET http://tracker-server-url/api/v1/uid/U78010019435\n" + //
            "    Answer :\n" + //
            "    {\n" + //
            "        \"value\": \"U78010019435\",\n" + //
            "        \"created\": 1463738071235,\n" + //
            "        \"client\": {\n" + //
            "            \"created\": 1463673051028,\n" + //
            "            \"name\": \"Guichet entreprises\"\n" + //
            "        },\n" + //
            "        \"parent\": null,\n" + //
            "        \"references\": [\n" + //
            "            {\n" + //
            "                \"value\": \"2016-04-AAA-AAA-69\",\n" + //
            "                \"created\": 1463739523361,\n" + //
            "                \"client\": {\n" + //
            "                    \"created\": 1463673051028,\n" + //
            "                    \"name\": \"Guichet entreprises\"\n" + //
            "                },\n" + //
            "                \"parent\": \"2016-04-AAA-AAA-69\",\n" + //
            "                \"references\": [],\n" + //
            "                \"messages\": []\n" + //
            "            },\n" + //
            "            {\n" + //
            "                \"value\": \"2016-04-BBB-BBB-42\",\n" + //
            "                \"created\": 1463739523362,\n" + //
            "                \"client\": {\n" + //
            "                    \"created\": 1463673051028,\n" + //
            "                    \"name\": \"Guichet entreprises\"\n" + //
            "                },\n" + //
            "                \"parent\": \"2016-04-AAA-AAA-69\",\n" + //
            "                \"references\": [],\n" + //
            "                \"messages\": []\n" + //
            "            }\n" + //
            "        ],\n" + //
            "        \"messages\": [\n" + //
            "            {\n" + //
            "                \"created\": 1463739545898,\n" + //
            "                \"content\": \"UID has been created\"\n" + //
            "                \"client\": {\n" + //
            "                    \"created\": 1463673051028,\n" + //
            "                    \"name\": \"tracker\"\n" + //
            "                }\n" + //
            "            },\n" + //
            "            {\n" + //
            "                \"created\": 1463739545899,\n" + //
            "                \"content\": \"Record step 1 has been validated\",\n" + //
            "                \"client\": {\n" + //
            "                    \"created\": 1463673051029,\n" + //
            "                    \"name\": \"form-manager\"\n" + //
            "                }\n" + //
            "            }\n" + //
            "        ]\n" + //
            "    }" //
    )
    @ApiResponses({ //
            @ApiResponse(code = 200, message = "Information about a reference"), //
            @ApiResponse(code = 202, message = "No matching data found") //
    })
    public UidNfo getUid(@ApiParam("UID or reference to retrieve information from") final String value) {
        return this.internalService.getUid(value);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @ApiOperation(value = "Link between references", notes = "**Response** :\n" + //
            "\n" + //
            "- **Status 204** - application/json - References are linked\n" + //
            "\n" + //
            "**Examples**\n" + //
            "\n" + //
            "    Request : POST http://tracker-server-url/api/v1/uid/2016-06-NFK-NZE-30/ref/U78010019435?author=Guichet Entreprises\n" + //
            "    Answer : Reference linked to this UID\n" + //
            "\n" + //
            "    Request : POST http://tracker-server-url/api/v1/uid/U78010019435/ref/H12345678900?author=Guichet Entreprises\n" + //
            "    Answer : Reference linked to this UID\n" + //
            "\n" + //
            "    Request : POST http://tracker-server-url/api/v1/uid/U78010019435/ref/H12345678900?author=Guichet Entreprises\n" + //
            "    Answer : Reference already linked to this UID\n" + //
            "\n" + //
            "    Request : POST http://tracker-server-url/api/v1/uid/2016-06-NFK-NZE-30/ref/H12345678900?author=Guichet Entreprises\n" + //
            "    Answer : Reference already linked to another UID and this UID exists\n" + //
            "\n" + //
            "    Request : POST http://tracker-server-url/api/v1/uid/not existing UID/ref/H12345678900?author=Guichet Entreprises\n" + //
            "    Answer : Reference reversed linked to UID, which didn't exist and was created on the fly\n" + //
            "\n" + //
            "    Request : POST http://tracker-server-url/api/v1/uid/not existing UID bis/ref/new ref?author=Guichet Entreprises\n" + //
            "    Answer : Reference linked to this UID, which didn't exist and was created on the fly\n" + //
            "\n" + //
            "**Behaviour** :\n" + //
            "\n" + //
            "* If no **author** is provided :\n" + //
            "    * **Status 400** is returned with message \"Missing author\" and the algorithm stops here\n" + //
            "\n" + //
            "\n" + //
            "* If **ref** does not exist and **uid** does not exist :\n" + //
            "    * An UID **newUid** is created\n" + //
            "    * The reference **uid** is created and linked to the UID **newUid**\n" + //
            "    * The reference **ref** is created and linked to the reference **uid**\n" + //
            "    * **Status 200** is returned with message \"Reference linked to this UID, which didn't exist and was created on the fly\"\n" + //
            "\n" + //
            "\n" + //
            "* If **ref** does not exist and **uid** exists :\n" + //
            "    * The reference **ref** is created and linked to the UID **uid**\n" + //
            "    * **Status 200** is returned with message \"Reference linked to this UID\"\n" + //
            "\n" + //
            "\n" + //
            "* If **ref** exists and **uid** does not exist :\n" + //
            "    * Reverse link between **ref** and **uid**\n" + //
            "    * **Status 200** is returned with message \"Reference reversed linked to UID, which didn't exist and was created on the fly\"\n" + //
            "\n" + //
            "\n" + //
            "* If **ref** exists and **uid** exists, **ref** is not linked to **uid**, and has no parent :\n" + //
            "    * The reference **ref** is linked to the UID **uid**\n" + //
            "    * **Status 200** is returned with message \"Reference linked to this UID\"\n" + //
            "\n" + //
            "\n" + //
            "* If **ref** exists and **uid** exists but **ref** is not linked to **uid** :\n" + //
            "    * **Status 202** is returned with message \"Reference already linked to another UID and this UID exists\"\n" + //
            "\n" + //
            "\n" + //
            "* If **ref** exists and **uid** exists and **ref** is linked to **uid** :\n" + //
            "    * **Status 200** is returned with message \"Reference already linked to this UID\"\n" + //
            "\n" + //
            "![Link process](../images/uml/rest-api-link.png)" //
    )
    public String addReference( //
            @ApiParam("UID or reference to link to") final String uid, //
            @ApiParam("UID or reference to link") final String ref, //
            @ApiParam(value = "identity of the entity sending this request", required = true) final String author //
    ) {
        return this.internalService.addReference(uid, ref, author);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @ApiOperation("List all references linked to a specific UID")
    public List<String> getReferences(@ApiParam("UID to retrieve linked references") final String uid) {
        return this.internalService.getReferences(uid);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @ApiOperation(value = "Post a new message", notes = "**Response** :\n" + //
            "\n" + //
            "- **Status 204** - application/json - Message created and linked with specific UID reference.\n" + //
            "\n" + //
            "**Example**\n" + //
            "\n" + //
            "    Request : POST http://tracker-server-url/api/v1/uid/U78010019435/msg?content=File sent to partner&author=Guichet Entreprises\n" + //
            "    Answer : Message created and linked to this reference\n" + //
            "\n" + //
            "    Request : POST http://tracker-server-url/api/v1/uid/not existing UID/msg?content=File sent to partner&author=Guichet Entreprises\n" + //
            "    Answer : Message created and linked to this reference, which didn't exist and was created on the fly\n" + //
            "\n" + //
            "**Behaviour** :\n" + //
            "\n" + //
            "* If no **content** is provided :\n" + //
            "    * **Status 400** is returned with message \"Missing content\" and the algorithm stops here\n" + //
            "\n" + //
            "* If no **author** is provided :\n" + //
            "    * **Status 400** is returned with message \"Missing author\" and the algorithm stops here\n" + //
            "\n" + //
            "* If **ref** does not exist :\n" + //
            "    * An UID **uid** is created\n" + //
            "    * The reference **ref** is created and linked to the UID **uid**\n" + //
            "    * A message is created and linked to the reference **ref**\n" + //
            "    * **Status 200** is returned with message \"Message created and linked to this reference, which didn't exist and was created on the fly\"\n" + //
            "\n" + //
            "* If **ref** exists :\n" + //
            "    * A message is created and linked to the reference **ref**\n" + //
            "    * **Status 200** is returned with message \"Message created and linked to this reference\"\n" + //
            "\n" + //
            "![Post message](../images/uml/rest-api-post-message.png)" //
    )
    public String addMessage(@ApiParam("UID or reference to link the message to") final String uid, //
            @ApiParam("content of the message") final String content, //
            @ApiParam(value = "identity of the entity sending this request", required = true) final String author //
    ) {
        return this.internalService.addMessage(uid, content, author);
    }

}
