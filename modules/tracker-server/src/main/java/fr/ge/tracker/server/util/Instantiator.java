/**
 *
 */
package fr.ge.tracker.server.util;

import java.beans.IntrospectionException;
import java.beans.PropertyDescriptor;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.util.ArrayList;
import java.util.List;
import java.util.Map.Entry;

import javax.ws.rs.core.MultivaluedMap;

import org.apache.commons.lang3.math.NumberUtils;
import org.apache.commons.lang3.reflect.ConstructorUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ge.tracker.core.util.CoreUtil;

/**
 * Utility class to instantiate objects hierarchies.
 *
 * @author $Author: jpauchet $
 * @version $Revision: 0 $
 */
public final class Instantiator {

    /** Logger. */
    private static final Logger LOGGER = LoggerFactory.getLogger(Instantiator.class);

    /**
     * Constructor.
     */
    private Instantiator() {
        // Nothing to do.
    }

    /**
     * Gets a constructed type.
     *
     * @param <T>
     *            the type to instantiate
     * @param type
     *            the new type
     * @return the constructed type
     */
    private static <T> T getConstructedType(final Class<T> type) {
        T constructedType = null;
        final Constructor<?> constructor = ConstructorUtils.getMatchingAccessibleConstructor(type);
        if (constructor != null) {
            try {
                constructedType = CoreUtil.cast(constructor.newInstance());
            } catch (final InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
                LOGGER.debug("could not map the HTTP request to the criteria", e);
            }
        }
        return constructedType;
    }

    /**
     * Converts a string value.
     *
     * @param type
     *            the new type
     * @param value
     *            the value
     * @return the converted value
     */
    private static Object convert(final Class<?> type, final String value) {
        Object convertedValue = value;
        if (Boolean.class.equals(type)) {
            convertedValue = Boolean.valueOf(value);
        } else if (Integer.class.equals(type)) {
            convertedValue = Integer.valueOf(value);
        } else if (Long.class.equals(type)) {
            convertedValue = Long.valueOf(value);
        }
        return convertedValue;
    }

    /**
     * Recursively instantiates an objects hierarchy.
     *
     * @param curObj
     *            the current object
     * @param curParameter
     *            the current object parameter
     * @param pathSplit
     *            the splitted path
     * @param index
     *            the current index
     * @param value
     *            the value to set at the end
     */
    private static void instantiate(final Object curObj, final Class<?> curParameter, final String[] pathSplit, final int index, final String value) {
        try {
            final String curPath = pathSplit[index];
            final boolean isLastPathPart = index + 1 == pathSplit.length;
            Object nextObject = null;
            Class<?> nextParameter = null;
            if (List.class.isAssignableFrom(curObj.getClass())) {
                final List<Object> list = CoreUtil.cast(curObj);
                if (isLastPathPart) {
                    // add element to a list of primitive types
                    list.add(value);
                } else if (NumberUtils.isDigits(curPath)) {
                    final int listIndex = Integer.valueOf(curPath);
                    if (listIndex < list.size()) {
                        // retrieve element if list[i] exists
                        nextObject = list.get(listIndex);
                    } else {
                        // create element if list[i] does not exist
                        nextObject = getConstructedType(curParameter);
                        if (nextObject != null) {
                            list.add(nextObject);
                        }
                    }
                    // evaluate next path part
                    instantiate(nextObject, null, pathSplit, index + 1, value);
                }
            } else {
                final PropertyDescriptor propertyDescriptor = new PropertyDescriptor(curPath, curObj.getClass());
                final Method getter = propertyDescriptor.getReadMethod();
                if (getter != null) {
                    nextObject = getter.invoke(curObj);
                    final Class<?> getterReturnType = getter.getReturnType();
                    if (List.class.equals(getterReturnType)) {
                        // search parameter type for evaluating next path part
                        // for List<String>, nextObject is a List instance and
                        // nextParameter is String.class
                        final ParameterizedType parameterizedType = (ParameterizedType) curObj.getClass().getDeclaredField(curPath).getGenericType();
                        nextParameter = (Class<?>) parameterizedType.getActualTypeArguments()[0];
                    }
                    if (nextObject == null) {
                        // next time we evaluate a similar hierarchy, the object
                        // value will be retrievable
                        final Method setter = propertyDescriptor.getWriteMethod();
                        if (setter != null) {
                            if (List.class.equals(getterReturnType)) {
                                // instantiate a list attribute of the object
                                nextObject = getConstructedType(ArrayList.class);
                            } else if (!isLastPathPart) {
                                // instantiate a complex type attribute of the
                                // object
                                nextObject = getConstructedType(getterReturnType);
                            } else {
                                // set a primitive type attribute of the object,
                                // at the end of an object hierarchy
                                nextObject = convert(getterReturnType, value);
                            }
                            if (nextObject != null) {
                                setter.invoke(curObj, nextObject);
                            }
                        }
                    }
                    if (nextObject != null && !isLastPathPart) {
                        // evaluate next path part
                        instantiate(nextObject, nextParameter, pathSplit, index + 1, value);
                    }
                }
            }
        } catch (final IntrospectionException | IllegalAccessException | IllegalArgumentException | InvocationTargetException | NoSuchFieldException | SecurityException e) {
            LOGGER.debug("could not map the HTTP request to the criteria", e);
        }
    }

    /**
     * Instantiates an objects hierarchy.
     *
     * @param <T>
     *            the type to instantiate
     * @param clazz
     *            the class
     * @param paths
     *            the paths
     * @return the instantiated object hierarchy
     */
    public static <T> T instantiate(final Class<T> clazz, final MultivaluedMap<String, String> paths) {
        try {
            final T root = getConstructedType(clazz);
            if (root != null) {
                for (final Entry<String, List<String>> entry : paths.entrySet()) {
                    // ][ ou ]. ou [ ou ] ou .
                    final String[] pathSplit = entry.getKey().split("\\]\\[|\\]\\.|\\[|\\]|\\.");
                    if (entry.getValue() != null && !entry.getValue().isEmpty()) {
                        instantiate(root, null, pathSplit, 0, entry.getValue().get(0));
                    }
                }
                return root;
            }
        } catch (final IllegalArgumentException e) {
            LOGGER.warn("Error while instanciate object hierarchy", e);
        }
        return null;
    }

}
