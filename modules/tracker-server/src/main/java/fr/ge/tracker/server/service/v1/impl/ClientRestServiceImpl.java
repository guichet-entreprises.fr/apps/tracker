package fr.ge.tracker.server.service.v1.impl;

import javax.ws.rs.Path;

import org.apache.commons.codec.digest.DigestUtils;
import org.springframework.beans.factory.annotation.Autowired;

import fr.ge.tracker.data.bean.dto.ClientBean;
import fr.ge.tracker.data.service.IClientService;
import fr.ge.tracker.service.v1.IClientRestService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

/**
 * Clients management service.
 *
 * @author $Author: jpauchet $
 * @version $Revision: 0 $
 */
@Path("/private/v1/client")
@Api(value = "Client REST Services", hidden = true)
public class ClientRestServiceImpl implements IClientRestService {

    /** The client service. */
    @Autowired
    private IClientService clientService;

    /**
     * Constructor.
     */
    public ClientRestServiceImpl() {
        // Nothing to do
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @ApiOperation(value = "Gets an API key from a client name. Registers the client if it is unknown.")
    public String getOrBuildClient(@ApiParam(value = "readable name of a client which should be able to use Tracker", required = true) final String author) {
        final String apiKey = DigestUtils.sha1Hex(author);
        ClientBean client = this.clientService.load(apiKey);

        if (client == null) {
            client = new ClientBean.Builder().name(author).apiKey(apiKey).build();
            client = this.clientService.persist(client);
        }

        return client.getApiKey();
    }

}
