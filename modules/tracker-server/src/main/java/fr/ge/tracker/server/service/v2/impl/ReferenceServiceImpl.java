package fr.ge.tracker.server.service.v2.impl;

import java.util.List;
import java.util.Map;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.springframework.beans.factory.annotation.Autowired;

import fr.ge.common.utils.bean.search.SearchQueryFilter;
import fr.ge.common.utils.bean.search.SearchQueryOrder;
import fr.ge.common.utils.bean.search.SearchResult;
import fr.ge.tracker.bean.Message;
import fr.ge.tracker.bean.Reference;
import fr.ge.tracker.data.facade.ReferenceServiceFacade;
import fr.ge.tracker.service.v2.IMessageService;
import fr.ge.tracker.service.v2.IReferenceService;

/**
 *
 * @author Christian Cougourdan
 */
public class ReferenceServiceImpl implements IReferenceService, IMessageService {

    @Autowired
    private ReferenceServiceFacade referenceServiceFacade;

    @Override
    public Reference load(final String key) {
        final Reference bean = this.referenceServiceFacade.load(key, Reference.class);

        if (null == bean) {
            throw new WebApplicationException( //
                    Response.status(Status.ACCEPTED) //
                            .entity("No content found") //
                            .build() //
            );
        }

        return bean;
    }

    @Override
    public SearchResult<Reference> search(final long startIndex, final long maxResults, final List<SearchQueryFilter> filters, final List<SearchQueryOrder> orders) {
        final List<String> sorts = null;
        final Map<String, String> criteria = null;
        final String messageSearchValue = null;

        final fr.ge.tracker.data.bean.transverse.SearchResult<Reference> searchResult = this.referenceServiceFacade.search(startIndex, //
                maxResults, //
                sorts, //
                criteria, //
                messageSearchValue, //
                Reference.class //
        );

        final SearchResult<Reference> result = new SearchResult<>(startIndex, maxResults);
        result.setTotalResults(searchResult.getTotalResults());
        result.setContent(searchResult.getContent());

        return result;
    }

    // @Override
    // public SearchResult<Message> searchMessage(final String key, final long
    // startIndex, final long maxResults, final List<SearchQueryOrder> orders) {
    // final fr.ge.tracker.data.bean.transverse.SearchResult<Message>
    // internalSearchResult = this.referenceServiceFacade.searchMessages( //
    // startIndex, //
    // maxResults, //
    // key, //
    // null == orders ? null :
    // orders.stream().map(SearchQueryOrder::toString).collect(Collectors.toList()),
    // //
    // Message.class //
    // );
    //
    // final SearchResult<Message> result = new SearchResult<>(startIndex,
    // maxResults);
    // result.setTotalResults(internalSearchResult.getTotalResults());
    // result.setContent(internalSearchResult.getContent());
    //
    // return result;
    // }

    /**
     * {@inheritDoc}
     */
    @Override
    public SearchResult<Message> searchMessage(final long startIndex, final long maxResults, final List<SearchQueryFilter> filters, final List<SearchQueryOrder> orders) {
        return this.referenceServiceFacade.searchMessages( //
                startIndex, //
                maxResults, //
                filters, //
                orders, //
                Message.class //
        );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Response removeMessages(final String key, final String terms) {
        final long deleted = this.referenceServiceFacade.removeMessages( //
                key, //
                terms //
        );

        if (deleted > 0) {
            return Response.ok().build();
        }

        return Response.status(Status.NOT_MODIFIED).build();
    }

}
