package fr.ge.tracker.server.service.v1.impl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.ws.rs.Path;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.core.UriInfo;

import org.springframework.beans.factory.annotation.Autowired;

import fr.ge.tracker.bean.Message;
import fr.ge.tracker.bean.StructuredSearchQuery;
import fr.ge.tracker.bean.StructuredSearchQueryOrder;
import fr.ge.tracker.bean.StructuredSearchResult;
import fr.ge.tracker.bean.UidNfo;
import fr.ge.tracker.core.exception.ApiPermissionException;
import fr.ge.tracker.core.exception.FunctionalException;
import fr.ge.tracker.core.exception.MissingParameterException;
import fr.ge.tracker.data.bean.transverse.SearchResult;
import fr.ge.tracker.data.facade.ReferenceServiceFacade;
import fr.ge.tracker.server.constant.enumeration.ApiCodeEnum;
import fr.ge.tracker.server.service.v1.internal.UidInternalService;
import fr.ge.tracker.server.util.Instantiator;
import fr.ge.tracker.service.v1.IUidRestService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

/**
 * Service providing an API in order for ids to be used in different
 * applications.
 *
 * @author $Author: jpauchet $
 * @version $Revision: 0 $
 */
@Path("/v1/uid")
@Api(value = "UID REST Services")
public class UidRestServiceImpl implements IUidRestService {

    /** Message. */
    private static final String MESSAGES = "messages";

    /** The reference service facade. */
    @Autowired
    private ReferenceServiceFacade referenceServiceFacade;

    @Autowired
    private UidInternalService internalService;

    /**
     * Constructor.
     */
    public UidRestServiceImpl() {
        // Nothing to do
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @ApiOperation(value = "Create a new UID", notes = "**Response** :\n" + //
            "\n" + //
            "- **Status 200** - text/plain - UID has been created\n" + //
            "\n" + //
            "**Behaviour** :\n" + //
            "\n" + //
            "* If no **author** is provided :\n" + //
            "    * **Status 400** is returned with message \"Missing author\"")
    @ApiResponses({ //
            @ApiResponse(code = 200, message = "UID has been created"), //
            @ApiResponse(code = 400, message = "Missing author") //
    })
    public String createUid(@ApiParam(value = "identity of the entity sending this request", required = true) final String author) {
        return this.internalService.createUid(author);
    }

    @Override
    @ApiOperation(value = "Retrieve an UID or create it", notes = "**Response** :\n" + //
            "\n" + //
            "- **Status 200** - text/plain - UID has been created\n" + //
            "\n" + //
            "**Behaviour** :\n" + //
            "\n" + //
            "* If no **author** is provided :\n" + //
            "    * **Status 400** is returned with message \"Missing author\"")
    @ApiResponses({ //
            @ApiResponse(code = 200, message = "UID found or has been created"), //
            @ApiResponse(code = 400, message = "Missing author") //
    })
    public String getOrCreateUid(@ApiParam("UID or reference to retrieve or create") final String ref, //
            @ApiParam(value = "identity of the entity sending this request", required = true) final String author //
    ) {
        try {
            return this.referenceServiceFacade.getOrCreate(ref, author);
        } catch (final MissingParameterException ex) {
            throw new WebApplicationException(Response.status(Status.BAD_REQUEST).entity(ex.getMessage()).build());
        } catch (final ApiPermissionException ex) {
            throw new WebApplicationException(Response.status(Status.FORBIDDEN).entity(ex.getMessage()).build());
        } catch (final FunctionalException ex) {
            throw new WebApplicationException(Response.status(Status.ACCEPTED).entity(ex.getMessage()).build());
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @ApiOperation(value = "", notes = "**Response** :\n" + //
            "\n" + //
            "- **Status 200** - application/json - The searched UID are returned.\n" + //
            "\n" + //
            "**Behaviour** :\n" + //
            "\n" + //
            "* Searches and returns the matching UIDs. The search criteria are cumulative.\n" + //
            "\n" + //
            "* If search value is \"empty-mode:true\"\n" + //
            "    * The search will be performed to check if the column which name was provided is empty.\n" + //
            "\n" + //
            "\n" + //
            "* If search value is like \"date-range-mode:DD/MM/YYYY:DD/MM/YYYY\"\n" + //
            "    * The search will be performed to check if the column which name was provided is a date between the 2 provided dates\n" + //
            "\n" + //
            "* In other cases\n" + //
            "    * The search will be performed to check if the column which name was provided contains the provided string\n" + //
            "\n" + //
            "**Example**\n" + //
            "\n" + //
            "    Request : GET http://tracker-server-url/api/v1/uid/search/data\n" + //
            "        &columns[0][data]=value   &columns[0][search][value]=48\n" + //
            "        &columns[1][data]=parent  &columns[1][search][value]=empty-mode:true\n" + //
            "        &columns[2][data]=created &columns[2][search][value]=date-range-mode:10/06/2016:06/07/2016\n" + //
            "        &draw=6                   &length=10\n" + //
            "        &order[0][column]=2       &order[0][dir]=desc\n" + //
            "        &start=0\n" + //
            "\n" + //
            "    Answer :\n" + //
            "    {\n" + //
            "        \"draw\": 6,\n" + //
            "        \"recordsTotal\": 2,\n" + //
            "        \"recordsFiltered\": 2,\n" + //
            "        \"data\": [\n" + //
            "            {\n" + //
            "                \"value\": \"2016-07-KCH-ZNY-48\",\n" + //
            "                \"client\": \"Guichet Entreprises\",\n" + //
            "                \"created\": 1467619509662,\n" + //
            "                \"references\": [],\n" + //
            "                \"parent\": \"\",\n" + //
            "                \"messages\": null\n" + //
            "            },\n" + //
            "            {\n" + //
            "                \"value\": \"2016-06-BKP-CCX-48\",\n" + //
            "                \"client\": \"Guichet Entreprises\",\n" + //
            "                \"created\": 1467619509762,\n" + //
            "                \"references\": [],\n" + //
            "                \"parent\": \"\",\n" + //
            "                \"messages\": null\n" + //
            "            }\n" + //
            "        ]\n" + //
            "    }" //
    )
    public StructuredSearchResult<UidNfo> searchUidData(final UriInfo uriInfo) {
        final StructuredSearchQuery criteria = Instantiator.instantiate(StructuredSearchQuery.class, uriInfo.getQueryParameters());

        final StructuredSearchResult<UidNfo> datatableSearchResult = new StructuredSearchResult<>(criteria.getDraw());

        List<String> orders;
        if (null == criteria.getOrder()) {
            orders = null;
        } else {
            orders = new ArrayList<>();
            for (final StructuredSearchQueryOrder criteriaOrder : criteria.getOrder()) {
                orders.add(criteria.getColumns().get(criteriaOrder.getColumn()).getData() + ":" + criteriaOrder.getDir());
            }
        }

        final SearchResult<UidNfo> searchResult = this.referenceServiceFacade.search((long) criteria.getStart() / criteria.getLength(), criteria.getLength(), orders, criteria.toCriteriaMap(),
                criteria.findCustomSearchValue(MESSAGES), UidNfo.class);

        datatableSearchResult.setRecordsFiltered((int) searchResult.getTotalResults());
        datatableSearchResult.setRecordsTotal((int) searchResult.getTotalResults());

        if (null != searchResult.getContent()) {
            datatableSearchResult.setData(searchResult.getContent());
        } else {
            datatableSearchResult.setData(Collections.<UidNfo>emptyList());
        }

        return datatableSearchResult;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @ApiOperation(value = "Information for a specific reference", notes = "**Response** :\n" + //
            "\n" + //
            "- **Status 200** - application/json - Information about a reference.\n" + //
            "\n" + //
            "**Behaviour** :\n" + //
            "\n" + //
            "* If no content is found :\n" + //
            "    * **Status 202** is returned with message \"No matching data found\"\n" + //
            "\n" + //
            "**Example**\n" + //
            "\n" + //
            "    Request : GET http://tracker-server-url/api/v1/uid/U78010019435\n" + //
            "    Answer :\n" + //
            "    {\n" + //
            "        \"value\": \"U78010019435\",\n" + //
            "        \"created\": 1463738071235,\n" + //
            "        \"client\": {\n" + //
            "            \"created\": 1463673051028,\n" + //
            "            \"name\": \"Guichet entreprises\"\n" + //
            "        },\n" + //
            "        \"parent\": null,\n" + //
            "        \"references\": [\n" + //
            "            {\n" + //
            "                \"value\": \"2016-04-AAA-AAA-69\",\n" + //
            "                \"created\": 1463739523361,\n" + //
            "                \"client\": {\n" + //
            "                    \"created\": 1463673051028,\n" + //
            "                    \"name\": \"Guichet entreprises\"\n" + //
            "                },\n" + //
            "                \"parent\": \"2016-04-AAA-AAA-69\",\n" + //
            "                \"references\": [],\n" + //
            "                \"messages\": []\n" + //
            "            },\n" + //
            "            {\n" + //
            "                \"value\": \"2016-04-BBB-BBB-42\",\n" + //
            "                \"created\": 1463739523362,\n" + //
            "                \"client\": {\n" + //
            "                    \"created\": 1463673051028,\n" + //
            "                    \"name\": \"Guichet entreprises\"\n" + //
            "                },\n" + //
            "                \"parent\": \"2016-04-AAA-AAA-69\",\n" + //
            "                \"references\": [],\n" + //
            "                \"messages\": []\n" + //
            "            }\n" + //
            "        ],\n" + //
            "        \"messages\": [\n" + //
            "            {\n" + //
            "                \"created\": 1463739545898,\n" + //
            "                \"content\": \"UID has been created\"\n" + //
            "                \"client\": {\n" + //
            "                    \"created\": 1463673051028,\n" + //
            "                    \"name\": \"tracker\"\n" + //
            "                }\n" + //
            "            },\n" + //
            "            {\n" + //
            "                \"created\": 1463739545899,\n" + //
            "                \"content\": \"Record step 1 has been validated\",\n" + //
            "                \"client\": {\n" + //
            "                    \"created\": 1463673051029,\n" + //
            "                    \"name\": \"form-manager\"\n" + //
            "                }\n" + //
            "            }\n" + //
            "        ]\n" + //
            "    }" //
    )
    @ApiResponses({ //
            @ApiResponse(code = 200, message = "Information about a reference"), //
            @ApiResponse(code = 202, message = "No matching data found") //
    })
    public UidNfo getUid(@ApiParam("UID or reference to retrieve information from") final String value) {
        return this.internalService.getUid(value);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @ApiOperation(value = "Information for a reference tree", notes = "**Response** :\n" + //
            "\n" + //
            "- **Status 200** - application/json - Information about a reference.\n" + //
            "\n" + //
            "**Behaviour** :\n" + //
            "\n" + //
            "* If no content is found :\n" + //
            "    * **Status 202** is returned with message \"No matching data found\"\n" + //
            "\n" + //
            "**Example**\n" + //
            "\n" + //
            "    Request : GET http://tracker-server-url/api/v1/uid/U78010019435/parent\n" + //
            "    Answer :\n" + //
            "    {\n" + //
            "        \"value\": \"U78010019435\",\n" + //
            "        \"created\": 1463738071235,\n" + //
            "        \"client\": {\n" + //
            "            \"created\": 1463673051028,\n" + //
            "            \"name\": \"Guichet entreprises\"\n" + //
            "        },\n" + //
            "        \"parent\": null,\n" + //
            "        \"references\": [\n" + //
            "            {\n" + //
            "                \"value\": \"2016-04-AAA-AAA-69\",\n" + //
            "                \"created\": 1463739523361,\n" + //
            "                \"client\": {\n" + //
            "                    \"created\": 1463673051028,\n" + //
            "                    \"name\": \"Guichet entreprises\"\n" + //
            "                },\n" + //
            "                \"parent\": \"2016-04-AAA-AAA-69\",\n" + //
            "                \"references\": [],\n" + //
            "                \"messages\": []\n" + //
            "            },\n" + //
            "            {\n" + //
            "                \"value\": \"2016-04-BBB-BBB-42\",\n" + //
            "                \"created\": 1463739523362,\n" + //
            "                \"client\": {\n" + //
            "                    \"created\": 1463673051028,\n" + //
            "                    \"name\": \"Guichet entreprises\"\n" + //
            "                },\n" + //
            "                \"parent\": \"2016-04-AAA-AAA-69\",\n" + //
            "                \"references\": [],\n" + //
            "                \"messages\": []\n" + //
            "            }\n" + //
            "        ],\n" + //
            "        \"messages\": [\n" + //
            "            {\n" + //
            "                \"created\": 1463739545898,\n" + //
            "                \"content\": \"UID has been created\"\n" + //
            "                \"client\": {\n" + //
            "                    \"created\": 1463673051028,\n" + //
            "                    \"name\": \"tracker\"\n" + //
            "                }\n" + //
            "            },\n" + //
            "            {\n" + //
            "                \"created\": 1463739545899,\n" + //
            "                \"content\": \"Record step 1 has been validated\",\n" + //
            "                \"client\": {\n" + //
            "                    \"created\": 1463673051029,\n" + //
            "                    \"name\": \"form-manager\"\n" + //
            "                }\n" + //
            "            }\n" + //
            "        ]\n" + //
            "    }" //
    )
    @ApiResponses({ //
            @ApiResponse(code = 200, message = "Information about a reference"), //
            @ApiResponse(code = 202, message = "No matching data found") //
    })
    public UidNfo getUidByRef(@ApiParam("UID or reference to retrieve information from") final String value) {
        final UidNfo bean = this.referenceServiceFacade.loadParent(value, UidNfo.class);

        if (bean == null) {
            throw new WebApplicationException(Response.status(Status.ACCEPTED).entity(ApiCodeEnum.API_CODE_003.toString()).build());
        }

        bean.setMessages(this.referenceServiceFacade.loadMessages(bean.getId(), Message.class));

        return bean;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @ApiOperation(value = "Link between references", notes = "**Response** :\n" + //
            "\n" + //
            "- **Status 204** - application/json - References are linked\n" + //
            "\n" + //
            "**Examples**\n" + //
            "\n" + //
            "    Request : POST http://tracker-server-url/api/v1/uid/2016-06-NFK-NZE-30/ref/U78010019435?author=Guichet Entreprises\n" + //
            "    Answer : Reference linked to this UID\n" + //
            "\n" + //
            "    Request : POST http://tracker-server-url/api/v1/uid/U78010019435/ref/H12345678900?author=Guichet Entreprises\n" + //
            "    Answer : Reference linked to this UID\n" + //
            "\n" + //
            "    Request : POST http://tracker-server-url/api/v1/uid/U78010019435/ref/H12345678900?author=Guichet Entreprises\n" + //
            "    Answer : Reference already linked to this UID\n" + //
            "\n" + //
            "    Request : POST http://tracker-server-url/api/v1/uid/2016-06-NFK-NZE-30/ref/H12345678900?author=Guichet Entreprises\n" + //
            "    Answer : Reference already linked to another UID and this UID exists\n" + //
            "\n" + //
            "    Request : POST http://tracker-server-url/api/v1/uid/not existing UID/ref/H12345678900?author=Guichet Entreprises\n" + //
            "    Answer : Reference reversed linked to UID, which didn't exist and was created on the fly\n" + //
            "\n" + //
            "    Request : POST http://tracker-server-url/api/v1/uid/not existing UID bis/ref/new ref?author=Guichet Entreprises\n" + //
            "    Answer : Reference linked to this UID, which didn't exist and was created on the fly\n" + //
            "\n" + //
            "**Behaviour** :\n" + //
            "\n" + //
            "* If no **author** is provided :\n" + //
            "    * **Status 400** is returned with message \"Missing author\" and the algorithm stops here\n" + //
            "\n" + //
            "\n" + //
            "* If **ref** does not exist and **uid** does not exist :\n" + //
            "    * An UID **newUid** is created\n" + //
            "    * The reference **uid** is created and linked to the UID **newUid**\n" + //
            "    * The reference **ref** is created and linked to the reference **uid**\n" + //
            "    * **Status 200** is returned with message \"Reference linked to this UID, which didn't exist and was created on the fly\"\n" + //
            "\n" + //
            "\n" + //
            "* If **ref** does not exist and **uid** exists :\n" + //
            "    * The reference **ref** is created and linked to the UID **uid**\n" + //
            "    * **Status 200** is returned with message \"Reference linked to this UID\"\n" + //
            "\n" + //
            "\n" + //
            "* If **ref** exists and **uid** does not exist :\n" + //
            "    * Reverse link between **ref** and **uid**\n" + //
            "    * **Status 200** is returned with message \"Reference reversed linked to UID, which didn't exist and was created on the fly\"\n" + //
            "\n" + //
            "\n" + //
            "* If **ref** exists and **uid** exists, **ref** is not linked to **uid**, and has no parent :\n" + //
            "    * The reference **ref** is linked to the UID **uid**\n" + //
            "    * **Status 200** is returned with message \"Reference linked to this UID\"\n" + //
            "\n" + //
            "\n" + //
            "* If **ref** exists and **uid** exists but **ref** is not linked to **uid** :\n" + //
            "    * **Status 202** is returned with message \"Reference already linked to another UID and this UID exists\"\n" + //
            "\n" + //
            "\n" + //
            "* If **ref** exists and **uid** exists and **ref** is linked to **uid** :\n" + //
            "    * **Status 200** is returned with message \"Reference already linked to this UID\"\n" + //
            "\n" + //
            "![Link process](../images/uml/rest-api-link.png)" //
    )
    public String addReference( //
            @ApiParam("UID or reference to link to") final String uid, //
            @ApiParam("UID or reference to link") final String ref, //
            @ApiParam(value = "identity of the entity sending this request", required = true) final String author //
    ) {
        return this.internalService.addReference(uid, ref, author);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @ApiOperation("List all references linked to a specific UID")
    public List<String> getReferences(@ApiParam("UID to retrieve linked references") final String uid) {
        return this.internalService.getReferences(uid);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @ApiOperation(value = "Post a new message", notes = "**Response** :\n" + //
            "\n" + //
            "- **Status 204** - application/json - Message created and linked with specific UID reference.\n" + //
            "\n" + //
            "**Example**\n" + //
            "\n" + //
            "    Request : POST http://tracker-server-url/api/v1/uid/U78010019435/msg?content=File sent to partner&author=Guichet Entreprises\n" + //
            "    Answer : Message created and linked to this reference\n" + //
            "\n" + //
            "    Request : POST http://tracker-server-url/api/v1/uid/not existing UID/msg?content=File sent to partner&author=Guichet Entreprises\n" + //
            "    Answer : Message created and linked to this reference, which didn't exist and was created on the fly\n" + //
            "\n" + //
            "**Behaviour** :\n" + //
            "\n" + //
            "* If no **content** is provided :\n" + //
            "    * **Status 400** is returned with message \"Missing content\" and the algorithm stops here\n" + //
            "\n" + //
            "* If no **author** is provided :\n" + //
            "    * **Status 400** is returned with message \"Missing author\" and the algorithm stops here\n" + //
            "\n" + //
            "* If **ref** does not exist :\n" + //
            "    * An UID **uid** is created\n" + //
            "    * The reference **ref** is created and linked to the UID **uid**\n" + //
            "    * A message is created and linked to the reference **ref**\n" + //
            "    * **Status 200** is returned with message \"Message created and linked to this reference, which didn't exist and was created on the fly\"\n" + //
            "\n" + //
            "* If **ref** exists :\n" + //
            "    * A message is created and linked to the reference **ref**\n" + //
            "    * **Status 200** is returned with message \"Message created and linked to this reference\"\n" + //
            "\n" + //
            "![Post message](../images/uml/rest-api-post-message.png)" //
    )
    public String addMessage(@ApiParam("UID or reference to link the message to") final String uid, //
            @ApiParam("content of the message") final String content, //
            @ApiParam(value = "identity of the entity sending this request", required = true) final String author //
    ) {
        return this.internalService.addMessage(uid, content, author);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @ApiOperation("Remove all UIDs")
    public Response remove(@ApiParam("UIDs to remove") final List<String> uids) {
        return this.internalService.remove(uids);
    }

}
