/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.ge.tracker.core.util;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The Class CoreUtil.
 *
 * @author Christian Cougourdan
 */
public final class CoreUtil {

    /** The Constant LOGGER. */
    private static final Logger LOGGER = LoggerFactory.getLogger(CoreUtil.class);

    /** The Constant STREAM_BUFFER_SIZE. */
    private static final int STREAM_BUFFER_SIZE = 32 * 1024;

    /** The Constant DEFAULT_CHARSET. */
    private static final Charset DEFAULT_CHARSET = StandardCharsets.UTF_8;

    /**
     * Instantiates a new core util.
     */
    private CoreUtil() {
        // Nothing to do
    }

    /**
     * Resource as bytes.
     *
     * @param resourceName
     *            the resource name
     * @return the byte[]
     */
    public static byte[] resourceAsBytes(final String resourceName) {
        return resourceAsBytes(resourceName, CoreUtil.class);
    }

    /**
     * Resource as bytes.
     *
     * @param resourceName
     *            the resource name
     * @param fromClass
     *            the from class
     * @return the byte[]
     */
    public static byte[] resourceAsBytes(final String resourceName, final Class<?> fromClass) {
        try (InputStream in = fromClass.getResourceAsStream(resourceName)) {
            return read(in);
        } catch (final IOException ex) {
            LOGGER.warn("Unable to close input resource stream '{}'.", resourceName, ex);
        }

        return new byte[] {};
    }

    /**
     * Resource as string.
     *
     * @param resourceName
     *            the resource name
     * @param fromClass
     *            the from class
     * @return the string
     */
    public static String resourceAsString(final String resourceName, final Class<?> fromClass) {
        return resourceAsString(resourceName, fromClass, DEFAULT_CHARSET);
    }

    /**
     * Resource as string.
     *
     * @param resourceName
     *            the resource name
     * @param fromClass
     *            the from class
     * @param charset
     *            the charset
     * @return the string
     */
    public static String resourceAsString(final String resourceName, final Class<?> fromClass, final Charset charset) {
        return new String(resourceAsBytes(resourceName, fromClass), charset);
    }

    /**
     * Read all bytes from an input stream.
     *
     * @param in
     *            stream to read
     * @return stream content as byte array
     */
    public static byte[] read(final InputStream in) {
        byte[] result = new byte[] {};

        if (null == in) {
            return result;
        }

        try (ByteArrayOutputStream out = new ByteArrayOutputStream()) {
            final byte[] buffer = new byte[STREAM_BUFFER_SIZE];
            int len;
            while ((len = in.read(buffer)) > 0) {
                out.write(buffer, 0, len);
            }

            result = out.toByteArray();
        } catch (final IOException ex) {
            LOGGER.warn("Unable to close buffer output stream.", ex);
        }

        return result;
    }

    /**
     * Cast any object to expected returned type.
     *
     * @param <T>
     *            the generic type
     * @param o
     *            object to cast
     * @return casted object
     */
    @SuppressWarnings("unchecked")
    public static <T> T cast(final Object o) {
        return (T) o;
    }

}
