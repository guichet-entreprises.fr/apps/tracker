/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.ge.tracker.data.service.impl;

import static org.fest.assertions.Assertions.assertThat;

import java.util.Arrays;

import org.dbunit.dataset.ITable;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import fr.ge.tracker.data.bean.dto.ClientBean;
import fr.ge.tracker.data.bean.transverse.SearchResult;
import fr.ge.tracker.data.service.IClientService;
import fr.ge.tracker.test.util.AbstractDbTest;

/**
 * Tests {@link ClientServiceImpl}.
 *
 * @author $Author: jpauchet $
 * @version $Revision: 0 $
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:spring/tracker-core-data-root-test.xml" })
public class ClientServiceImplTest extends AbstractDbTest {

    @Autowired
    private IClientService service;

    @Before
    public void setUp() throws Exception {
    }

    /**
     * Tests persist.
     * 
     * @throws Exception
     *             the technical Exception
     */
    @Test
    public void testPersist() throws Exception {
        initDb(getClass().getSimpleName() + "-dataset.xml");
        final ClientBean bean = service.persist(new ClientBean.Builder().apiKey("new apiKey").name("new name").build());
        assertThat(bean).isNotNull();
        assertThat(bean.getId()).isNotNull();
        assertThat(bean.getCreated()).isNotNull();
        assertThat(bean.getUpdated()).isNotNull();
        final ITable tbl = extract("client");
        assertThat(tbl.getRowCount()).isEqualTo(4);
    }

    /**
     * Tests persist.
     * 
     * @throws Exception
     *             the technical Exception
     */
    @Test
    public void testUpdate() throws Exception {
        initDb(getClass().getSimpleName() + "-dataset.xml");
        ClientBean bean = service.load(11111111);
        final Long oldId = bean.getId();
        bean.setApiKey("apiKey updated");
        bean = service.persist(bean);
        assertThat(bean.getId()).isEqualTo(oldId);
    }

    /**
     * Tests search.
     * 
     * @throws Exception
     *             the technical Exception
     */
    @Test
    public void testSearchEmpty() throws Exception {
        initDb(getClass().getSimpleName() + "-dataset.xml");
        final SearchResult<ClientBean> searchResult = service.search(1, 10);
        assertThat(searchResult).isNotNull();
        assertThat(searchResult.getStartIndex()).isEqualTo(1);
        assertThat(searchResult.getMaxResults()).isEqualTo(10);
        assertThat(searchResult.getTotalResults()).isEqualTo(3);
        assertThat(searchResult.getContent()).isNotNull();
        assertThat(searchResult.getContent()).hasSize(0);
    }

    /**
     * Tests search.
     * 
     * @throws Exception
     *             the technical Exception
     */
    @Test
    public void testSearch() throws Exception {
        initDb(getClass().getSimpleName() + "-dataset.xml");
        SearchResult<ClientBean> searchResult = service.search(0, 2);
        assertThat(searchResult).isNotNull();
        assertThat(searchResult.getStartIndex()).isEqualTo(0);
        assertThat(searchResult.getMaxResults()).isEqualTo(2);
        assertThat(searchResult.getTotalResults()).isEqualTo(3);
        assertThat(searchResult.getContent()).isNotNull();
        assertThat(searchResult.getContent()).hasSize(2);

        searchResult = service.search(1, 2);
        assertThat(searchResult).isNotNull();
        assertThat(searchResult.getStartIndex()).isEqualTo(1);
        assertThat(searchResult.getMaxResults()).isEqualTo(2);
        assertThat(searchResult.getTotalResults()).isEqualTo(3);
        assertThat(searchResult.getContent()).isNotNull();
        assertThat(searchResult.getContent()).hasSize(1);
    }

    /**
     * Tests search.
     * 
     * @throws Exception
     *             the technical Exception
     */
    @Test
    public void testSearchSort() throws Exception {
        initDb(getClass().getSimpleName() + "-dataset.xml");
        SearchResult<ClientBean> searchResult = service.search(0, 10, Arrays.asList("apiKey:desc"));
        assertThat(searchResult.getContent()).onProperty("apiKey").containsExactly("GHI", "DEF", "ABC");

        searchResult = service.search(0, 10, Arrays.asList("apiKey:asc"));
        assertThat(searchResult.getContent()).onProperty("apiKey").containsExactly("ABC", "DEF", "GHI");

        searchResult = service.search(0, 10, Arrays.asList("apiKey"));
        assertThat(searchResult.getContent()).onProperty("apiKey").containsExactly("ABC", "DEF", "GHI");
    }

    /**
     * Tests load.
     * 
     * @throws Exception
     *             the technical Exception
     */
    @Test
    public void testLoad() throws Exception {
        initDb(getClass().getSimpleName() + "-dataset.xml");
        final ClientBean bean = service.load(11111111);
        assertThat(bean).isNotNull();
        assertThat(bean.getApiKey()).isEqualTo("ABC");
    }

    /**
     * Tests load.
     * 
     * @throws Exception
     *             the technical Exception
     */
    @Test
    public void testLoadUnknown() throws Exception {
        initDb(getClass().getSimpleName() + "-dataset.xml");
        final ClientBean bean = service.load(42);
        assertThat(bean).isNull();
    }

    /**
     * Tests load with API key.
     * 
     * @throws Exception
     *             the technical Exception
     */
    @Test
    public void testLoadFromApiKey() throws Exception {
        initDb(getClass().getSimpleName() + "-dataset.xml");
        final ClientBean bean = service.load("ABC");
        assertThat(bean).isNotNull();
        assertThat(bean.getId()).isEqualTo(11111111L);
    }

    /**
     * Tests load with unknown API key.
     * 
     * @throws Exception
     */
    @Test
    public void testLoadUnknownFromApiKey() throws Exception {
        initDb(getClass().getSimpleName() + "-dataset.xml");
        final ClientBean bean = service.load("xxx");
        assertThat(bean).isNull();
    }

    /**
     * Tests remove.
     * 
     * @throws Exception
     *             the technical Exception
     */
    @Test
    public void testRemove() throws Exception {
        initDb(getClass().getSimpleName() + "-dataset.xml");
        service.remove(11111113);
        final ITable tbl = extract("client");
        assertThat(tbl.getRowCount()).isEqualTo(2);
    }

    /**
     * Tests remove.
     * 
     * @throws Exception
     *             the technical Exception
     */
    @Test
    public void testRemoveUnknown() throws Exception {
        initDb(getClass().getSimpleName() + "-dataset.xml");
        service.remove(301);
        final ITable tbl = extract("client");
        assertThat(tbl.getRowCount()).isEqualTo(3);
    }

}
