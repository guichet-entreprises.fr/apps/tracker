/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.ge.tracker.data.facade;

import static org.fest.assertions.Assertions.assertThat;
import static org.junit.Assert.fail;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import fr.ge.tracker.core.exception.ApiPermissionException;
import fr.ge.tracker.core.exception.FunctionalException;
import fr.ge.tracker.core.exception.MissingParameterException;
import fr.ge.tracker.data.bean.dto.ClientBean;
import fr.ge.tracker.data.bean.dto.MessageBean;
import fr.ge.tracker.data.bean.dto.UidBean;
import fr.ge.tracker.data.bean.transverse.SearchResult;
import fr.ge.tracker.data.service.IClientService;
import fr.ge.tracker.data.service.IMessageService;
import fr.ge.tracker.data.service.IUidSearchService;
import fr.ge.tracker.data.service.IUidService;
import fr.ge.tracker.data.util.UidFactoryImpl;

/**
 * Tests {@link referenceServiceFacadeImpl}.
 *
 * @author $Author: jpauchet $
 * @version $Revision: 0 $
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:spring/mock-service-context.xml", "classpath:spring/mock-uid-factory-context.xml", "classpath:spring/facade-context.xml" })
public class ReferenceServiceFacadeTest {

    /** API key. */
    private static final String API_KEY = "apiKey";

    /** Author. */
    private static final String AUTHOR = "Guichet Entreprises";

    @Autowired
    private UidFactoryImpl uidFactory;

    @Autowired
    private IUidService uidService;

    @Autowired
    private IUidSearchService uidSearchService;

    @Autowired
    private IMessageService messageService;

    @Autowired
    private IClientService clientService;

    @Autowired
    private ReferenceServiceFacade referenceServiceFacade;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        reset(this.uidFactory, this.uidService, this.messageService, this.clientService);
    }

    /**
     * Tests {@link ReferenceServiceFacadeImpl#createUid()}.
     * 
     * @throws Exception
     *             the technical Exception
     */
    @Ignore("To be fixed")
    @Test
    public void testCreateUid() throws Exception {
        final ClientBean client = new ClientBean.Builder().name(AUTHOR).apiKey(API_KEY).build();
        final UidBean uidToCreate = new UidBean.Builder().value("uid").client(client).build();
        final UidBean createdUid = new UidBean.Builder().id(1L).value("uid").client(client).build();
        final MessageBean messageToCreate = new MessageBean.Builder().content("Création de l'UID \"uid\"").uid(createdUid).client(client).build();

        // prepare
        when(this.uidFactory.uid()).thenReturn("uid");
        when(this.uidService.load(any(String.class))).thenReturn(null, createdUid);
        when(this.clientService.load(any(String.class))).thenReturn(client);
        when(this.messageService.persist(any(MessageBean.class))).thenReturn(messageToCreate);

        // call
        final String id = this.referenceServiceFacade.create(API_KEY);

        // verify
        verify(this.uidFactory).uid();
        verify(this.uidService, times(2)).load(eq("uid"));
        verify(this.uidService).persist(eq(uidToCreate));
        verify(this.messageService).persist(eq(messageToCreate));
        verify(this.clientService, times(2)).load(eq(API_KEY));
        assertThat(id).isEqualTo("uid");
    }

    @Test
    public void testCreateNoAuthor() throws Exception {
        try {
            this.referenceServiceFacade.create(null);
            fail("MissingParameterException expected");
        } catch (final MissingParameterException ex) {
            assertThat(ex.getMessage()).isEqualTo("Missing author");
        }
    }

    @Ignore("To be fixed")
    @Test
    public void testCreateInvalidApiKey() throws Exception {
        try {
            this.referenceServiceFacade.create("Toto");
            fail("ApiPermissionException expected");
        } catch (final ApiPermissionException ex) {
            assertThat(ex.getMessage()).isEqualTo("Invalid API key");
        }
    }

    @Test
    public void testGetOrCreateUidNewRef() throws Exception {
        final ClientBean client = new ClientBean.Builder().name(AUTHOR).apiKey(API_KEY).build();
        final UidBean createdUid = new UidBean.Builder().id(1L).value("2017-01-NEW-UID-42").build();

        when(this.clientService.load(any(String.class))).thenReturn(client);
        when(this.uidFactory.uid()).thenReturn(createdUid.getValue());
        when(this.uidService.load(eq("new-ref"))).thenReturn(null);
        when(this.uidService.load(eq(createdUid.getValue()))).thenReturn(null, createdUid);

        final String actual = this.referenceServiceFacade.getOrCreate("new-ref", API_KEY);

        assertThat(actual).isEqualTo(createdUid.getValue());
    }

    @Test
    public void testGetOrCreateUidExistingRef() throws Exception {
        final UidBean existingUid = new UidBean.Builder().id(1L).value("2017-01-PRE-UID-42").build();
        final UidBean existingRef = new UidBean.Builder().id(2L).value("2017-01-PRE-REF-42").parent(existingUid).build();

        when(this.uidService.load(eq(existingRef.getValue()))).thenReturn(existingRef);
        when(this.uidService.load(eq(existingUid.getValue()))).thenReturn(existingUid);

        final String actual = this.referenceServiceFacade.getOrCreate(existingRef.getValue(), AUTHOR);

        assertThat(actual).isEqualTo(existingUid.getValue());
    }

    @Test
    public void testGetOrCreateNoAuthor() throws Exception {
        try {
            this.referenceServiceFacade.getOrCreate("new-ref", null);
            fail("MissingParameterException expected");
        } catch (final MissingParameterException ex) {
            assertThat(ex.getMessage()).isEqualTo("Missing author");
        }
    }

    @Ignore("To be fixed")
    @Test
    public void testGetOrCreateInvalidApiKey() throws Exception {
        try {
            this.referenceServiceFacade.getOrCreate("new-ref", "Toto");
            fail("ApiPermissionException expected");
        } catch (final ApiPermissionException ex) {
            assertThat(ex.getMessage()).isEqualTo("Invalid API key");
        }
    }

    /**
     * Tests load.
     */
    @Test
    public void testGetUid() {
        final UidBean loadedUid = new UidBean();
        loadedUid.setId(1L);
        loadedUid.setValue("uid");

        // prepare
        when(this.uidService.load(any(String.class), eq(UidBean.class))).thenReturn(loadedUid);

        // call
        final UidBean actual = this.referenceServiceFacade.load("uid", UidBean.class);

        // verify
        verify(this.uidService).load(eq("uid"), eq(UidBean.class));
        assertThat(actual).isNotNull();
        assertThat(actual.getValue()).isEqualTo("uid");
    }

    /**
     * Tests testGetUidByRef.
     */
    @Test
    public void testGetUidByRef() {
        final UidBean parentUid = new UidBean();
        parentUid.setId(1L);
        parentUid.setValue("parent");

        // prepare
        when(this.uidService.parent(eq("ref"), eq(UidBean.class))).thenReturn(parentUid);

        // call
        final UidBean actual = this.referenceServiceFacade.loadParent("ref", UidBean.class);

        // verify
        verify(this.uidService).parent(eq("ref"), eq(UidBean.class));
        assertThat(actual).isNotNull();
        assertThat(actual.getValue()).isEqualTo("parent");
    }

    /**
     * Tests linkReferences.
     * 
     * @throws Exception
     *             the technical Exception
     */
    @Ignore("To be fixed")
    @Test
    public void testAddReferenceRefDoesNotExistAndUidExists() throws Exception {
        final ClientBean client = new ClientBean.Builder().name(AUTHOR).apiKey(API_KEY).build();
        final ReferenceServiceFacade apiServiceSpy = Mockito.spy(this.referenceServiceFacade);
        final UidBean parentUid = new UidBean.Builder().id(1L).value("uid").build();
        final UidBean referenceUidToCreate = new UidBean.Builder().value("ref").parent(parentUid).client(client).build();
        final UidBean createdReferenceUid = new UidBean.Builder().id(1L).value("ref").parent(parentUid).client(client).build();

        // prepare
        when(this.uidService.load(eq("uid"))).thenReturn(parentUid);
        when(this.uidService.load(eq("ref"))).thenReturn(null);
        when(this.uidService.persist(any(UidBean.class))).thenReturn(createdReferenceUid);
        when(this.clientService.load(any(String.class))).thenReturn(client);
        doReturn("").when(apiServiceSpy).postMessage(any(String.class), any(String.class), any(String.class));

        // call
        apiServiceSpy.linkReferences("ref", "uid", API_KEY);

        // verify
        verify(this.uidService).load(eq("uid"));
        verify(this.uidService).load(eq("ref"));
        verify(this.uidService).persist(eq(referenceUidToCreate));
        verify(this.clientService).load(eq(API_KEY));
        verify(apiServiceSpy, never()).create(any(String.class));
        verify(apiServiceSpy).linkReferences(eq("ref"), eq("uid"), eq(API_KEY));
        verify(apiServiceSpy).postMessage(eq("uid"), eq("Association de la référence \"ref\" à l'UID \"uid\""), eq(API_KEY));
    }

    /**
     * Tests linkReferences.
     * 
     * @throws Exception
     *             the technical Exception
     */
    @Ignore("To be fixed")
    @Test
    public void testAddReferenceRefDoesNotExistAndUidDoesNotExist() throws Exception {
        final ClientBean client = new ClientBean.Builder().name(AUTHOR).apiKey(API_KEY).build();
        final ReferenceServiceFacade apiServiceSpy = Mockito.spy(this.referenceServiceFacade);
        final UidBean parentUid = new UidBean.Builder().id(1L).value("uid").build();
        final UidBean referenceUidToCreate = new UidBean.Builder().value("ref").parent(parentUid).client(client).build();
        final UidBean createdReferenceUid = new UidBean.Builder().id(1L).value("ref").parent(parentUid).client(client).build();

        // prepare
        when(this.uidService.load(eq("uid"))).thenReturn(null, parentUid);
        when(this.uidService.load(eq("ref"))).thenReturn(null);
        when(this.uidService.persist(any(UidBean.class))).thenReturn(createdReferenceUid);
        when(this.clientService.load(any(String.class))).thenReturn(client);
        doReturn("rootUid").when(apiServiceSpy).create(eq(API_KEY));
        doReturn("").when(apiServiceSpy).linkReferences(eq("uid"), eq("rootUid"), eq(API_KEY));
        doReturn("").when(apiServiceSpy).postMessage(any(String.class), any(String.class), any(String.class));

        // call
        apiServiceSpy.linkReferences("ref", "uid", API_KEY);

        // verify
        verify(this.uidService, times(2)).load(eq("uid"));
        verify(this.uidService).load(eq("ref"));
        verify(this.uidService).persist(eq(referenceUidToCreate));
        verify(this.clientService).load(eq(API_KEY));
        verify(apiServiceSpy).create(eq(API_KEY));
        verify(apiServiceSpy).linkReferences(eq("ref"), eq("uid"), eq(API_KEY));
        verify(apiServiceSpy).linkReferences(eq("uid"), eq("rootUid"), eq(API_KEY));
        verify(apiServiceSpy).postMessage(any(String.class), any(String.class), any(String.class));
    }

    @Test
    public void testLinkReferencesNoAuthor() throws Exception {
        try {
            this.referenceServiceFacade.linkReferences("new-ref", "old-ref", null);
            fail("MissingParameterException expected");
        } catch (final MissingParameterException ex) {
            assertThat(ex.getMessage()).isEqualTo("Missing author");
        }
    }

    @Ignore("To be fixed")
    @Test
    public void testLinkReferencesInvalidApiKey() throws Exception {
        try {
            this.referenceServiceFacade.linkReferences("new-ref", "old-ref", "Toto");
            fail("ApiPermissionException expected");
        } catch (final ApiPermissionException ex) {
            assertThat(ex.getMessage()).isEqualTo("Invalid API key");
        }
    }

    @Test
    public void testLinkReferencesOldAndNewExistsWithParent() throws Exception {
        final UidBean rootRef = new UidBean.Builder().id(1L).value("root-ref").build();
        final UidBean oldRef = new UidBean.Builder().id(2L).value("old-ref").parent(rootRef).build();
        final UidBean newRef = new UidBean.Builder().id(3L).value("new-ref").parent(rootRef).build();
        final ClientBean client = new ClientBean.Builder().name(AUTHOR).apiKey(API_KEY).build();

        when(this.uidService.load(oldRef.getValue())).thenReturn(oldRef);
        when(this.uidService.load(newRef.getValue())).thenReturn(newRef);
        when(this.clientService.load(any(String.class))).thenReturn(client);

        try {
            this.referenceServiceFacade.linkReferences(newRef.getValue(), oldRef.getValue(), AUTHOR);
            fail("Expected functional exception");
        } catch (final FunctionalException ex) {
            assertThat(ex.getMessage()).isEqualTo("Reference already linked to another UID and this UID exists");
        }
    }

    @Test
    public void testLinkReferencesOldAndNewExistsWithNoParent() throws Exception {
        final UidBean rootRef = new UidBean.Builder().id(1L).value("root-ref").build();
        final UidBean oldRef = new UidBean.Builder().id(2L).value("old-ref").parent(rootRef).build();
        final UidBean newRef = new UidBean.Builder().id(3L).value("new-ref").build();
        final ClientBean client = new ClientBean.Builder().name(AUTHOR).apiKey(API_KEY).build();

        when(this.uidService.load(oldRef.getValue())).thenReturn(oldRef);
        when(this.uidService.load(newRef.getValue())).thenReturn(newRef);
        when(this.clientService.load(any(String.class))).thenReturn(client);

        final String actual = this.referenceServiceFacade.linkReferences(newRef.getValue(), oldRef.getValue(), AUTHOR);

        assertThat(actual).isEqualTo("Reference linked to this UID");
    }

    /**
     * Tests postMessage.
     * 
     * @throws Exception
     *             the technical Exception
     */
    @Ignore("To be fixed")
    @Test
    public void testPostMessageUidExists() throws Exception {
        final ClientBean client = new ClientBean.Builder().name(AUTHOR).apiKey(API_KEY).build();
        final ReferenceServiceFacade apiServiceSpy = Mockito.spy(this.referenceServiceFacade);
        final UidBean loadedUid = new UidBean.Builder().id(1L).value("uid").build();
        final MessageBean messageToCreate = new MessageBean.Builder().content("content").uid(loadedUid).client(client).build();
        final MessageBean createdMessage = new MessageBean.Builder().id(1L).content("content").uid(loadedUid).client(client).build();

        // prepare
        when(this.uidService.load(any(String.class))).thenReturn(loadedUid);
        when(this.messageService.persist(any(MessageBean.class))).thenReturn(createdMessage);
        when(this.clientService.load(any(String.class))).thenReturn(client);

        // call
        apiServiceSpy.postMessage("uid", "content", API_KEY);

        // verify
        verify(this.uidService).load(eq("uid"));
        verify(this.messageService).persist(eq(messageToCreate));
        verify(this.clientService).load(eq(API_KEY));
        verify(apiServiceSpy, never()).create(any(String.class));
        verify(apiServiceSpy, never()).linkReferences(any(String.class), any(String.class), any(String.class));
    }

    /**
     * Tests postMessage with no existing uid.
     * 
     * @throws Exception
     *             the technical Exception
     */
    @Ignore("To be fixed")
    @Test
    public void testPostMessageUidDoesNotExist() throws Exception {
        final ClientBean client = new ClientBean.Builder().name(AUTHOR).apiKey(API_KEY).build();
        final ReferenceServiceFacade apiServiceSpy = Mockito.spy(this.referenceServiceFacade);
        final UidBean loadedUid = new UidBean.Builder().id(1L).value("uid").build();
        final MessageBean messageToCreate = new MessageBean.Builder().content("content").uid(loadedUid).client(client).build();
        final MessageBean createdMessage = new MessageBean.Builder().id(1L).content("content").uid(loadedUid).client(client).build();

        // prepare
        when(this.uidService.load(any(String.class))).thenReturn(null, loadedUid);
        when(this.messageService.persist(any(MessageBean.class))).thenReturn(createdMessage);
        when(this.clientService.load(any(String.class))).thenReturn(client);
        doReturn("rootUid").when(apiServiceSpy).create(eq(API_KEY));
        doReturn("").when(apiServiceSpy).linkReferences(eq("uid"), eq("rootUid"), eq(API_KEY));

        // call
        apiServiceSpy.postMessage("uid", "content", API_KEY);

        // verify
        verify(this.uidService, times(2)).load(eq("uid"));
        verify(this.messageService).persist(eq(messageToCreate));
        verify(this.clientService).load(eq(API_KEY));
        verify(apiServiceSpy).create(eq(API_KEY));
        verify(apiServiceSpy).linkReferences(eq("uid"), eq("rootUid"), eq(API_KEY));
    }

    @Test
    public void testPostMessageNoAuthor() throws Exception {
        try {
            this.referenceServiceFacade.postMessage("ref", "message content", null);
            fail("MissingParameterException expected");
        } catch (final MissingParameterException ex) {
            assertThat(ex.getMessage()).isEqualTo("Missing author");
        }
    }

    @Ignore("To be fixed")
    @Test
    public void testPostMessageInvalidApiKey() throws Exception {
        try {
            this.referenceServiceFacade.postMessage("ref", "message content", "Toto");
            fail("ApiPermissionException expected");
        } catch (final ApiPermissionException ex) {
            assertThat(ex.getMessage()).isEqualTo("Invalid API key");
        }
    }

    @Test
    public void testPostMessageNoRef() throws Exception {
        try {
            this.referenceServiceFacade.postMessage(null, "message content", AUTHOR);
            fail("MissingParameterException expected");
        } catch (final MissingParameterException ex) {
            assertThat(ex.getMessage()).isEqualTo("Missing reference");
        }
    }

    @Test
    public void testPostMessageNoMessage() throws Exception {
        try {
            this.referenceServiceFacade.postMessage("ref", null, AUTHOR);
            fail("MissingParameterException expected");
        } catch (final MissingParameterException ex) {
            assertThat(ex.getMessage()).isEqualTo("Missing content");
        }
    }

    @Test
    public void testSearchMessageNull() throws Exception {
        final String messageSearchValue = null;
        final List<String> sorts = Arrays.asList("2:desc");
        final Map<String, String> criteria = new HashMap<>();
        criteria.put("value", "48");
        criteria.put("parent", "empty-mode:true");
        criteria.put("created", "date-range-mode:10/06/2016:06/07/2016");

        final SearchResult<UidBean> searchResult = new SearchResult<>();
        final UidBean uid1 = new UidBean();
        uid1.setId(1L);
        uid1.setValue("val1");
        final UidBean uid2 = new UidBean();
        uid2.setId(2L);
        uid2.setValue("val2");
        searchResult.setContent(Arrays.asList(uid1, uid2));

        // call
        when(this.uidService.search(0, 10, sorts, criteria, UidBean.class)).thenReturn(searchResult);

        // call
        final SearchResult<UidBean> actual = this.referenceServiceFacade.search(0, 10, sorts, criteria, messageSearchValue, UidBean.class);

        // verify
        verify(this.uidService).search(0, 10, sorts, criteria, UidBean.class);
        assertThat(actual).isEqualTo(searchResult);
    }

    @Test
    public void testSearchMessageNotNull() throws Exception {
        final String messageSearchValue = "xxx";
        final List<String> sorts = Arrays.asList("2:desc");
        final Map<String, String> criteria = new HashMap<>();
        criteria.put("value", "48");
        criteria.put("parent", "empty-mode:true");
        criteria.put("created", "date-range-mode:10/06/2016:06/07/2016");

        final SearchResult<UidBean> searchResult = new SearchResult<>();
        final UidBean uid1 = new UidBean();
        uid1.setId(1L);
        uid1.setValue("val1");
        final UidBean uid2 = new UidBean();
        uid2.setId(2L);
        uid2.setValue("val2");
        searchResult.setContent(Arrays.asList(uid1, uid2));

        // call
        when(this.uidSearchService.search(0, 10, sorts, criteria, messageSearchValue, UidBean.class)).thenReturn(searchResult);

        // call
        final SearchResult<UidBean> actual = this.referenceServiceFacade.search(0, 10, sorts, criteria, messageSearchValue, UidBean.class);

        // verify
        verify(this.uidSearchService).search(0, 10, sorts, criteria, messageSearchValue, UidBean.class);
        assertThat(actual).isEqualTo(searchResult);
    }

}
