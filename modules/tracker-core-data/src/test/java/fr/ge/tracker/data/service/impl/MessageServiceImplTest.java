/**
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2014-2016)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.ge.tracker.data.service.impl;

import static org.fest.assertions.Assertions.assertThat;

import java.util.Arrays;
import java.util.List;

import org.dbunit.dataset.ITable;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import fr.ge.tracker.data.bean.dto.MessageBean;
import fr.ge.tracker.data.bean.dto.UidBean;
import fr.ge.tracker.data.bean.transverse.SearchResult;
import fr.ge.tracker.data.service.IMessageService;
import fr.ge.tracker.test.util.AbstractDbTest;

/**
 * Tests {@link MessageServiceImpl}.
 *
 * @author $Author: jpauchet $
 * @version $Revision: 0 $
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:spring/tracker-core-data-root-test.xml" })
public class MessageServiceImplTest extends AbstractDbTest {

    @Autowired
    private IMessageService service;

    @Before
    public void setUp() throws Exception {
    }

    /**
     * Tests persist.
     * 
     * @throws Exception
     *             the technical Exception
     */
    @Test
    public void testPersist() throws Exception {
        this.initDb(this.getClass().getSimpleName() + "-dataset.xml");
        final MessageBean bean = this.service.persist(new MessageBean.Builder().content("new message").build());
        assertThat(bean).isNotNull();
        assertThat(bean.getId()).isNotNull();
        assertThat(bean.getCreated()).isNotNull();
        assertThat(bean.getUpdated()).isNotNull();
        final ITable tbl = this.extract("message");
        assertThat(tbl.getRowCount()).isEqualTo(4);
    }

    /**
     * Tests persist.
     * 
     * @throws Exception
     *             the technical Exception
     */
    @Test
    public void testUpdate() throws Exception {
        this.initDb(this.getClass().getSimpleName() + "-dataset.xml");
        MessageBean bean = this.service.load(31111111);
        final Long oldId = bean.getId();
        bean.setContent("message updated");
        bean = this.service.persist(bean);
        assertThat(bean.getId()).isEqualTo(oldId);
    }

    /**
     * Tests search.
     * 
     * @throws Exception
     *             the technical Exception
     */
    @Test
    public void testSearchEmpty() throws Exception {
        this.initDb(this.getClass().getSimpleName() + "-dataset.xml");
        final SearchResult<MessageBean> searchResult = this.service.search(1, 10);
        assertThat(searchResult).isNotNull();
        assertThat(searchResult.getStartIndex()).isEqualTo(1);
        assertThat(searchResult.getMaxResults()).isEqualTo(10);
        assertThat(searchResult.getTotalResults()).isEqualTo(3);
        assertThat(searchResult.getContent()).isNotNull();
        assertThat(searchResult.getContent()).hasSize(0);
    }

    /**
     * Tests search.
     * 
     * @throws Exception
     *             the technical Exception
     */
    @Test
    public void testSearch() throws Exception {
        this.initDb(this.getClass().getSimpleName() + "-dataset.xml");
        SearchResult<MessageBean> searchResult = this.service.search(0, 2);
        assertThat(searchResult).isNotNull();
        assertThat(searchResult.getStartIndex()).isEqualTo(0);
        assertThat(searchResult.getMaxResults()).isEqualTo(2);
        assertThat(searchResult.getTotalResults()).isEqualTo(3);
        assertThat(searchResult.getContent()).isNotNull();
        assertThat(searchResult.getContent()).hasSize(2);

        searchResult = this.service.search(1, 2);
        assertThat(searchResult).isNotNull();
        assertThat(searchResult.getStartIndex()).isEqualTo(1);
        assertThat(searchResult.getMaxResults()).isEqualTo(2);
        assertThat(searchResult.getTotalResults()).isEqualTo(3);
        assertThat(searchResult.getContent()).isNotNull();
        assertThat(searchResult.getContent()).hasSize(1);
    }

    /**
     * Tests search and sort.
     * 
     * @throws Exception
     *             the technical Exception
     */
    @Test
    public void testSearchSort() throws Exception {
        this.initDb(this.getClass().getSimpleName() + "-dataset.xml");
        SearchResult<MessageBean> searchResult = this.service.search(0, 10, Arrays.asList("content:desc"));
        assertThat(searchResult.getContent()).onProperty("content").containsExactly("Hello world !", "Hello France !", "Hello Bercy !");

        searchResult = this.service.search(0, 10, Arrays.asList("content:asc"));
        assertThat(searchResult.getContent()).onProperty("content").containsExactly("Hello Bercy !", "Hello France !", "Hello world !");

        searchResult = this.service.search(0, 10, Arrays.asList("content"));
        assertThat(searchResult.getContent()).onProperty("content").containsExactly("Hello Bercy !", "Hello France !", "Hello world !");
    }

    /**
     * Tests {@link MessageServiceImpl#search(long, long)}.
     */
    // @Test
    // @Ignore
    // public void testSearchByReference() throws Exception {
    // final String ref = "2016-04-MKL-DWV-49";
    // this.initDb(this.getClass().getSimpleName() + "-dataset.xml");
    // SearchResult<MessageBean> searchResult = this.service.search(0, 2, ref,
    // null, MessageBean.class);
    // assertThat(searchResult).isNotNull();
    // assertThat(searchResult.getStartIndex()).isEqualTo(0);
    // assertThat(searchResult.getMaxResults()).isEqualTo(2);
    // assertThat(searchResult.getTotalResults()).isEqualTo(3);
    // assertThat(searchResult.getContent()).isNotNull();
    // assertThat(searchResult.getContent()).hasSize(2);
    //
    // searchResult = this.service.search(0, 2, ref, null, MessageBean.class);
    // assertThat(searchResult).isNotNull();
    // assertThat(searchResult.getStartIndex()).isEqualTo(1);
    // assertThat(searchResult.getMaxResults()).isEqualTo(2);
    // assertThat(searchResult.getTotalResults()).isEqualTo(3);
    // assertThat(searchResult.getContent()).isNotNull();
    // assertThat(searchResult.getContent()).hasSize(1);
    // }

    /**
     * Tests load.
     * 
     * @throws Exception
     *             the technical Exception
     */
    @Test
    public void testLoad() throws Exception {
        this.initDb(this.getClass().getSimpleName() + "-dataset.xml");
        final MessageBean bean = this.service.load(31111111);
        assertThat(bean).isNotNull();
        assertThat(bean.getContent()).isEqualTo("Hello world !");
        assertThat(bean.getUid()).isNotNull();
        assertThat(bean.getClient()).isNotNull();
    }

    /**
     * Tests load unknown message.
     * 
     * @throws Exception
     *             the technical Exception
     */
    @Test
    public void testLoadUnknown() throws Exception {
        this.initDb(this.getClass().getSimpleName() + "-dataset.xml");
        final MessageBean bean = this.service.load(42);
        assertThat(bean).isNull();
    }

    /**
     * Tests remove message.
     * 
     * @throws Exception
     *             the technical Exception
     */
    @Test
    public void testRemove() throws Exception {
        this.initDb(this.getClass().getSimpleName() + "-dataset.xml");
        this.service.remove(31111112);
        final ITable tbl = this.extract("message");
        assertThat(tbl.getRowCount()).isEqualTo(2);
    }

    /**
     * Tests remove message.
     * 
     * @throws Exception
     *             the technical Exception
     */
    @Test
    public void testRemoveUnknown() throws Exception {
        this.initDb(this.getClass().getSimpleName() + "-dataset.xml");
        this.service.remove(301);
        final ITable tbl = this.extract("message");
        assertThat(tbl.getRowCount()).isEqualTo(3);
    }

    /**
     * Tests find a message.
     * 
     * @throws Exception
     *             the technical Exception
     */
    @Test
    @Ignore("PostgreSQL syntax : with recursive")
    public void testfindByUid() throws Exception {
        this.initDb(this.getClass().getSimpleName() + "-dataset.xml");
        final List<UidBean> actual = this.service.findByUid(21111115L, UidBean.class);
        assertThat(actual).hasSize(1);

        final UidBean uid = new UidBean();
        uid.setId(21111115L);
        uid.setValue("2016-04-AAA-AAA-49");
        final List<UidBean> expected = Arrays.asList(uid);

        assertThat(actual).isEqualTo(expected);

    }

}
