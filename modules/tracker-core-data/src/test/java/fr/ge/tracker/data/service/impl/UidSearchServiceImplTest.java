/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package fr.ge.tracker.data.service.impl;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.verify;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import fr.ge.tracker.data.bean.dto.UidBean;
import fr.ge.tracker.data.bean.transverse.SearchResult;

/**
 * Tests {@link UidSearchServiceImpl}.
 *
 * @author amboussa
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:spring/search-service-context.xml" })
public class UidSearchServiceImplTest {

    @Autowired
    private UidSearchServiceImpl uidSearchService;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }

    /**
     * Tests search.
     * 
     * @throws Exception
     *             the technical Exception
     */
    @Test
    public void testSearch() throws Exception {
        final List<String> sorts = Arrays.asList("2:desc");

        final Map<String, String> criteria = new HashMap<>();
        criteria.put("value", "48");
        criteria.put("parent", "empty-mode:true");
        criteria.put("created", "date-range-mode:10/06/2016:06/07/2016");

        final SearchResult<UidBean> searchWithSpecResult = new SearchResult<>();
        final UidBean uid1 = new UidBean();
        uid1.setId(1L);
        uid1.setValue("val1");
        final UidBean uid2 = new UidBean();
        uid2.setId(2L);
        uid2.setValue("val2");
        searchWithSpecResult.setContent(Arrays.asList(uid1, uid2));

        // prepare
        final UidSearchServiceImpl uidSearchServiceSpy = Mockito.spy(this.uidSearchService);
        doReturn(searchWithSpecResult).when(uidSearchServiceSpy).searchWithSpec(any(Long.class), any(Long.class), any(List.class), any(Specification.class));

        // call
        final SearchResult<UidBean> searchResult = uidSearchServiceSpy.search(0, 10, sorts, criteria, null, UidBean.class);

        // verify
        verify(uidSearchServiceSpy).searchWithSpec(eq(0L), eq(10L), eq(sorts), any());
        assertThat(searchResult.getContent()).isEqualTo(searchWithSpecResult.getContent());
    }

}
