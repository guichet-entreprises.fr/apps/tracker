/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.ge.tracker.data.service.impl;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.verify;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.dbunit.dataset.ITable;
import org.dozer.DozerBeanMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import fr.ge.tracker.data.bean.dto.UidBean;
import fr.ge.tracker.data.bean.transverse.SearchResult;
import fr.ge.tracker.data.service.IUidService;
import fr.ge.tracker.test.util.AbstractDbTest;

/**
 * Tests {@link UidServiceImpl}.
 *
 * @author $Author: jpauchet $
 * @version $Revision: 0 $
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:spring/tracker-core-data-root-test.xml" })
public class UidServiceImplTest extends AbstractDbTest {

    @Autowired
    private IUidService service;

    @Autowired
    @Qualifier("dozerBeanMapperForCoreDataInternalServices")
    protected DozerBeanMapper dozer;

    @Before
    public void setUp() throws Exception {
    }

    /**
     * Tests {@link UidServiceImpl#persist(UidBean)}.
     * 
     * @throws Exception
     */
    @Test
    public void testPersist() throws Exception {
        this.initDb(this.getClass().getSimpleName() + "-dataset.xml");
        final UidBean bean = this.service.persist(new UidBean.Builder().value("new value").build());
        assertThat(bean).isNotNull();
        assertThat(bean.getId()).isNotNull();
        assertThat(bean.getCreated()).isNotNull();
        assertThat(bean.getUpdated()).isNotNull();
        final ITable tbl = this.extract("uid");
        assertThat(tbl.getRowCount()).isEqualTo(8);
    }

    /**
     * Tests {@link UidServiceImpl#persist(UidBean)}.
     * 
     * @throws Exception
     */
    @Test
    public void testUpdate() throws Exception {
        this.initDb(this.getClass().getSimpleName() + "-dataset.xml");
        UidBean bean = this.service.load(21111111);
        final Long oldId = bean.getId();
        bean.setValue("value updated");
        bean = this.service.persist(bean);
        assertThat(bean.getId()).isEqualTo(oldId);
    }

    /**
     * Tests {@link UidServiceImpl#search(long, long)}.
     * 
     * @throws Exception
     */
    @Test
    public void testSearchEmpty() throws Exception {
        this.initDb(this.getClass().getSimpleName() + "-dataset.xml");
        final SearchResult<UidBean> searchResult = this.service.search(1, 10);
        assertThat(searchResult).isNotNull();
        assertThat(searchResult.getStartIndex()).isEqualTo(1);
        assertThat(searchResult.getMaxResults()).isEqualTo(10);
        assertThat(searchResult.getTotalResults()).isEqualTo(7);
        assertThat(searchResult.getContent()).isNotNull();
        assertThat(searchResult.getContent()).hasSize(0);
    }

    /**
     * Tests {@link UidServiceImpl#search(long, long)}.
     * 
     * @throws Exception
     */
    @Test
    public void testSearch() throws Exception {
        this.initDb(this.getClass().getSimpleName() + "-dataset.xml");
        SearchResult<UidBean> searchResult = this.service.search(0, 2);
        assertThat(searchResult).isNotNull();
        assertThat(searchResult.getStartIndex()).isEqualTo(0);
        assertThat(searchResult.getMaxResults()).isEqualTo(2);
        assertThat(searchResult.getTotalResults()).isEqualTo(7);
        assertThat(searchResult.getContent()).isNotNull();
        assertThat(searchResult.getContent()).hasSize(2);

        searchResult = this.service.search(1, 2);
        assertThat(searchResult).isNotNull();
        assertThat(searchResult.getStartIndex()).isEqualTo(1);
        assertThat(searchResult.getMaxResults()).isEqualTo(2);
        assertThat(searchResult.getTotalResults()).isEqualTo(7);
        assertThat(searchResult.getContent()).isNotNull();
        assertThat(searchResult.getContent()).hasSize(2);
    }

    /**
     * Tests {@link UidServiceImpl#search(long, long, java.util.List)}.
     * 
     * @throws Exception
     */
    @Test
    public void testSearchSort() throws Exception {
        this.initDb(this.getClass().getSimpleName() + "-dataset.xml");
        SearchResult<UidBean> searchResult = this.service.search(0, 10, Arrays.asList("value:desc"));
        assertThat(searchResult.getContent()).onProperty("value").contains("toto", "tata", "C78010045682", "2016-04-MKL-DWV-49", "2016-04-BBB-BBB-49", "2016-04-AAA-AAA-49", "2020-01-REC-ORD-01");

        searchResult = this.service.search(0, 10, Arrays.asList("value:asc"));
        assertThat(searchResult.getContent()).onProperty("value").contains("2020-01-REC-ORD-01", "2016-04-AAA-AAA-49", "2016-04-BBB-BBB-49", "2016-04-MKL-DWV-49", "C78010045682", "tata", "toto");

        searchResult = this.service.search(0, 10, Arrays.asList("value"));
        assertThat(searchResult.getContent()).onProperty("value").contains("2020-01-REC-ORD-01", "2016-04-AAA-AAA-49", "2016-04-BBB-BBB-49", "2016-04-MKL-DWV-49", "C78010045682", "tata", "toto");
    }

    /**
     * Tests search.
     * 
     * @throws Exception
     */
    @Test
    public void testSearch2() throws Exception {
        final List<String> sorts = Arrays.asList("2:desc");

        final Map<String, String> criteria = new HashMap<>();
        criteria.put("value", "48");
        criteria.put("parent", "empty-mode:true");
        criteria.put("created", "date-range-mode:10/06/2016:06/07/2016");

        final SearchResult<UidBean> searchWithSpecResult = new SearchResult<>();
        final UidBean uid1 = new UidBean();
        uid1.setId(1L);
        uid1.setValue("val1");
        final UidBean uid2 = new UidBean();
        uid2.setId(2L);
        uid2.setValue("val2");
        searchWithSpecResult.setContent(Arrays.asList(uid1, uid2));

        // prepare
        final UidServiceImpl uidServiceBeforeSpy = new UidServiceImpl();
        uidServiceBeforeSpy.dozer = this.dozer;
        final UidServiceImpl uidServiceSpy = Mockito.spy(uidServiceBeforeSpy);
        doReturn(searchWithSpecResult).when(uidServiceSpy).searchWithSpec(any(Long.class), any(Long.class), any(List.class), any(Specification.class));

        // call
        final SearchResult<UidBean> searchResult = uidServiceSpy.search(0, 10, sorts, criteria, UidBean.class);

        // verify
        verify(uidServiceSpy).searchWithSpec(eq(0L), eq(10L), eq(sorts), any());
        assertThat(searchResult.getContent()).isEqualTo(searchWithSpecResult.getContent());
    }

    /**
     * Tests {@link UidServiceImpl#load(long)}.
     * 
     * @throws Exception
     */
    @Test
    public void testLoad() throws Exception {
        this.initDb(this.getClass().getSimpleName() + "-dataset.xml");
        final UidBean bean = this.service.load(21111111);
        assertThat(bean).isNotNull();
        assertThat(bean.getValue()).isEqualTo("2016-04-MKL-DWV-49");
    }

    /**
     * Tests {@link UidServiceImpl#load(long)}.
     * 
     * @throws Exception
     */
    @Test
    public void testLoadUnknown() throws Exception {
        this.initDb(this.getClass().getSimpleName() + "-dataset.xml");
        final UidBean bean = this.service.load(42);
        assertThat(bean).isNull();
    }

    /**
     * Tests {@link UidServiceImpl#load(String)}.
     * 
     * @throws Exception
     */
    @Test
    public void testLoadFromValue() throws Exception {
        this.initDb(this.getClass().getSimpleName() + "-dataset.xml");
        final UidBean bean = this.service.load("2016-04-MKL-DWV-49");
        assertThat(bean).isNotNull();
        assertThat(bean.getId()).isEqualTo(21111111L);
    }

    /**
     * Tests {@link UidServiceImpl#load(String)}.
     * 
     * @throws Exception
     */
    @Test
    public void testLoadUnknownFromValue() throws Exception {
        this.initDb(this.getClass().getSimpleName() + "-dataset.xml");
        final UidBean bean = this.service.load("2016-02-XXX-ZZZ-02");
        assertThat(bean).isNull();
    }

    /**
     * Tests {@link UidServiceImpl#remove(long)}.
     * 
     * @throws Exception
     */
    @Test
    public void testRemove() throws Exception {
        this.initDb(this.getClass().getSimpleName() + "-dataset.xml");
        this.service.remove(21111116);
        final ITable tbl = this.extract("uid");
        assertThat(tbl.getRowCount()).isEqualTo(6);
    }

    /**
     * Tests {@link UidServiceImpl#remove(long)}.
     * 
     * @throws Exception
     */
    @Test
    public void testRemoveUnknown() throws Exception {
        this.initDb(this.getClass().getSimpleName() + "-dataset.xml");
        this.service.remove(301);
        final ITable tbl = this.extract("uid");
        assertThat(tbl.getRowCount()).isEqualTo(7);
    }

    /**
     * Tests
     * {@link UidServiceImpl#search(long, long, java.util.List, java.util.Map)}.
     * 
     * @throws Exception
     */
    @Test
    public void testSearchSimple() throws Exception {
        this.initDb(this.getClass().getSimpleName() + "-dataset.xml");

        final Map<String, String> criteriaMap = new HashMap<>();
        criteriaMap.put("value", "-49");

        final SearchResult<UidBean> searchResult = this.service.search(0, 10, criteriaMap);
        assertThat(searchResult).isNotNull();
        assertThat(searchResult.getStartIndex()).isEqualTo(0);
        assertThat(searchResult.getMaxResults()).isEqualTo(10);
        assertThat(searchResult.getTotalResults()).isEqualTo(3);
        assertThat(searchResult.getContent()).isNotNull();
        assertThat(searchResult.getContent()).hasSize(3);
        assertThat(searchResult.getContent()).onProperty("value").containsOnly("2016-04-MKL-DWV-49", "2016-04-AAA-AAA-49", "2016-04-BBB-BBB-49");
    }

    /**
     * Tests
     * {@link UidServiceImpl#search(long, long, java.util.List, java.util.Map)}.
     * 
     * @throws Exception
     */
    @Test
    public void testSearchEmptyParent() throws Exception {
        this.initDb(this.getClass().getSimpleName() + "-dataset.xml");

        final Map<String, String> criteriaMap = new HashMap<>();
        criteriaMap.put("parent", "empty-mode:true");

        final SearchResult<UidBean> searchResult = this.service.search(0, 10, criteriaMap);
        assertThat(searchResult).isNotNull();
        assertThat(searchResult.getStartIndex()).isEqualTo(0);
        assertThat(searchResult.getMaxResults()).isEqualTo(10);
        assertThat(searchResult.getTotalResults()).isEqualTo(3);
        assertThat(searchResult.getContent()).isNotNull();
        assertThat(searchResult.getContent()).hasSize(3);
        assertThat(searchResult.getContent()).onProperty("value").contains("2020-01-REC-ORD-01", "2016-04-MKL-DWV-49", "2016-04-BBB-BBB-49");
    }

    /**
     * Tests
     * {@link UidServiceImpl#search(long, long, java.util.List, java.util.Map)}.
     * 
     * @throws Exception
     */
    @Test
    public void testSearchDateRange() throws Exception {
        this.initDb(this.getClass().getSimpleName() + "-dataset.xml");

        final Map<String, String> criteriaMap = new HashMap<>();
        criteriaMap.put("created", "date-range-mode:02/04/2016:19/04/2016");

        final SearchResult<UidBean> searchResult = this.service.search(0, 10, criteriaMap);
        assertThat(searchResult).isNotNull();
        assertThat(searchResult.getStartIndex()).isEqualTo(0);
        assertThat(searchResult.getMaxResults()).isEqualTo(10);
        assertThat(searchResult.getTotalResults()).isEqualTo(5);
        assertThat(searchResult.getContent()).isNotNull();
        assertThat(searchResult.getContent()).hasSize(5);
        assertThat(searchResult.getContent()).onProperty("value").containsOnly("2016-04-MKL-DWV-49", "C78010045682", "toto", "tata", "2016-04-AAA-AAA-49");
    }

    /**
     * Tests
     * {@link UidServiceImpl#search(long, long, java.util.List, java.util.Map)}.
     * 
     * @throws Exception
     */
    @Test
    public void testSearchSimpleEmptyParentDateRange() throws Exception {
        this.initDb(this.getClass().getSimpleName() + "-dataset.xml");

        final Map<String, String> criteriaMap = new HashMap<>();
        criteriaMap.put("value", "-49");
        criteriaMap.put("parent", "empty-mode:true");
        criteriaMap.put("created", "date-range-mode:02/04/2016:19/04/2016");

        final SearchResult<UidBean> searchResult = this.service.search(0, 10, criteriaMap);
        assertThat(searchResult).isNotNull();
        assertThat(searchResult.getStartIndex()).isEqualTo(0);
        assertThat(searchResult.getMaxResults()).isEqualTo(10);
        assertThat(searchResult.getTotalResults()).isEqualTo(1);
        assertThat(searchResult.getContent()).isNotNull();
        assertThat(searchResult.getContent()).hasSize(1);
        assertThat(searchResult.getContent()).onProperty("value").containsOnly("2016-04-MKL-DWV-49");
    }

    /**
     * Tests remove multiples uids.
     * 
     * @throws Exception
     */
    @Test
    public void testRemoveUids() throws Exception {
        this.initDb(this.getClass().getSimpleName() + "-dataset.xml");

        final String uid = "2020-01-REC-ORD-01";
        assertThat(this.service.load(uid)).isNotNull();

        this.service.remove(Arrays.asList(uid));
        assertThat(this.service.load(uid)).isNull();
    }
}
