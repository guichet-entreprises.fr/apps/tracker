/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.ge.tracker.data.service.impl;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import fr.ge.common.utils.bean.search.SearchQueryFilter;
import fr.ge.common.utils.bean.search.SearchQueryOrder;
import fr.ge.common.utils.bean.search.SearchResult;
import fr.ge.tracker.data.bean.dto.MessageBean;
import fr.ge.tracker.data.bean.dto.UidBean;
import fr.ge.tracker.data.bean.model.MessageEntity;
import fr.ge.tracker.data.bean.model.UidEntity;
import fr.ge.tracker.data.persistence.MessageDao;
import fr.ge.tracker.data.persistence.repository.MessageRepository;
import fr.ge.tracker.data.persistence.repository.UidRepository;
import fr.ge.tracker.data.service.IMessageService;

/**
 * The message service.
 *
 * @author $Author: jpauchet $
 * @version $Revision: 0 $
 */
@Component
public class MessageServiceImpl extends AbstractInternalService<MessageBean, MessageEntity, MessageRepository> implements IMessageService {

    @Autowired
    protected UidRepository uidRepository;

    @Autowired
    protected MessageDao messageDao;

    /**
     * {@inheritDoc}
     */
    @Override
    @Transactional(readOnly = true)
    public List<MessageBean> getMessagesByUidId(final Long id) {
        return this.findByUid(id, MessageBean.class);
    }

    @Override
    @Transactional(readOnly = true)
    public <R> List<R> findByUid(final Long id, final Class<? extends R> expected) {
        final List<R> beans = new ArrayList<R>();
        final List<MessageEntity> entities = this.repository.getMessagesByUidId(id);
        if (null != entities) {
            for (final MessageEntity entity : entities) {
                beans.add(this.dozer.map(entity, expected));
            }
        }
        return beans;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Class<MessageBean> getTypeDto() {
        return MessageBean.class;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Class<MessageEntity> getTypeEntity() {
        return MessageEntity.class;
    }

    @Override
    @Transactional
    public MessageBean persist(final MessageBean bean) {
        final MessageBean persistedBean = super.persist(bean);

        final Calendar updated = persistedBean.getUpdated();
        UidEntity entity = Optional.ofNullable(persistedBean).map(MessageBean::getUid).map(UidBean::getId).map(id -> this.uidRepository.findOne(id)).orElse(null);
        for (; null != entity; entity = entity.getParent()) {
            entity.setUpdated(updated);
            this.uidRepository.save(entity);
        }

        return persistedBean;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public <R> SearchResult<R> search(int startIndex, int maxResults, List<SearchQueryFilter> filters, List<SearchQueryOrder> orders, Class<? extends R> expected) {
        fr.ge.common.utils.bean.search.SearchResult<MessageBean> result = this.messageDao.getMessages(startIndex, //
                maxResults, //
                filters, //
                orders);

        final fr.ge.common.utils.bean.search.SearchResult<R> expectedSearchResult = new fr.ge.common.utils.bean.search.SearchResult<R>(startIndex, maxResults);
        expectedSearchResult.setTotalResults(result.getTotalResults());
        expectedSearchResult.setContent(result.getContent().stream().map(elm -> this.dozer.map(elm, expected)).collect(Collectors.toList()));

        return expectedSearchResult;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @Transactional(readOnly = false)
    public long removeMessages(final String key, final String terms) {
        return this.messageDao.removeMessages(key, terms);
    }
}
