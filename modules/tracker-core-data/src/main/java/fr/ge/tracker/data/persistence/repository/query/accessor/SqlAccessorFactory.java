/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.ge.tracker.data.persistence.repository.query.accessor;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * A factory for creating SqlAccessor objects.
 *
 * @author Christian Cougourdan
 */
public final class SqlAccessorFactory {

    /** The Constant LOGGER. */
    private static final Logger LOGGER = LoggerFactory.getLogger(SqlAccessorFactory.class);

    /** The Constant ACCESSORS. */
    private static final Map<Class<?>, ISqlAccessor<?>> ACCESSORS_BY_CLASS;

    private static final Map<Integer, Map<Class<?>, ISqlAccessor<?>>> ACCESSORS_BY_TYPE;

    static {
        final Map<Class<?>, ISqlAccessor<?>> byClass = new HashMap<>();
        final Map<Integer, Map<Class<?>, ISqlAccessor<?>>> byType = new HashMap<>();

        Arrays.asList( //
                CalendarSqlAccessor.class, //
                InstantSqlAccessor.class, //
                LongSqlAccessor.class, //
                StringSqlAccessor.class, //
                TimestampSqlAccessor.class //
        ) //
                .forEach(cls -> {
                    try {
                        final ISqlAccessor<?> accessor = cls.newInstance();
                        final Class<?> accepted = accessor.accept();
                        byClass.put(accepted, accessor);
                        accessor.types().forEach(type -> {
                            Map<Class<?>, ISqlAccessor<?>> m = byType.get(type);
                            if (null == m) {
                                byType.put(type, m = new HashMap<>());
                            }
                            m.put(accepted, accessor);
                        });
                    } catch (InstantiationException | IllegalAccessException ex) {
                        LOGGER.warn("Unable to load accessor", ex);
                    }
                });

        ACCESSORS_BY_CLASS = Collections.unmodifiableMap(byClass);
        ACCESSORS_BY_TYPE = Collections.unmodifiableMap(byType);
    }

    /**
     * Instantiates a new sql accessor factory.
     */
    private SqlAccessorFactory() {
        // Nothing to do
    }

    /**
     * Gets the.
     *
     * @param <R>
     *            the generic type
     * @param expected
     *            the expected
     * @return the i sql accessor
     */
    @SuppressWarnings("unchecked")
    public static <R> ISqlAccessor<R> get(final Class<R> expected) {
        return (ISqlAccessor<R>) ACCESSORS_BY_CLASS.get(expected);
    }

    @SuppressWarnings("unchecked")
    public static <R> ISqlAccessor<R> get(final int type, final Class<R> expected) {
        return (ISqlAccessor<R>) Optional.ofNullable(ACCESSORS_BY_TYPE.get(type)).map(m -> m.get(expected)).orElse(null);
    }

}
