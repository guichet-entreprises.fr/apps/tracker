/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.ge.tracker.data.service.impl;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicInteger;

import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import fr.ge.tracker.data.bean.dto.UidBean;
import fr.ge.tracker.data.bean.model.UidEntity;
import fr.ge.tracker.data.bean.transverse.SearchResult;
import fr.ge.tracker.data.persistence.repository.UidRepository;
import fr.ge.tracker.data.service.IUidService;

/**
 * The UID service.
 *
 * @author $Author: jpauchet $
 * @version $Revision: 0 $
 */
@Component
public class UidServiceImpl extends AbstractInternalService<UidBean, UidEntity, UidRepository> implements IUidService {

    /**
     * {@inheritDoc}
     */
    @Override
    @Transactional(readOnly = true)
    public UidBean load(final String value) {
        return load(value, UidBean.class);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @Transactional(readOnly = true)
    public <T> SearchResult<T> search(final long pageIndex, final long maxResults, final List<String> sorts, final Map<String, String> criteria, final Class<? extends T> expected) {
        final SearchResult<UidBean> dtoSearchResult = search(pageIndex, maxResults, sorts, criteria);
        final SearchResult<T> expectedSearchResult = new SearchResult.Builder<T>().startIndex(dtoSearchResult.getStartIndex()).maxResults(dtoSearchResult.getMaxResults())
                .totalResults(dtoSearchResult.getTotalResults()).build();
        final List<T> expectedContent = new ArrayList<T>();
        for (final UidBean uidBean : dtoSearchResult.getContent()) {
            expectedContent.add(dozer.map(uidBean, expected));
        }
        expectedSearchResult.setContent(expectedContent);
        return expectedSearchResult;
    }

    @Override
    @Transactional(readOnly = true)
    public <T> T load(final String value, final Class<? extends T> expected) {
        final UidEntity entity = repository.findByValue(value);
        if (null == entity) {
            return null;
        } else {
            return dozer.map(entity, expected);
        }
    }

    @Override
    @Transactional(readOnly = true)
    public <T> T parent(final String value, final Class<? extends T> expected) {
        final UidEntity entity = repository.findByValue(value);

        if (null == entity || null == entity.getParent()) {
            return null;
        } else {
            return dozer.map(entity, expected);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Class<UidBean> getTypeDto() {
        return UidBean.class;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Class<UidEntity> getTypeEntity() {
        return UidEntity.class;
    }

    @Override
    @Transactional
    public UidBean persist(final UidBean bean) {
        final UidBean persistedBean = super.persist(bean);

        final Calendar updated = persistedBean.getUpdated();
        UidEntity entity = Optional.ofNullable(persistedBean).map(UidBean::getParent).map(UidBean::getId).map(id -> repository.findOne(id)).orElse(null);
        for (; null != entity; entity = entity.getParent()) {
            entity.setUpdated(updated);
            repository.save(entity);
        }

        return persistedBean;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @Transactional
    public int remove(final List<String> uids) {
        final AtomicInteger deleted = new AtomicInteger(0);
        uids.stream().forEach(uid -> {
            final UidEntity entity = Optional.of(uid).map(id -> this.repository.findByValue(id)).orElse(null);
            if (null != entity) {
                this.repository.delete(entity);
                final UidEntity parent = Optional.of(entity).map(UidEntity::getParent).map(id -> this.repository.findOne(id.getId())).orElse(null);
                if (null != parent) {
                    this.repository.delete(parent);
                }
                deleted.getAndIncrement();
            }
        });
        return deleted.get();
    }

}
