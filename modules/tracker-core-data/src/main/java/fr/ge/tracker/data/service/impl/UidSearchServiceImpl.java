/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.ge.tracker.data.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import fr.ge.tracker.data.bean.dto.UidBean;
import fr.ge.tracker.data.bean.model.UidSearchEntity;
import fr.ge.tracker.data.bean.transverse.SearchResult;
import fr.ge.tracker.data.persistence.repository.UidSearchRepository;
import fr.ge.tracker.data.persistence.specification.GenericSpecification;
import fr.ge.tracker.data.service.IUidSearchService;

/**
 * The UID search service.
 *
 * @author $Author: jpauchet $
 * @version $Revision: 0 $
 */
@Component
public class UidSearchServiceImpl extends AbstractInternalService<UidBean, UidSearchEntity, UidSearchRepository> implements IUidSearchService {

    /** Content. */
    private static final String CONTENT = "content";

    /** Like clause wildcard. */
    private static final String LIKE_CLAUSE_WILDCARD = "%";

    /** Message. */
    private static final String MESSAGES = "messages";

    /**
     * {@inheritDoc}
     */
    @Override
    @Transactional(readOnly = true)
    public <T> SearchResult<T> search(final long pageIndex, final long maxResults, final List<String> sorts, final Map<String, String> criteria, final String messageSearchValue,
            final Class<? extends T> expected) {
        final Specification<UidSearchEntity> messageCriteria = new Specification<UidSearchEntity>() {
            @Override
            public Predicate toPredicate(final Root<UidSearchEntity> root, final CriteriaQuery<?> query, final CriteriaBuilder cb) {
                query.distinct(true);
                Predicate genericCriteria = GenericSpecification.<UidSearchEntity>areSearchCriteriaVerified(criteria).toPredicate(root, query, cb);
                Predicate messageCriteria = cb.like(root.join(MESSAGES).get(CONTENT).as(String.class), LIKE_CLAUSE_WILDCARD + messageSearchValue + LIKE_CLAUSE_WILDCARD);
                if (genericCriteria == null) {
                    return messageCriteria;
                } else {
                    return cb.and(genericCriteria, messageCriteria);
                }
            }
        };
        SearchResult<UidBean> dtoSearchResult = searchWithSpec(pageIndex, maxResults, sorts, messageCriteria);
        SearchResult<T> expectedSearchResult = new SearchResult.Builder<T>().startIndex(dtoSearchResult.getStartIndex()).maxResults(dtoSearchResult.getMaxResults())
                .totalResults(dtoSearchResult.getTotalResults()).build();
        List<T> expectedContent = new ArrayList<T>();
        for (UidBean uidBean : dtoSearchResult.getContent()) {
            expectedContent.add(dozer.map(uidBean, expected));
        }
        expectedSearchResult.setContent(expectedContent);
        return expectedSearchResult;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Class<UidBean> getTypeDto() {
        return UidBean.class;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Class<UidSearchEntity> getTypeEntity() {
        return UidSearchEntity.class;
    }

}
