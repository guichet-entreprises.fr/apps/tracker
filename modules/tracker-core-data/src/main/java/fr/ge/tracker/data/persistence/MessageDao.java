/**
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2014-2016)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.ge.tracker.data.persistence;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.BiConsumer;
import java.util.function.Function;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import fr.ge.common.utils.bean.search.SearchQueryFilter;
import fr.ge.common.utils.bean.search.SearchQueryOrder;
import fr.ge.common.utils.bean.search.SearchResult;
import fr.ge.tracker.data.bean.dto.ClientBean;
import fr.ge.tracker.data.bean.dto.MessageBean;
import fr.ge.tracker.data.persistence.repository.query.SqlQuery;
import fr.ge.tracker.data.persistence.repository.query.accessor.SqlAccessorFactory;

/**
 * Message repository.
 * 
 * @author $Author: aolubi $
 * @version $Revision: 0 $
 */
@Repository
public class MessageDao {

    private static final String WITH_CLAUSE_MESSAGE = "with recursive parent(value) as ( " //
            + "select uid.value, uid.id from uid " //
            + "union all " //
            + "select child.value, child.id from parent, uid as child where child.uid_fk = parent.id " //
            + ") " //
            + "select COUNT(1) OVER() __totalResults, m.*, p.value as client from parent p inner join message m on m.uid_fk = p.id "; //

    /** The bean mapper. */
    private final RowMapper<MessageBean> beanMapper = (rs, rowNum) -> new MessageBean() //
            .setId(SqlAccessorFactory.get(Long.class).get(rs, "id")) //
            .setContent(SqlAccessorFactory.get(String.class).get(rs, "content")) //
            .setCreated(SqlAccessorFactory.get(Calendar.class).get(rs, "created")) //
            .setUpdated(SqlAccessorFactory.get(Calendar.class).get(rs, "updated")) //
            .setClient(new ClientBean().setName(SqlAccessorFactory.get(String.class).get(rs, "client"))) //
    ;

    /** The Constant OPERATORS. */
    private static final Map<String, String> OPERATORS;

    static {
        final Map<String, String> m = new HashMap<>();

        m.put(":", "=");
        m.put("!", "<>");
        m.put("~", "LIKE");
        m.put("in", "=");
        Arrays.asList(">", "<", ">=", "<=").forEach(key -> m.put(key, key));

        OPERATORS = Collections.unmodifiableMap(m);
    }
    /** Operation AND. */
    public static final String GROUP_FILTER_AND = "AND";

    /** The Constant DATE_FORMATTER. */
    private static final DateTimeFormatter DATE_FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd");

    /** The Constant SEARCH_FILTERS. */
    private static final Map<String, BiConsumer<SqlQuery, SearchQueryFilter>> SEARCH_FILTERS;

    static {
        final Map<String, BiConsumer<SqlQuery, SearchQueryFilter>> map = new HashMap<>();

        map.put("value", buildQueryPredicate(GROUP_FILTER_AND, "p", v -> v));
        map.put("content", buildQueryPredicate(GROUP_FILTER_AND, "m", v -> v));
        map.put("created", buildQueryPredicate(GROUP_FILTER_AND, "m", str -> DATE_FORMATTER.parse(str, LocalDate::from)));
        map.put("updated", buildQueryPredicate(GROUP_FILTER_AND, "m", str -> DATE_FORMATTER.parse(str, LocalDate::from)));
        SEARCH_FILTERS = Collections.unmodifiableMap(map);
    }

    @Autowired(required = true)
    private JdbcTemplate jdbcTemplate;

    /**
     * Builds the query predicate.
     *
     * @param operator
     *            the operator
     * @param alias
     *            the alias
     * @param accessor
     *            the accessor
     * @return the bi consumer
     */
    protected static BiConsumer<SqlQuery, SearchQueryFilter> buildQueryPredicate(final String operator, final CharSequence alias, final Function<String, Object> accessor) {
        return (query, filter) -> {
            final String op = OPERATORS.get(filter.getOperator());
            if (null != op) {
                final String column = (null == alias ? "" : alias + ".") + filter.getColumn();
                if ("=".equals(op)) {
                    final List<Object> values = filter.getValues().stream().filter(StringUtils::isNotEmpty).map(accessor).collect(Collectors.toList());
                    if (values.isEmpty()) {
                        query.addFilter(operator, column + " IS NULL");
                    } else if (values.size() == 1) {
                        query.addFilter(operator, column + " = ?", values.get(0));
                    } else {
                        query.addFilter(operator, //
                                column + " IN (" + values.stream().map(value -> "?").collect(Collectors.joining(", ")) + ")", //
                                values.toArray(new Object[] {}) //
                        );
                    }
                } else if ("<>".equals(op)) {
                    final List<Object> values = filter.getValues().stream().filter(StringUtils::isNotEmpty).map(accessor).collect(Collectors.toList());
                    if (values.isEmpty()) {
                        query.addFilter(operator, column + " IS NOT NULL");
                    } else if (values.size() == 1) {
                        query.addFilter(operator, //
                                column + " <> ?", //
                                values.get(0) //
                        );
                    } else {
                        query.addFilter(operator, //
                                column + " NOT IN (" + values.stream().map(value -> "?").collect(Collectors.joining(", ")) + ")", //
                                values.toArray(new Object[] {}) //
                        );
                    }
                } else if ("LIKE".equals(op)) {
                    if (StringUtils.isNotEmpty(filter.getValue())) {
                        query.addFilter(operator, "LOWER(" + column + ") LIKE ?", accessor.apply(filter.getValue().toLowerCase().replace('*', '%')));
                    }
                } else {
                    query.addFilter(operator, column + ' ' + op + " ?", accessor.apply(filter.getValue()));
                }
            }
        };
    }

    public SearchResult<MessageBean> getMessages(final int startIndex, final int maxResults, final List<SearchQueryFilter> filters, final List<SearchQueryOrder> orders) {
        final SqlQuery sqlQuery = new SqlQuery(WITH_CLAUSE_MESSAGE, "m.id, p.value");

        if (null != filters) {
            for (final SearchQueryFilter filter : filters) {
                Optional.ofNullable(SEARCH_FILTERS.get(filter.getColumn())).ifPresent(builder -> {
                    builder.accept(sqlQuery, filter);
                });
            }
        }

        if (null != orders) {
            for (final SearchQueryOrder order : orders) {
                sqlQuery.addOrder(order.getColumn() + " " + order.getOrder());
            }
        }

        return sqlQuery.execute(this.jdbcTemplate, startIndex, maxResults, this.beanMapper);
    }

    /**
     * Remove messages associated to a reference.
     * 
     * @param key
     *            the Tracker reference key
     * @param terms
     *            the terms
     * @return the total of lines removed
     */
    public long removeMessages(final String key, final String terms) {
        final String query = "with recursive parent(value) as ( " //
                + "select uid.value, uid.id from uid where value = ? " //
                + "union all " //
                + "select child.value, child.id from parent, uid as child where child.uid_fk = parent.id " //
                + ") " //
                + "delete from message m where m.uid_fk in (select p.id from parent p WHERE m.uid_fk = p.id) and m.content like '%' || ? ||'%' " //
        ;

        return this.jdbcTemplate.update( //
                query, //
                new Object[] { key, terms } //
        );
    }
}
