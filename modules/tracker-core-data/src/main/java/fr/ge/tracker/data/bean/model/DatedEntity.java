/**
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2014-2016)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.ge.tracker.data.bean.model;

import java.util.Calendar;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;

/**
 * An abstract entity.
 *
 * @author $Author: jpauchet $
 * @version $Revision: 0 $
 */
@MappedSuperclass
public class DatedEntity {

    /** The id. */
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_pk")
    @Column(name = "id", updatable = false, nullable = false)
    private Long id;

    /** The date of creation. */
    @Column(nullable = false)
    private Calendar created;

    /** The date of last update. */
    @Column(nullable = false)
    private Calendar updated;

    /**
     * Constructor.
     */
    protected DatedEntity() {
        // Nothing to do.
    };

    /**
     * Pre persist.
     */
    @PrePersist
    public void prePersist() {
        final Calendar now = Calendar.getInstance();
        if (null == this.created) {
            this.created = now;
        }
        if (null == this.updated) {
            this.updated = now;
        }
    }

    /**
     * Pre update.
     */
    @PreUpdate
    public void preUpdate() {
        this.updated = Calendar.getInstance();
    }

    /**
     * Getter on attribute {@link #id}.
     *
     * @return id
     */
    public Long getId() {
        return this.id;
    }

    /**
     * Setter on attribute {@link #id}.
     *
     * @param id
     *            the new value of attribute id
     */
    public void setId(final Long id) {
        this.id = id;
    }

    /**
     * Getter on attribute {@link #created}.
     *
     * @return created
     */
    public Calendar getCreated() {
        return this.created;
    }

    /**
     * Setter on attribute {@link #created}.
     *
     * @param created
     *            the new value of attribute created
     */
    public void setCreated(final Calendar created) {
        this.created = created;
    }

    /**
     * Getter on attribute {@link #updated}.
     *
     * @return updated
     */
    public Calendar getUpdated() {
        return this.updated;
    }

    /**
     * Setter on attribute {@link #updated}.
     *
     * @param updated
     *            the new value of attribute updated
     */
    public void setUpdated(final Calendar updated) {
        this.updated = updated;
    }

}
