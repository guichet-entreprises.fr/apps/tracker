/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.ge.tracker.data.bean.transverse;

import java.util.Arrays;
import java.util.List;

/**
 * A search result.
 *
 * @author $Author: jpauchet $
 * @version $Revision: 0 $
 * @param <T>
 *            the type of items contained in the search result
 */
public class SearchResult<T> {

    /** The constant DEFAULT_MAXRESULTS. */
    public static final long DEFAULT_MAXRESULTS = 10L;

    /** The start index. */
    private long startIndex = 0;

    /** The max results. */
    private long maxResults = DEFAULT_MAXRESULTS;

    /** The total results. */
    private long totalResults = 0;

    /** The content. */
    private List<T> content;

    /**
     * Constructor.
     */
    public SearchResult() {
        // Nothing to do
    }

    /**
     * Constructor.
     *
     * @param startIndex
     *            start index
     * @param maxResults
     *            max results
     */
    public SearchResult(final long startIndex, final long maxResults) {
        this.startIndex = startIndex;
        this.maxResults = maxResults;
    }

    /**
     * Getter on attribute {@link #startIndex}.
     *
     * @return start index
     */
    public long getStartIndex() {
        return startIndex;
    }

    /**
     * Setter on attribute {@link #startIndex}.
     *
     * @param startIndex
     *            the new value of attribute start index
     */
    public void setStartIndex(final long startIndex) {
        this.startIndex = startIndex;
    }

    /**
     * Getter on attribute {@link #maxResults}.
     *
     * @return max results
     */
    public long getMaxResults() {
        return maxResults;
    }

    /**
     * Setter on attribute {@link #maxResults}.
     *
     * @param maxResults
     *            the new value of attribute max results
     */
    public void setMaxResults(final long maxResults) {
        this.maxResults = maxResults;
    }

    /**
     * Getter on attribute {@link #totalResults}.
     *
     * @return total results
     */
    public long getTotalResults() {
        return totalResults;
    }

    /**
     * Setter on attribute {@link #totalResults}.
     *
     * @param totalResults
     *            the new value of attribute total results
     */
    public void setTotalResults(final long totalResults) {
        this.totalResults = totalResults;
    }

    /**
     * Getter on attribute {@link #content}.
     *
     * @return content
     */
    public List<T> getContent() {
        return content;
    }

    /**
     * Setter on attribute {@link #content}.
     *
     * @param content
     *            the new value of attribute content
     */
    public void setContent(final List<T> content) {
        this.content = content;
    }

    /**
     * Class Builder.
     *
     * @param <T>
     *            type générique
     */
    public static class Builder<T> {

        /** start index. */
        private long startIndex = 0;

        /** max results. */
        private long maxResults = DEFAULT_MAXRESULTS;

        /** total results. */
        private long totalResults = 0;

        /** content. */
        private List<T> content;

        /**
         * Start index.
         *
         * @param value
         *            value
         * @return builder
         */
        public Builder<T> startIndex(final long value) {
            this.startIndex = value;
            return this;
        }

        /**
         * Max results.
         *
         * @param value
         *            value
         * @return builder
         */
        public Builder<T> maxResults(final long value) {
            this.maxResults = value;
            return this;
        }

        /**
         * Total results.
         *
         * @param value
         *            value
         * @return builder
         */
        public Builder<T> totalResults(final long value) {
            this.totalResults = value;
            return this;
        }

        /**
         * Content.
         *
         * @param value
         *            value
         * @return builder
         */
        public Builder<T> content(final T... value) {
            this.content = Arrays.asList(value);
            return this;
        }

        /**
         * Build.
         *
         * @return search result
         */
        public SearchResult<T> build() {
            final SearchResult<T> obj = new SearchResult<T>();
            obj.setStartIndex(startIndex);
            obj.setMaxResults(maxResults);
            obj.setTotalResults(totalResults);
            obj.setContent(content);
            return obj;
        }

    }

}
