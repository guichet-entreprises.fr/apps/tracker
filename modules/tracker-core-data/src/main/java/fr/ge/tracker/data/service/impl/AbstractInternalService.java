/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.ge.tracker.data.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.domain.Sort.Order;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import fr.ge.tracker.data.bean.dto.AbstractDatedBean;
import fr.ge.tracker.data.bean.model.DatedEntity;
import fr.ge.tracker.data.bean.transverse.SearchResult;
import fr.ge.tracker.data.persistence.AbstractRepository;
import fr.ge.tracker.data.persistence.specification.GenericSpecification;
import fr.ge.tracker.data.service.IInternalService;

/**
 * An internal service.
 *
 * @author $Author: jpauchet $
 * @version $Revision: 0 $
 * @param <D>
 *            The DTO
 * @param <E>
 *            The entity
 * @param <R>
 *            The repository
 */
@Component
public abstract class AbstractInternalService<D extends AbstractDatedBean, E extends DatedEntity, R extends AbstractRepository<E>> implements IInternalService<D, E> {

    /** The repository. */
    @Autowired
    protected R repository;

    /** The Dozer bean mapper. */
    @Autowired
    @Qualifier("dozerBeanMapperForCoreDataInternalServices")
    protected DozerBeanMapper dozer;

    /**
     * Get the type of the DTO.
     *
     * @return the type
     */
    public abstract Class<D> getTypeDto();

    /**
     * Get the type of the entity.
     *
     * @return the type
     */
    public abstract Class<E> getTypeEntity();

    /**
     * {@inheritDoc}
     */
    @Override
    @Transactional(readOnly = true)
    public SearchResult<D> search(final long pageIndex, final long maxResults) {
        return this.searchWithSpec(pageIndex, maxResults, null, null);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @Transactional(readOnly = true)
    public SearchResult<D> search(final long pageIndex, final long maxResults, final List<String> sorts) {
        return this.searchWithSpec(pageIndex, maxResults, sorts, null);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @Transactional(readOnly = true)
    public SearchResult<D> search(final long pageIndex, final long maxResults, final Map<String, String> criteria) {
        return this.searchWithSpec(pageIndex, maxResults, null, GenericSpecification.<E>areSearchCriteriaVerified(criteria));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @Transactional(readOnly = true)
    public SearchResult<D> search(final long pageIndex, final long maxResults, final List<String> sorts, final Map<String, String> criteria) {
        return this.searchWithSpec(pageIndex, maxResults, sorts, GenericSpecification.<E>areSearchCriteriaVerified(criteria));
    }

    /**
     * Search results with sorts and spec.
     *
     * @param pageIndex
     *            the page index
     * @param maxResults
     *            the maximum number of results
     * @param sorts
     *            the sorts
     * @param spec
     *            the specification
     * @return the search results
     */
    @Transactional(readOnly = true)
    protected SearchResult<D> searchWithSpec(final long pageIndex, final long maxResults, final List<String> sorts, final Specification<E> spec) {
        final SearchResult<D> searchResult = new SearchResult<>(pageIndex, maxResults);

        final List<Order> orders = new ArrayList<>();
        if (null != sorts) {
            for (final String sort : sorts) {
                final String[] lst = sort.split(":", 2);
                orders.add(new Order(lst.length > 1 && "desc".equals(lst[1]) ? Direction.DESC : Direction.ASC, lst[0]));
            }
        }

        final PageRequest pageRequest = new PageRequest((int) pageIndex, (int) maxResults, orders.isEmpty() ? null : new Sort(orders));
        final Page<E> page = this.repository.findAll(spec, pageRequest);

        searchResult.setTotalResults(page.getTotalElements());
        if (null == page.getContent()) {
            searchResult.setContent(new ArrayList<D>());
        } else {
            final List<D> lst = new ArrayList<>();
            for (final E e : page.getContent()) {
                lst.add(this.dozer.map(e, this.getTypeDto()));
            }
            searchResult.setContent(lst);
        }
        return searchResult;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @Transactional
    public D persist(final D bean) {
        E entity;
        if (null == bean.getId()) {
            entity = this.dozer.map(bean, this.getTypeEntity());
        } else {
            entity = this.repository.findOne(bean.getId());
            this.dozer.map(bean, entity);
        }
        return this.dozer.map(this.repository.save(entity), this.getTypeDto());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @Transactional(readOnly = true)
    public D load(final long id) {
        return this.load(id, this.getTypeDto());
    }

    /**
     * Load.
     *
     * @param <T>
     *            the generic type
     * @param id
     *            the id
     * @param expected
     *            the expected
     * @return the t
     */
    @Transactional(readOnly = true)
    public <T> T load(final long id, final Class<? extends T> expected) {
        final E entity = this.repository.findOne(id);
        if (null == entity) {
            return null;
        } else {
            return this.dozer.map(entity, expected);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @Transactional
    public void remove(final long id) {
        final E entity = this.repository.findOne(id);
        if (null == entity) {
            return;
        }
        this.repository.delete(entity);
    }

    /**
     * Entity.
     *
     * @param id
     *            the id
     * @return the e
     */
    protected E entity(final long id) {
        return this.repository.findOne(id);
    }

}
