/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.ge.tracker.data.bean.dto;

import java.util.Calendar;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * An abstract DTO.
 *
 * @author $Author: jpauchet $
 * @version $Revision: 0 $
 */
public abstract class AbstractDatedBean<T> {

    /** The id. */
    @JsonIgnore
    private Long id;

    /** The date of creation. */
    @DateTimeFormat(iso = ISO.DATE_TIME)
    private Calendar created;

    /** The date of last update. */
    @DateTimeFormat(iso = ISO.DATE_TIME)
    @JsonIgnore
    private Calendar updated;

    /**
     * Constructor.
     */
    public AbstractDatedBean() {
        // Nothing to do
    }

    /**
     * Constructor.
     *
     * @param builder
     *            the builder
     */
    public AbstractDatedBean(final DatedBuilder<?> builder) {
        this.id = builder.id;
        this.created = builder.created;
        this.updated = builder.updated;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int hashCode() {
        return HashCodeBuilder.reflectionHashCode(this);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (!(obj instanceof AbstractDatedBean)) {
            return false;
        }
        final AbstractDatedBean other = (AbstractDatedBean) obj;
        return EqualsBuilder.reflectionEquals(this, other);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.MULTI_LINE_STYLE);
    }

    /**
     * Getter on attribute {@link #id}.
     *
     * @return id
     */
    public Long getId() {
        return this.id;
    }

    /**
     * Setter on attribute {@link #id}.
     *
     * @param id
     *            the new value of attribute id
     * @return this
     */
    @SuppressWarnings("unchecked")
    public T setId(final Long id) {
        this.id = id;
        return (T) this;
    }

    /**
     * Getter on attribute {@link #created}.
     *
     * @return created
     */
    public Calendar getCreated() {
        return this.created;
    }

    /**
     * Setter on attribute {@link #created}.
     *
     * @param created
     *            the new value of attribute created
     * @return this
     */
    @SuppressWarnings("unchecked")
    public T setCreated(final Calendar created) {
        this.created = created;
        return (T) this;
    }

    /**
     * Getter on attribute {@link #updated}.
     *
     * @return updated
     */
    public Calendar getUpdated() {
        return this.updated;
    }

    /**
     * Setter on attribute {@link #updated}.
     *
     * @param updated
     *            the new value of attribute updated
     * @return this
     */
    @SuppressWarnings("unchecked")
    public T setUpdated(final Calendar updated) {
        this.updated = updated;
        return (T) this;
    }

    /**
     * Date builder.
     * 
     * @author $Author: jpauchet $
     * @version $Revision: 0 $
     * @param <T>
     *            type of the built bean
     */
    public static class DatedBuilder<T> {

        /** The id. */
        protected Long id;

        /** The date of creation. */
        protected Calendar created;

        /** The date of last update. */
        protected Calendar updated;

        /**
         * Set the id attribute.
         * 
         * @param id
         *            the id to set
         * @return builder instance
         */
        @SuppressWarnings("unchecked")
        public T id(final Long id) {
            this.id = id;
            return (T) this;
        }

        /**
         * Set the created attribute.
         * 
         * @param created
         *            the created to set
         * @return builder instance
         */
        @SuppressWarnings("unchecked")
        public T created(final Calendar created) {
            this.created = created;
            return (T) this;
        }

        /**
         * Set the updated attribute.
         * 
         * @param updated
         *            the updated to set
         * @return builder instance
         */
        @SuppressWarnings("unchecked")
        public T updated(final Calendar updated) {
            this.updated = updated;
            return (T) this;
        }

    }
}
