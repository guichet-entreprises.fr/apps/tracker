/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.ge.tracker.data.service;

import java.util.List;

import fr.ge.common.utils.bean.search.SearchQueryFilter;
import fr.ge.common.utils.bean.search.SearchQueryOrder;
import fr.ge.tracker.data.bean.dto.MessageBean;
import fr.ge.tracker.data.bean.model.MessageEntity;

/**
 * The message service.
 *
 * @author $Author: jpauchet $
 * @version $Revision: 0 $
 */
public interface IMessageService extends IInternalService<MessageBean, MessageEntity> {

    /**
     * Gets the messages by an UID id, including its references messages.
     *
     * @param id
     *            the id of the UID
     * @return the messages
     */
    List<MessageBean> getMessagesByUidId(Long id);

    /**
     * Find by uid.
     *
     * @param <R>
     *            the generic type
     * @param id
     *            the id
     * @param expected
     *            the expected
     * @return the list
     */
    <R> List<R> findByUid(Long id, Class<? extends R> expected);

    /**
     * Searches with sorts and criteria.
     *
     * @param <R>
     *            the generic type
     * @param pageIndex
     *            the page index
     * @param pageSize
     *            the maximum number of results
     * @param ref
     *            the root reference
     * @param orders
     *            the sorts
     * @param expected
     *            expected bean class type
     * @return the search results
     */
    // <R> SearchResult<R> search(final int pageIndex, final int pageSize,
    // String ref, final List<String> orders, final Class<? extends R>
    // expected);

    /**
     * Searches with sorts and criteria.
     * 
     * @param startIndex
     *            the start index
     * @param maxResults
     *            the maximum results
     * @param filters
     *            the input filters
     * @param orders
     *            the input orders
     * @param expected
     *            the class expected
     * @return the search results
     */
    <R> fr.ge.common.utils.bean.search.SearchResult<R> search(final int startIndex, final int maxResults, final List<SearchQueryFilter> filters, final List<SearchQueryOrder> orders,
            final Class<? extends R> expected);

    /**
     * Remove messages related to associated reference.
     * 
     * @param key
     *            the Tracker reference key
     * @param terms
     *            the terms
     * @return the total of lines removed
     */
    long removeMessages(final String key, final String terms);

}
