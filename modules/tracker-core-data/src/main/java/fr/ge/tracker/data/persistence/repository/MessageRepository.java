/**
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2014-2016)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.ge.tracker.data.persistence.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import fr.ge.tracker.data.bean.model.MessageEntity;
import fr.ge.tracker.data.persistence.AbstractRepository;

/**
 * The repository of table "message".
 *
 * @author $Author: jpauchet $
 * @version $Revision: 0 $
 */
public interface MessageRepository extends AbstractRepository<MessageEntity> {

    /**
     * Gets the messages by an UID id, including its references messages.
     *
     * @param id
     *            the id of the UID
     * @return the messages
     */
    @Query(nativeQuery = true, name = "getMessagesByUidId", value = "with recursive parent(value) as ( " //
            + "select uid.value, uid.id from uid where uid.id = :id " //
            + "union all " //
            + "select child.value, child.id from parent, uid as child where child.uid_fk = parent.id " //
            + ") " //
            + "select message.* from parent inner join message on message.uid_fk = parent.id " //
            + "order by message.created ")
    List<MessageEntity> getMessagesByUidId(@Param("id") Long id);

    @Query( //
            nativeQuery = true, //
            value = "WITH RECURSIVE parent(value) as (\n" + //
                    "    SELECT m.value, m.id FROM uid m WHERE m.value = :ref\n" + //
                    "    UNION ALL\n" + //
                    "    SELECT c.value, c.id FROM parent p INNER JOIN uid c ON c.uid_fk = p.id\n" + //
                    ")\n" + //
                    "SELECT m.* FROM parent p INNER JOIN message m ON m.uid_fk = p.id \n--#pageable\n", //
            countQuery = "WITH RECURSIVE parent(value) as (\n" + //
                    "    SELECT r.value, r.id FROM uid r WHERE r.value = :ref\n" + //
                    "    UNION ALL\n" + //
                    "    SELECT c.value, c.id FROM parent p INNER JOIN uid c ON c.uid_fk = p.id\n" + //
                    ")\n" + //
                    "SELECT COUNT(1) FROM parent p INNER JOIN message m ON m.uid_fk = p.id" //
    )
    Page<MessageEntity> findAll(@Param("ref") String ref, Pageable pageable);

}
