/**
 * 
 */
package fr.ge.tracker.data.persistence.specification;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.jpa.domain.Specification;

/**
 * Datatable specification.
 * 
 * @author $Author: jpauchet $
 * @version $Revision: 0 $
 */
public final class GenericSpecification {

  /** Logger. */
  private static final Logger LOGGER = LoggerFactory.getLogger(GenericSpecification.class);

  /** Column name separator. */
  private static final String COLUMN_NAME_SEPARATOR = "\\.";

  /** Date input format. */
  private static final String DATE_INPUT_FORMAT = "dd/MM/yyyy";

  /** Date range mode. */
  private static final String DATE_RANGE_MODE = "date-range-mode";

  /** Number of parts for the date range mode. */
  private static final int DATE_RANGE_MODE_PARTS = 3;

  /** Empty mode. */
  private static final String EMPTY_MODE = "empty-mode";

  /** Number of parts for the empty mode. */
  private static final int EMPTY_MODE_PARTS = 2;

  /** Like clause wildcard. */
  private static final String LIKE_CLAUSE_WILDCARD = "%";

  /** Mode separator. */
  private static final String MODE_SEPARATOR = ":";

  /** True. */
  private static final String TRUE = "true";

  /**
   * Constructeur de la classe.
   */
  private GenericSpecification() {
    // Nothing to do.
  }

  /**
   * Specifies that search criteria are verified.
   * 
   * @param <T>
   *          the bean on which the specification is defined
   * @param criteria
   *          the criteria
   * @return a specification
   */
  public static <T> Specification < T > areSearchCriteriaVerified(final Map < String, String > criteria) {
    return new Specification < T >() {
      /**
       * {@inheritDoc}
       */
      public Predicate toPredicate(final Root < T > root, final CriteriaQuery < ? > query, final CriteriaBuilder cb) {
        Predicate predicate = null;
        Predicate newPredicate = null;
        if (criteria != null && !criteria.isEmpty()) {
          for (Entry < String, String > entry : criteria.entrySet()) {
            String[] valueSplit = entry.getValue().split(MODE_SEPARATOR, -1);
            if (valueSplit[0].equals(DATE_RANGE_MODE) && valueSplit.length == DATE_RANGE_MODE_PARTS) {
              newPredicate = applyDateRangeCriteria(root, cb, entry.getKey(), valueSplit, predicate);
            } else if (valueSplit[0].equals(EMPTY_MODE) && valueSplit.length == EMPTY_MODE_PARTS) {
              newPredicate = applyEmptyCriteria(root, cb, entry.getKey(), valueSplit, predicate);
            } else {
              newPredicate = applySimpleCriteria(root, cb, entry.getKey(), entry.getValue(), predicate);
            }
            predicate = mergePredicate(cb, predicate, newPredicate);
          }
        }
        return predicate;
      }
    };
  }

  /**
   * Finds the path relative to a column name.
   * 
   * @param <T>
   *          the bean on which the specification is defined
   * @param root
   *          the root
   * @param cb
   *          the criteria builder
   * @param columnName
   *          the column name
   * @return the path
   */
  private static <T> Path < Object > findPath(final Root < T > root, final CriteriaBuilder cb, final String columnName) {
    String[] columnNameSplit = columnName.split(COLUMN_NAME_SEPARATOR, -1);
    Path < Object > path = null;
    for (String columnNamePart : columnNameSplit) {
      if (path == null) {
        path = root.get(columnNamePart);
      } else {
        path = path.get(columnNamePart);
      }
    }
    return path;
  }

  /**
   * Applies date range criteria.
   * 
   * @param <T>
   *          the bean on which the specification is defined
   * @param root
   *          the root
   * @param cb
   *          the criteria builder
   * @param columnName
   *          the column name
   * @param columnSearchValue
   *          the column search value
   * @param predicate
   *          the predicate
   * @return the new predicate with applied criteria
   */
  private static <T> Predicate applyDateRangeCriteria(final Root < T > root, final CriteriaBuilder cb, final String columnName,
    final String[] columnSearchValue, final Predicate predicate) {
    Predicate newPredicate = null;
    try {
      Predicate searchMinDatePredicate = null;
      String searchMinDateStr = columnSearchValue[1];
      if (StringUtils.isNotEmpty(searchMinDateStr)) {
        Date searchMinDate = new SimpleDateFormat(DATE_INPUT_FORMAT).parse(searchMinDateStr);
        Calendar c = new DateTime(searchMinDate).withTimeAtStartOfDay().toCalendar(Locale.FRANCE);
        searchMinDatePredicate = cb.greaterThanOrEqualTo(findPath(root, cb, columnName).as(Calendar.class), c);
      }

      Predicate searchMaxDatePredicate = null;
      String searchMaxDateStr = columnSearchValue[2];
      if (StringUtils.isNotEmpty(searchMaxDateStr)) {
        Date searchMaxDate = new SimpleDateFormat(DATE_INPUT_FORMAT).parse(searchMaxDateStr);
        Calendar c = new DateTime(searchMaxDate).withTimeAtStartOfDay().plusDays(1).minusSeconds(1).toCalendar(Locale.FRANCE);
        searchMaxDatePredicate = cb.lessThanOrEqualTo(findPath(root, cb, columnName).as(Calendar.class), c);
      }

      newPredicate = mergePredicate(cb, searchMinDatePredicate, searchMaxDatePredicate);
    } catch (ParseException e) {
      LOGGER.debug(StringUtils.EMPTY, e);
    }
    return newPredicate;
  }

  /**
   * Applies empty criteria.
   * 
   * @param <T>
   *          the bean on which the specification is defined
   * @param root
   *          the root
   * @param cb
   *          the criteria builder
   * @param columnName
   *          the column name
   * @param columnSearchValue
   *          the column search value
   * @param predicate
   *          the predicate
   * @return the new predicate with applied criteria
   */
  private static <T> Predicate applyEmptyCriteria(final Root < T > root, final CriteriaBuilder cb, final String columnName,
    final String[] columnSearchValue, final Predicate predicate) {
    Predicate newPredicate = null;
    if (TRUE.equals(columnSearchValue[1])) {
      newPredicate = cb.isNull(findPath(root, cb, columnName));
    }
    return newPredicate;
  }

  /**
   * Applies simple criteria.
   * 
   * @param <T>
   *          the bean on which the specification is defined
   * @param root
   *          the root
   * @param cb
   *          the criteria builder
   * @param columnName
   *          the column name
   * @param columnSearchValue
   *          the column search value
   * @param predicate
   *          the predicate
   * @return the new predicate with applied criteria
   */
  private static <T> Predicate applySimpleCriteria(final Root < T > root, final CriteriaBuilder cb, final String columnName,
    final String columnSearchValue, final Predicate predicate) {
    Predicate newPredicate = cb.like(root.get(columnName).as(String.class),
      LIKE_CLAUSE_WILDCARD + columnSearchValue + LIKE_CLAUSE_WILDCARD);
    return newPredicate;
  }

  /**
   * Initializes or merges predicates.
   * 
   * @param cb
   *          the criteria builder
   * @param oldPredicate
   *          the old predicate
   * @param newPredicate
   *          the new predicate
   * @return the merged predicate
   */
  private static Predicate mergePredicate(final CriteriaBuilder cb, final Predicate oldPredicate, final Predicate newPredicate) {
    Predicate resultPredicate = null;
    if (newPredicate == null) {
      resultPredicate = oldPredicate;
    } else {
      if (oldPredicate == null) {
        resultPredicate = newPredicate;
      } else {
        resultPredicate = cb.and(oldPredicate, newPredicate);
      }
    }
    return resultPredicate;
  }

}
