/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.ge.tracker.data.bean.dto;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

/**
 * The bean that describes an unique id.
 *
 * @author $Author: jpauchet $
 * @version $Revision: 0 $
 */
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "value")
public class UidBean extends AbstractDatedBean<UidBean> {

    /** The value. */
    private String value;

    /** The client. */
    private ClientBean client;

    /** The parent uid. */
    private UidBean parent;

    /** The references to this uid. */
    private List<UidBean> references;

    /**
     * Default constructor.
     */
    public UidBean() {
        // Nothing to do
    }

    /**
     * Builder constructor.
     *
     * @param builder
     *            le builder
     */
    public UidBean(final Builder builder) {
        super(builder);
        value = builder.value;
        client = builder.client;
        parent = builder.parent;
        references = builder.references;
    }

    /**
     * Getter on attribute {@link #value}.
     *
     * @return String value
     */
    public String getValue() {
        return value;
    }

    /**
     * Setter on attribute {@link #value}.
     *
     * @param value
     *            the new value of attribute value
     */
    public void setValue(final String value) {
        this.value = value;
    }

    /**
     * Getter on attribute {@link #client}.
     *
     * @return ClientBean client
     */
    public ClientBean getClient() {
        return client;
    }

    /**
     * Setter on attribute {@link #client}.
     *
     * @param client
     *            the new value of attribute client
     */
    public void setClient(final ClientBean client) {
        this.client = client;
    }

    /**
     * Getter on attribute {@link #parent}.
     *
     * @return UidBean parent
     */
    public UidBean getParent() {
        return parent;
    }

    /**
     * Setter on attribute {@link #parent}.
     *
     * @param parent
     *            the new value of attribute parent
     */
    public void setParent(final UidBean parent) {
        this.parent = parent;
    }

    /**
     * Getter on attribute {@link #references}.
     *
     * @return List&lt;UidBean&gt; references
     */
    public List<UidBean> getReferences() {
        return references;
    }

    /**
     * Setter on attribute {@link #references}.
     *
     * @param references
     *            the new value of attribute references
     */
    public void setReferences(final List<UidBean> references) {
        this.references = references;
    }

    /**
     * Builder of Uid.
     *
     * @author $Author: jpauchet $
     * @version $Revision: 0 $
     */
    public static class Builder extends AbstractDatedBean.DatedBuilder<Builder> {

        /** The value. */
        private String value;

        /** The client. */
        private ClientBean client;

        /** The parent uid. */
        private UidBean parent;

        /** The references to this uid. */
        private List<UidBean> references;

        /**
         * Setter on attribute {@link #value}.
         *
         * @param value
         *            the new value of attribute value
         * @return builder instance
         */
        public Builder value(final String value) {
            this.value = value;
            return this;
        }

        /**
         * Setter on attribute {@link #client}.
         *
         * @param client
         *            the new value of attribute client
         * @return builder instance
         */
        public Builder client(final ClientBean client) {
            this.client = client;
            return this;
        }

        /**
         * Setter on attribute {@link #parent}.
         *
         * @param parent
         *            the new value of attribute parent
         * @return builder instance
         */
        public Builder parent(final UidBean parent) {
            this.parent = parent;
            return this;
        }

        /**
         * Setter on attribute {@link #references}.
         *
         * @param references
         *            the new value of attribute references
         * @return builder instance
         */
        public Builder references(final List<UidBean> references) {
            this.references = references;
            return this;
        }

        /**
         * Build.
         *
         * @return uid bean
         */
        public UidBean build() {
            return new UidBean(this);
        }

    }

}
