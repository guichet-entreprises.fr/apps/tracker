/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.ge.tracker.data.bean.dto;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * The bean that describes a client.
 *
 * @author $Author: jpauchet $
 * @version $Revision: 0 $
 */
public class ClientBean extends AbstractDatedBean<ClientBean> {

    /** The API key. */
    @JsonIgnore
    private String apiKey;

    /** The name. */
    private String name;

    /** The uids. */
    @JsonIgnore
    private List<UidBean> uids;

    /**
     * Default constructor.
     */
    public ClientBean() {
        // Nothing to do
    }

    /**
     * Builder constructor.
     *
     * @param builder
     *            le builder
     */
    public ClientBean(final Builder builder) {
        super(builder);
        this.apiKey = builder.apiKey;
        this.name = builder.name;
        this.uids = builder.uids;
    }

    /**
     * Getter on attribute {@link #apiKey}.
     *
     * @return api key
     */
    public String getApiKey() {
        return this.apiKey;
    }

    /**
     * Setter on attribute {@link #apiKey}.
     *
     * @param apiKey
     *            the new value of attribute api key
     * @return this
     */
    public ClientBean setApiKey(final String apiKey) {
        this.apiKey = apiKey;
        return this;
    }

    /**
     * Getter on attribute {@link #name}.
     *
     * @return name
     */
    public String getName() {
        return this.name;
    }

    /**
     * Setter on attribute {@link #name}.
     *
     * @param name
     *            the new value of attribute name
     * @return this
     */
    public ClientBean setName(final String name) {
        this.name = name;
        return this;
    }

    /**
     * Getter on attribute {@link #uids}.
     *
     * @return List&lt;UidBean&gt; uids
     */
    public List<UidBean> getUids() {
        return this.uids;
    }

    /**
     * Setter on attribute {@link #uids}.
     *
     * @param uids
     *            the new value of attribute uids
     */
    public void setUids(final List<UidBean> uids) {
        this.uids = uids;
    }

    /**
     * Builder of Client.
     *
     * @author $Author: jpauchet $
     * @version $Revision: 0 $
     */
    public static class Builder extends AbstractDatedBean.DatedBuilder<Builder> {

        /** The API key. */
        private String apiKey;

        /** The name. */
        private String name;

        /** The uids. */
        private List<UidBean> uids;

        /**
         * Setter on attribute {@link #apiKey}.
         *
         * @param apiKey
         *            the new value of attribute api key
         * @return builder instance
         */
        public Builder apiKey(final String apiKey) {
            this.apiKey = apiKey;
            return this;
        }

        /**
         * Setter on attribute {@link #name}.
         *
         * @param name
         *            the new value of attribute name
         * @return builder instance
         */
        public Builder name(final String name) {
            this.name = name;
            return this;
        }

        /**
         * Setter on attribute {@link #uids}.
         *
         * @param uids
         *            the new value of attribute uids
         * @return builder instance
         */
        public Builder uids(final List<UidBean> uids) {
            this.uids = uids;
            return this;
        }

        /**
         * Build.
         *
         * @return client bean
         */
        public ClientBean build() {
            return new ClientBean(this);
        }

    }
}
