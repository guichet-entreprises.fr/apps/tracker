/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.ge.tracker.data.bean.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * The bean that describes a message.
 *
 * @author $Author: jpauchet $
 * @version $Revision: 0 $
 */
public class MessageBean extends AbstractDatedBean<MessageBean> {

    /** The content. */
    private String content;

    /** The uid. */
    @JsonIgnore
    private UidBean uid;

    /** The client. */
    private ClientBean client;

    /**
     * Default constructor.
     */
    public MessageBean() {
        // Nothing to do
    }

    /**
     * Builder constructor.
     *
     * @param builder
     *            le builder
     */
    public MessageBean(final Builder builder) {
        super(builder);
        this.content = builder.content;
        this.uid = builder.uid;
        this.client = builder.client;
    }

    /**
     * Getter on attribute {@link #content}.
     *
     * @return String content
     */
    public String getContent() {
        return this.content;
    }

    /**
     * Setter on attribute {@link #content}.
     *
     * @param content
     *            the new value of attribute content
     * @return this
     */
    public MessageBean setContent(final String content) {
        this.content = content;
        return this;
    }

    /**
     * Getter on attribute {@link #uid}.
     *
     * @return uid
     */
    public UidBean getUid() {
        return this.uid;
    }

    /**
     * Setter on attribute {@link #uid}.
     *
     * @param uid
     *            the new value of attribute uid
     * @return this
     */
    public MessageBean setUid(final UidBean uid) {
        this.uid = uid;
        return this;
    }

    /**
     * Getter on attribute {@link #client}.
     *
     * @return ClientBean client
     */
    public ClientBean getClient() {
        return this.client;
    }

    /**
     * Setter on attribute {@link #client}.
     *
     * @param client
     *            the new value of attribute client
     * @return this
     */
    public MessageBean setClient(final ClientBean client) {
        this.client = client;
        return this;
    }

    /**
     * Builder of Message.
     *
     * @author $Author: jpauchet $
     * @version $Revision: 0 $
     */
    public static class Builder extends AbstractDatedBean.DatedBuilder<Builder> {

        /** The msg. */
        private String content;

        /** The uid. */
        private UidBean uid;

        /** The client. */
        private ClientBean client;

        /**
         * Setter on attribute {@link #content}.
         *
         * @param content
         *            the new value of attribute content
         * @return builder instance
         */
        public Builder content(final String content) {
            this.content = content;
            return this;
        }

        /**
         * Setter on attribute {@link #uid}.
         *
         * @param uid
         *            the new value of attribute uid
         * @return builder instance
         */
        public Builder uid(final UidBean uid) {
            this.uid = uid;
            return this;
        }

        /**
         * Setter on attribute {@link #client}.
         *
         * @param client
         *            the new value of attribute client
         * @return builder instance
         */
        public Builder client(final ClientBean client) {
            this.client = client;
            return this;
        }

        /**
         * Build.
         *
         * @return message bean
         */
        public MessageBean build() {
            return new MessageBean(this);
        }

    }

}
