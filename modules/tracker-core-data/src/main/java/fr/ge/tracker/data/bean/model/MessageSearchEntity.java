/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.ge.tracker.data.bean.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * The entity of table "message".
 *
 * @author $Author: jpauchet $
 * @version $Revision: 0 $
 */
@Entity
@Table(name = "message")
@SequenceGenerator(name = "seq_pk", sequenceName = "seq_message")
public class MessageSearchEntity extends DatedEntity {

  /** The content. */
  @Column(nullable = false, columnDefinition = "text")
  private String content;

  /** The main uid. */
  @ManyToOne
  @JoinColumn(name = "uid_fk")
  private UidSearchEntity uid;

  /**
   * Default constructor.
   */
  public MessageSearchEntity() {
    // Nothing to do
  }

  /**
   * Getter on attribute {@link #content}.
   *
   * @return String content
   */
  public String getContent() {
    return content;
  }

  /**
   * Setter on attribute {@link #content}.
   *
   * @param content
   *          the new value of attribute content
   */
  public void setContent(final String content) {
    this.content = content;
  }

  /**
   * Getter on attribute {@link #uid}.
   *
   * @return UidSearchEntity uid
   */
  public UidSearchEntity getUid() {
    return uid;
  }

  /**
   * Setter on attribute {@link #uid}.
   *
   * @param uid
   *          the new value of attribute uid
   */
  public void setUid(final UidSearchEntity uid) {
    this.uid = uid;
  }

}
