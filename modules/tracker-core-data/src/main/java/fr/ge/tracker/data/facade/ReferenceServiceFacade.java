/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.ge.tracker.data.facade;

import java.text.MessageFormat;
import java.util.List;
import java.util.Map;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import fr.ge.common.utils.bean.search.SearchQueryFilter;
import fr.ge.common.utils.bean.search.SearchQueryOrder;
import fr.ge.tracker.core.exception.ApiPermissionException;
import fr.ge.tracker.core.exception.FunctionalException;
import fr.ge.tracker.core.exception.MissingParameterException;
import fr.ge.tracker.data.bean.dto.ClientBean;
import fr.ge.tracker.data.bean.dto.MessageBean;
import fr.ge.tracker.data.bean.dto.UidBean;
import fr.ge.tracker.data.bean.transverse.SearchResult;
import fr.ge.tracker.data.service.IClientService;
import fr.ge.tracker.data.service.IMessageService;
import fr.ge.tracker.data.service.IUidSearchService;
import fr.ge.tracker.data.service.IUidService;
import fr.ge.tracker.data.util.UidFactoryImpl;

/**
 * The Class ReferenceServiceFacade.
 *
 * @author Christian Cougourdan
 */
@Component
public class ReferenceServiceFacade {

    /** The Constant LOGGER. */
    private static final Logger LOGGER = LoggerFactory.getLogger(ReferenceServiceFacade.class);

    /** The Constant MESSAGE_UID_CREATION. */
    private static final String MESSAGE_UID_CREATION = "Création de l''UID \"{0}\"";

    /** The is alert enabled. */
    @Value("${feature.alert.enabled:false}")
    private boolean isAlertEnabled;

    /** The uid factory. */
    @Autowired
    private UidFactoryImpl uidFactory;

    /** The uid service. */
    @Autowired
    private IUidService uidService;

    /** The uid search service. */
    @Autowired
    private IUidSearchService uidSearchService;

    /** The client service. */
    @Autowired
    private IClientService clientService;

    /** The message service. */
    @Autowired
    private IMessageService messageService;

    /**
     * Creates the.
     *
     * @param author
     *            the author
     * @return the string
     * @throws FunctionalException
     *             the functional exception
     */
    public String create(final String author) throws FunctionalException {
        if (StringUtils.isEmpty(author)) {
            LOGGER.warn("Creating new UID : missing author");
            throw new MissingParameterException("Missing author");
        }
        final ClientBean client = this.getOrBuildClient(author);

        String uid = null;
        UidBean uidBean = null;
        do {
            uid = this.uidFactory.uid();
            uidBean = this.uidService.load(uid);
        } while (uidBean != null);

        uidBean = new UidBean.Builder().value(uid).client(client).build();
        this.uidService.persist(uidBean);

        LOGGER.info("New UID created for {}", author);

        this.postMessage(uid, MessageFormat.format(MESSAGE_UID_CREATION, uid), author);

        return uid;
    }

    public String getOrCreate(final String ref, final String author) throws FunctionalException {
        if (StringUtils.isEmpty(author)) {
            LOGGER.warn("Get or create UID : missing author");
            throw new MissingParameterException("Missing author");
        }

        String newRootUid = null;

        final UidBean referenceBean = this.uidService.load(ref);

        if (null == referenceBean) {
            newRootUid = this.create(author);
            this.linkReferences(ref, newRootUid, author);

            LOGGER.info("Get or create UID for \"{}\" : UID \"{}\" created and linked", ref, newRootUid);
        } else {
            for (UidBean bean = referenceBean; null != bean; bean = bean.getParent()) {
                newRootUid = bean.getValue();
            }
        }

        return newRootUid;
    }

    /**
     * Searches with sorts and criteria.
     *
     * @param <T>
     *            the generic type
     * @param pageIndex
     *            the page index
     * @param maxResults
     *            the maximum number of results
     * @param sorts
     *            the sorts
     * @param criteria
     *            the criteria
     * @param messageSearchValue
     *            the message search value
     * @param expected
     *            the expected return type
     * @return the search results
     */
    public <T> SearchResult<T> search(final long pageIndex, final long maxResults, final List<String> sorts, final Map<String, String> criteria, final String messageSearchValue,
            final Class<T> expected) {
        if (messageSearchValue == null) {
            return this.uidService.search(pageIndex, maxResults, sorts, criteria, expected);
        } else {
            return this.uidSearchService.search(pageIndex, maxResults, sorts, criteria, messageSearchValue, expected);
        }
    }

    /**
     * Load.
     *
     * @param <T>
     *            the generic type
     * @param reference
     *            the reference
     * @param expected
     *            the expected
     * @return the t
     */
    public <T> T load(final String reference, final Class<T> expected) {
        LOGGER.info("Load information for \"{}\"", reference);
        return this.uidService.load(reference, expected);
    }

    /**
     * Load messages.
     *
     * @param <T>
     *            the generic type
     * @param id
     *            the id
     * @param expected
     *            the expected
     * @return the list
     */
    public <T> List<T> loadMessages(final long id, final Class<T> expected) {
        LOGGER.info("Load messages for \"{}\"", id);
        return this.messageService.findByUid(id, expected);
    }

    /**
     *
     * @param <T>
     *            the generic type
     * @param pageIndex
     *            the page index
     * @param maxResults
     *            the maximum number of results
     * @param ref
     * @param sorts
     *            the sorts
     * @param criteria
     *            the criteria
     * @param messageSearchValue
     *            the message search value
     * @param expected
     *            the expected return type
     * @return the search results
     */
    // public <R> SearchResult<R> searchMessages(final long startIndex, final
    // long maxResults, final String ref, final List<String> sorts, final
    // Class<R> expected) {
    // LOGGER.debug("Search messages for reference {}", ref);
    // return this.messageService.search((int) startIndex, (int) maxResults,
    // ref, sorts, expected);
    // }

    /**
     * Load parent.
     *
     * @param <T>
     *            the generic type
     * @param reference
     *            the reference
     * @param expected
     *            the expected
     * @return the t
     */
    public <T> T loadParent(final String reference, final Class<T> expected) {
        LOGGER.info("Load information for \"{}\" parent", reference);
        return this.uidService.parent(reference, expected);
    }

    /**
     * Link references.
     *
     * @param newReference
     *            the new reference
     * @param existingReference
     *            the existing reference
     * @param author
     *            the author
     * @return the string
     * @throws FunctionalException
     *             the functional exception
     */
    public String linkReferences(final String newReference, final String existingReference, final String author) throws FunctionalException {
        if (StringUtils.isEmpty(author)) {
            LOGGER.warn("Link between references : missing author");
            throw new MissingParameterException("Missing author");
        }
        final ClientBean client = this.getOrBuildClient(author);

        String response;

        UidBean newReferenceBean = this.uidService.load(newReference);
        UidBean existingReferenceBean = this.uidService.load(existingReference);

        if (null == newReferenceBean) {

            if (existingReferenceBean == null) {
                // ref does not exist and uid does not exist
                final String newRootUid = this.create(author);
                this.linkReferences(existingReference, newRootUid, author);
                existingReferenceBean = this.uidService.load(existingReference);
                response = "Reference linked to this UID, which didn't exist and was created on the fly";
            } else {
                response = "Reference linked to this UID";
            }

            // ref does not exist and uid exists
            newReferenceBean = new UidBean.Builder().value(newReference).client(client).parent(existingReferenceBean).build();
            this.uidService.persist(newReferenceBean);

            LOGGER.info("Link between references \"{}\" and \"{}\" : {}", newReference, existingReference, response);

        } else {

            if (null == existingReferenceBean) {
                /*
                 * new reference already exists but not existing one
                 */
                LOGGER.warn("Link between references : reference \"{}\" already linked to \"{}\", but this last one does not exists. Reverse link between references", newReference, existingReference);
                this.linkReferences(existingReference, newReference, author);
                response = "Reference reversed linked to UID, which didn't exist and was created on the fly";
            } else if (null == newReferenceBean.getParent()) {
                /*
                 * both references already exists and new one has no parent
                 */
                newReferenceBean.setParent(existingReferenceBean);
                this.uidService.persist(newReferenceBean);

                response = "Reference linked to this UID";
                LOGGER.info("Link between references \"{}\" and \"{}\" : {}", newReference, existingReference, response);

            } else if (!existingReferenceBean.getValue().equals(newReferenceBean.getParent().getValue())) {
                /*
                 * both references already exists but are not linked together
                 */
                LOGGER.warn("Link between references : reference \"{}\" already linked to another one (\"{}\"), but not given one (\"{}\")", newReference, newReferenceBean.getParent().getValue(),
                        existingReference);
                throw new FunctionalException("Reference already linked to another UID and this UID exists");
            } else {
                /*
                 * new and existing references already linked together
                 */
                LOGGER.info("Link between references : reference \"{}\" already linked to \"{}\"", newReference, existingReference);
                response = "Reference already linked to this UID";
            }

        }

        return response;
    }

    /**
     * Post message.
     *
     * @param reference
     *            the reference
     * @param message
     *            the message
     * @param author
     *            the author
     * @return the string
     * @throws FunctionalException
     *             the functional exception
     */
    public String postMessage(final String reference, final String message, final String author) throws FunctionalException {
        if (StringUtils.isEmpty(reference)) {
            LOGGER.warn("Post message : missing reference");
            throw new MissingParameterException("Missing reference");
        }
        if (StringUtils.isEmpty(message)) {
            LOGGER.warn("Post message to \"{}\" : missing content", reference);
            throw new MissingParameterException("Missing content");
        }
        if (StringUtils.isEmpty(author)) {
            LOGGER.warn("Post message to \"{}\" : missing author", reference);
            throw new MissingParameterException("Missing author");
        }
        final ClientBean client = this.getOrBuildClient(author);

        String response = "Message created and linked to this reference";

        UidBean uidBean = this.uidService.load(reference);
        if (uidBean == null) {
            response = "Message created and linked to this reference, which didn't exist and was created on the fly";
            final String rootUid = this.create(author);
            this.linkReferences(reference, rootUid, author);
            uidBean = this.uidService.load(reference);
        }

        MessageBean msgBean = new MessageBean.Builder().content(message).client(client).uid(uidBean).build();
        msgBean = this.messageService.persist(msgBean);

        LOGGER.info("Message posted to \"{}\" : {}", reference, response);

        return response;
    }

    /**
     * Gets an existing client.
     *
     * @param author
     *            the API key
     * @return a client
     * @throws FunctionalException
     *             a {@link FunctionalException}
     */
    private ClientBean getClient(final String author) throws FunctionalException {
        final ClientBean client = this.clientService.load(author);
        if (client == null) {
            throw new ApiPermissionException("Invalid API key");
        }
        return client;
    }

    /**
     * * Gets an existing client or builds it. * * @param author * the author
     * * @return a client
     */
    private ClientBean getOrBuildClient(final String author) {
        final String apiKey = DigestUtils.sha1Hex(author);
        ClientBean client = this.clientService.load(apiKey);
        if (client == null) {
            client = new ClientBean.Builder().name(author).apiKey(apiKey).build();
            client = this.clientService.persist(client);
        }
        return client;
    }

    /**
     *
     * @param <T>
     *            the generic type
     * @param pageIndex
     *            the page index
     * @param maxResults
     *            the maximum number of results
     * @param ref
     * @param sorts
     *            the sorts
     * @param criteria
     *            the criteria
     * @param messageSearchValue
     *            the message search value
     * @param expected
     *            the expected return type
     * @return the search results
     */
    // public <R> SearchResult<R> searchMessages(final long startIndex, final
    // long maxResults, final String ref, final List<String> sorts, final
    // Class<R> expected) {
    // LOGGER.debug("Search messages for reference {}", ref);
    // return this.messageService.search((int) startIndex, (int) maxResults,
    // ref, sorts, expected);
    // }

    /**
     * @param startIndex
     *            the page index
     * @param maxResults
     *            the maximum result
     * @param filters
     *            the input filters
     * @param orders
     *            the input orders criteria
     * @param expected
     *            the expected return type
     * @return list of messages
     */
    public <R> fr.ge.common.utils.bean.search.SearchResult<R> searchMessages(final long startIndex, final long maxResults, final List<SearchQueryFilter> filters, final List<SearchQueryOrder> orders,
            final Class<R> expected) {
        return this.messageService.search((int) startIndex, (int) maxResults, filters, orders, expected);
    }

    /**
     * Remove messages.
     * 
     * @param key
     *            the Tracker key reference
     * @param terms
     *            the term to search into message content
     * @return the total number of lines removed
     */
    public long removeMessages(final String key, final String terms) {
        return this.messageService.removeMessages(key, terms);
    }
}
