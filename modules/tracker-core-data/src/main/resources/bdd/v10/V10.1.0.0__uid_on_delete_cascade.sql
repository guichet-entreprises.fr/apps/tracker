-- --------------------------------------------------
-- Constraints on delete cascade for uid and messages
-- --------------------------------------------------
ALTER TABLE message DROP CONSTRAINT fkq60kbh5un603q1tlfqt0wnypj;
ALTER TABLE message ADD CONSTRAINT message_uid_fk FOREIGN KEY (uid_fk) REFERENCES "uid" (id) MATCH SIMPLE ON UPDATE NO ACTION ON DELETE CASCADE;
