SET statement_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET default_tablespace = '';
SET default_with_oids = false;

DROP SEQUENCE IF EXISTS seq_action_param;
DROP SEQUENCE IF EXISTS seq_action;
DROP SEQUENCE IF EXISTS seq_pattern;
DROP SEQUENCE IF EXISTS seq_alert;
DROP SEQUENCE IF EXISTS seq_tag;
