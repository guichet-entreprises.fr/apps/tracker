--
-- PostgreSQL database dump
--

-- Dumped from database version 9.4.4
-- Dumped by pg_dump version 9.4.4
-- Started on 2016-06-10 14:23:18

SET statement_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 180 (class 1259 OID 34411)
-- Name: action; Type: TABLE; Schema: public; Owner: tracker_usr; Tablespace: 
--

CREATE TABLE action (
    id bigint NOT NULL,
    created timestamp without time zone NOT NULL,
    updated timestamp without time zone NOT NULL,
    type character varying(255) NOT NULL,
    alert_fk bigint
);


 

--
-- TOC entry 181 (class 1259 OID 34416)
-- Name: action_param; Type: TABLE; Schema: public; Owner: tracker_usr; Tablespace: 
--

CREATE TABLE action_param (
    id bigint NOT NULL,
    created timestamp without time zone NOT NULL,
    updated timestamp without time zone NOT NULL,
    key character varying(255) NOT NULL,
    value character varying(255) NOT NULL,
    action_fk bigint
);


 

--
-- TOC entry 182 (class 1259 OID 34424)
-- Name: alert; Type: TABLE; Schema: public; Owner: tracker_usr; Tablespace: 
--

CREATE TABLE alert (
    id bigint NOT NULL,
    created timestamp without time zone NOT NULL,
    updated timestamp without time zone NOT NULL,
    name character varying(255) NOT NULL
);


 

--
-- TOC entry 183 (class 1259 OID 34429)
-- Name: client; Type: TABLE; Schema: public; Owner: tracker_usr; Tablespace: 
--

CREATE TABLE client (
    id bigint NOT NULL,
    created timestamp without time zone NOT NULL,
    updated timestamp without time zone NOT NULL,
    apikey character varying(255) NOT NULL,
    name character varying(255) NOT NULL
);


 

--
-- TOC entry 184 (class 1259 OID 34437)
-- Name: message; Type: TABLE; Schema: public; Owner: tracker_usr; Tablespace: 
--

CREATE TABLE message (
    id bigint NOT NULL,
    created timestamp without time zone NOT NULL,
    updated timestamp without time zone NOT NULL,
    content character varying(255) NOT NULL,
    client_fk bigint,
    uid_fk bigint
);


 

--
-- TOC entry 185 (class 1259 OID 34442)
-- Name: message_tag; Type: TABLE; Schema: public; Owner: tracker_usr; Tablespace: 
--

CREATE TABLE message_tag (
    message_id bigint NOT NULL,
    tag_id bigint NOT NULL
);


 

--
-- TOC entry 186 (class 1259 OID 34445)
-- Name: pattern; Type: TABLE; Schema: public; Owner: tracker_usr; Tablespace: 
--

CREATE TABLE pattern (
    id bigint NOT NULL,
    created timestamp without time zone NOT NULL,
    updated timestamp without time zone NOT NULL,
    regex character varying(255) NOT NULL,
    alert_fk bigint,
    client_fk bigint
);


 

--
-- TOC entry 176 (class 1259 OID 34300)
-- Name: seq_action; Type: SEQUENCE; Schema: public; Owner: tracker_usr
--

CREATE SEQUENCE seq_action
    START WITH 1
    INCREMENT BY 50
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


 

--
-- TOC entry 177 (class 1259 OID 34302)
-- Name: seq_action_param; Type: SEQUENCE; Schema: public; Owner: tracker_usr
--

CREATE SEQUENCE seq_action_param
    START WITH 1
    INCREMENT BY 50
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


 

--
-- TOC entry 178 (class 1259 OID 34304)
-- Name: seq_alert; Type: SEQUENCE; Schema: public; Owner: tracker_usr
--

CREATE SEQUENCE seq_alert
    START WITH 1
    INCREMENT BY 50
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


 

--
-- TOC entry 172 (class 1259 OID 34169)
-- Name: seq_client; Type: SEQUENCE; Schema: public; Owner: tracker_usr
--

CREATE SEQUENCE seq_client
    START WITH 1
    INCREMENT BY 50
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


 

--
-- TOC entry 173 (class 1259 OID 34171)
-- Name: seq_message; Type: SEQUENCE; Schema: public; Owner: tracker_usr
--

CREATE SEQUENCE seq_message
    START WITH 1
    INCREMENT BY 50
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


 

--
-- TOC entry 179 (class 1259 OID 34306)
-- Name: seq_pattern; Type: SEQUENCE; Schema: public; Owner: tracker_usr
--

CREATE SEQUENCE seq_pattern
    START WITH 1
    INCREMENT BY 50
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


 

--
-- TOC entry 175 (class 1259 OID 34196)
-- Name: seq_tag; Type: SEQUENCE; Schema: public; Owner: tracker_usr
--

CREATE SEQUENCE seq_tag
    START WITH 1
    INCREMENT BY 50
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


 

--
-- TOC entry 174 (class 1259 OID 34173)
-- Name: seq_uid; Type: SEQUENCE; Schema: public; Owner: tracker_usr
--

CREATE SEQUENCE seq_uid
    START WITH 1
    INCREMENT BY 50
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


 

--
-- TOC entry 187 (class 1259 OID 34450)
-- Name: tag; Type: TABLE; Schema: public; Owner: tracker_usr; Tablespace: 
--

CREATE TABLE tag (
    id bigint NOT NULL,
    created timestamp without time zone NOT NULL,
    updated timestamp without time zone NOT NULL,
    colour character varying(255) NOT NULL,
    name character varying(255) NOT NULL
);


 

--
-- TOC entry 188 (class 1259 OID 34458)
-- Name: uid; Type: TABLE; Schema: public; Owner: tracker_usr; Tablespace: 
--

CREATE TABLE uid (
    id bigint NOT NULL,
    created timestamp without time zone NOT NULL,
    updated timestamp without time zone NOT NULL,
    value character varying(255) NOT NULL,
    client_fk bigint,
    uid_fk bigint
);


 

--
-- TOC entry 1933 (class 2606 OID 34423)
-- Name: action_param_pkey; Type: CONSTRAINT; Schema: public; Owner: tracker_usr; Tablespace: 
--

ALTER TABLE ONLY action_param
    ADD CONSTRAINT action_param_pkey PRIMARY KEY (id);


--
-- TOC entry 1931 (class 2606 OID 34415)
-- Name: action_pkey; Type: CONSTRAINT; Schema: public; Owner: tracker_usr; Tablespace: 
--

ALTER TABLE ONLY action
    ADD CONSTRAINT action_pkey PRIMARY KEY (id);


--
-- TOC entry 1935 (class 2606 OID 34428)
-- Name: alert_pkey; Type: CONSTRAINT; Schema: public; Owner: tracker_usr; Tablespace: 
--

ALTER TABLE ONLY alert
    ADD CONSTRAINT alert_pkey PRIMARY KEY (id);


--
-- TOC entry 1937 (class 2606 OID 34436)
-- Name: client_pkey; Type: CONSTRAINT; Schema: public; Owner: tracker_usr; Tablespace: 
--

ALTER TABLE ONLY client
    ADD CONSTRAINT client_pkey PRIMARY KEY (id);


--
-- TOC entry 1939 (class 2606 OID 34441)
-- Name: message_pkey; Type: CONSTRAINT; Schema: public; Owner: tracker_usr; Tablespace: 
--

ALTER TABLE ONLY message
    ADD CONSTRAINT message_pkey PRIMARY KEY (id);


--
-- TOC entry 1941 (class 2606 OID 34449)
-- Name: pattern_pkey; Type: CONSTRAINT; Schema: public; Owner: tracker_usr; Tablespace: 
--

ALTER TABLE ONLY pattern
    ADD CONSTRAINT pattern_pkey PRIMARY KEY (id);


--
-- TOC entry 1943 (class 2606 OID 34457)
-- Name: tag_pkey; Type: CONSTRAINT; Schema: public; Owner: tracker_usr; Tablespace: 
--

ALTER TABLE ONLY tag
    ADD CONSTRAINT tag_pkey PRIMARY KEY (id);


--
-- TOC entry 1945 (class 2606 OID 34462)
-- Name: uid_pkey; Type: CONSTRAINT; Schema: public; Owner: tracker_usr; Tablespace: 
--

ALTER TABLE ONLY uid
    ADD CONSTRAINT uid_pkey PRIMARY KEY (id);


--
-- TOC entry 1947 (class 2606 OID 34468)
-- Name: fk115eem80vd4811btafsabx8ju; Type: FK CONSTRAINT; Schema: public; Owner: tracker_usr
--

ALTER TABLE ONLY action_param
    ADD CONSTRAINT fk115eem80vd4811btafsabx8ju FOREIGN KEY (action_fk) REFERENCES action(id);


--
-- TOC entry 1952 (class 2606 OID 34493)
-- Name: fk2di7wt0a2cc97v9yd3480fe4c; Type: FK CONSTRAINT; Schema: public; Owner: tracker_usr
--

ALTER TABLE ONLY pattern
    ADD CONSTRAINT fk2di7wt0a2cc97v9yd3480fe4c FOREIGN KEY (alert_fk) REFERENCES alert(id);


--
-- TOC entry 1948 (class 2606 OID 34473)
-- Name: fkaa3nmymnsyg7jjjrngea97sm3; Type: FK CONSTRAINT; Schema: public; Owner: tracker_usr
--

ALTER TABLE ONLY message
    ADD CONSTRAINT fkaa3nmymnsyg7jjjrngea97sm3 FOREIGN KEY (client_fk) REFERENCES client(id);


--
-- TOC entry 1954 (class 2606 OID 34503)
-- Name: fkcsmvu2n48u42h0q9fjinvnpi4; Type: FK CONSTRAINT; Schema: public; Owner: tracker_usr
--

ALTER TABLE ONLY uid
    ADD CONSTRAINT fkcsmvu2n48u42h0q9fjinvnpi4 FOREIGN KEY (client_fk) REFERENCES client(id);


--
-- TOC entry 1955 (class 2606 OID 34508)
-- Name: fkf355u6sspttoj0mek0ewnmigy; Type: FK CONSTRAINT; Schema: public; Owner: tracker_usr
--

ALTER TABLE ONLY uid
    ADD CONSTRAINT fkf355u6sspttoj0mek0ewnmigy FOREIGN KEY (uid_fk) REFERENCES uid(id);


--
-- TOC entry 1953 (class 2606 OID 34498)
-- Name: fkfsiwanhmpvr0crmcidqq4193x; Type: FK CONSTRAINT; Schema: public; Owner: tracker_usr
--

ALTER TABLE ONLY pattern
    ADD CONSTRAINT fkfsiwanhmpvr0crmcidqq4193x FOREIGN KEY (client_fk) REFERENCES client(id);


--
-- TOC entry 1951 (class 2606 OID 34488)
-- Name: fkhmcripvpsrvhfri911hbj9ac8; Type: FK CONSTRAINT; Schema: public; Owner: tracker_usr
--

ALTER TABLE ONLY message_tag
    ADD CONSTRAINT fkhmcripvpsrvhfri911hbj9ac8 FOREIGN KEY (message_id) REFERENCES message(id);


--
-- TOC entry 1946 (class 2606 OID 34463)
-- Name: fkigtmucyhwt8un6eghhy5vcyxo; Type: FK CONSTRAINT; Schema: public; Owner: tracker_usr
--

ALTER TABLE ONLY action
    ADD CONSTRAINT fkigtmucyhwt8un6eghhy5vcyxo FOREIGN KEY (alert_fk) REFERENCES alert(id);


--
-- TOC entry 1950 (class 2606 OID 34483)
-- Name: fkn0x58h0r1fs1fxqkldow0rh4h; Type: FK CONSTRAINT; Schema: public; Owner: tracker_usr
--

ALTER TABLE ONLY message_tag
    ADD CONSTRAINT fkn0x58h0r1fs1fxqkldow0rh4h FOREIGN KEY (tag_id) REFERENCES tag(id);


--
-- TOC entry 1949 (class 2606 OID 34478)
-- Name: fkq60kbh5un603q1tlfqt0wnypj; Type: FK CONSTRAINT; Schema: public; Owner: tracker_usr
--

ALTER TABLE ONLY message
    ADD CONSTRAINT fkq60kbh5un603q1tlfqt0wnypj FOREIGN KEY (uid_fk) REFERENCES uid(id);

-- Completed on 2016-06-10 14:23:18

--
-- PostgreSQL database dump complete
--

