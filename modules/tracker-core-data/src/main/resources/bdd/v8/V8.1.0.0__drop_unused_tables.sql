SET statement_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET default_tablespace = '';
SET default_with_oids = false;

DROP TABLE action_param;
DROP TABLE action;
DROP TABLE pattern;
DROP TABLE alert;
DROP TABLE message_tag;
DROP TABLE tag;
