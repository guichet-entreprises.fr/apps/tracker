/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.ge.tracker.adapter.urssaf.service.impl;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import javax.jws.WebService;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import fr.extelia.mgun.ws.MGUNPortType;
import fr.extelia.mgun.ws.objet.xsd.ActionPieceManquante;
import fr.extelia.mgun.ws.objet.xsd.DonneesCfeIncompetent;
import fr.extelia.mgun.ws.objet.xsd.DonneesLiasseBOM;
import fr.extelia.mgun.ws.objet.xsd.EtatFormaliteReponse;
import fr.extelia.mgun.ws.objet.xsd.EtatFormaliteRequete;
import fr.extelia.mgun.ws.objet.xsd.MiseAJourFormaliteReponse;
import fr.extelia.mgun.ws.objet.xsd.MiseAJourFormaliteRequete;
import fr.extelia.mgun.ws.objet.xsd.OperationBOMSurLiasse;
import fr.extelia.mgun.ws.objet.xsd.PieceManquante;
import fr.ge.tracker.core.exception.FunctionalException;
import fr.ge.tracker.core.util.CoreUtil;
import fr.ge.tracker.data.facade.ReferenceServiceFacade;

/**
 * URSSAF service implementation.
 *
 * @author Christian Cougourdan
 */
@WebService(endpointInterface = "fr.extelia.mgun.ws.MGUNPortType")
public class UrssafServiceImpl implements MGUNPortType {

    /** Default author. */
    private static final String DEFAULT_AUTHOR = "Urssaf";

    /** Logger. */
    private static final Logger LOGGER = LoggerFactory.getLogger(UrssafServiceImpl.class);

    /** Template cache. */
    private static Map<String, String> templateCache = new ConcurrentHashMap<>();

    /** Complete messages ?. */
    @Value("${feature.urssaf.complete_messages:false}")
    private boolean isCompleteMessages;

    /** Reference service facade. */
    @Autowired
    ReferenceServiceFacade referenceServiceFacade;

    /**
     * {@inheritDoc}
     */
    @Override
    public EtatFormaliteReponse getEtatFormalite(final EtatFormaliteRequete requete) {
        return null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MiseAJourFormaliteReponse miseAJourFormalite(final MiseAJourFormaliteRequete requete) {
        final Collection<String> messages = new ArrayList<>();

        if (this.isCompleteMessages) {
            messages.addAll(this.buildMessagesForDonneesLiasseBOM(requete));
            messages.addAll(this.buildMessagesForBomOperations(requete));
            messages.addAll(this.buildMessagesForActionsPiecesManquantes(requete));
        } else {
            messages.addAll(this.buildSimpleMessages(requete));
        }

        try {
            final Collection<String> references = this.buildReferences(requete);

            if (CollectionUtils.isNotEmpty(references)) {
                final String reference = references.iterator().next();
                for (final String msg : messages) {
                    if (StringUtils.isNotEmpty(msg)) {
                        this.referenceServiceFacade.postMessage(reference, msg, DEFAULT_AUTHOR);
                    }
                }
            }
        } catch (final FunctionalException ex) {
            LOGGER.warn("Unable to post message", ex);
        }

        return null;
    }

    /**
     * Builds simple messages.
     *
     * @param requete
     *            the request
     * @return the messages
     */
    private Collection<? extends String> buildSimpleMessages(final MiseAJourFormaliteRequete requete) {
        final Collection<String> messages = new ArrayList<>();

        if (null != requete.getOperationBOMSurLiasse()) {
            final OperationBOMSurLiasse operation = requete.getOperationBOMSurLiasse();
            messages.add(operation.getCommentaireTraitement());

            LOGGER.info("Register status \"{}\" on record \"{}\"", operation.getEtatFormalite(), requete.getNumeroLiasse());
        }

        return messages;
    }

    /**
     * Builds the messages for BOM operations.
     *
     * @param requete
     *            the request
     * @return the messages
     */
    private Collection<? extends String> buildMessagesForBomOperations(final MiseAJourFormaliteRequete requete) {
        final Collection<String> messages = new ArrayList<>();

        if (null != requete.getOperationBOMSurLiasse()) {
            final OperationBOMSurLiasse operation = requete.getOperationBOMSurLiasse();
            messages.add( //
                    MessageFormat.format( //
                            getMessageTemplate("operation-bom"), //
                            operation.getCodeOperateur(), //
                            operation.getEtatFormalite(), //
                            operation.getCommentaireTraitement() //
                    ) //
            );
            LOGGER.info("Register status \"{}\" on record \"{}\"", operation.getEtatFormalite(), requete.getNumeroLiasse());
        }

        return messages;
    }

    /**
     * Build messages for BOM liasse data.
     *
     * @param requete
     *            the requete
     * @return messages collection
     */
    private Collection<? extends String> buildMessagesForDonneesLiasseBOM(final MiseAJourFormaliteRequete requete) {
        final Collection<String> messages = new ArrayList<>();

        if (null != requete.getDonneesLiasseBOM()) {
            final DonneesLiasseBOM liasse = requete.getDonneesLiasseBOM();
            final DonneesCfeIncompetent cfe = liasse.getDonneesCfeIncompetent();
            messages.add( //
                    MessageFormat.format( //
                            getMessageTemplate("donnees-liasse"), //
                            liasse.getNumeroSiren(), //
                            liasse.getReferenceDossier(), //
                            liasse.getReferenceLiasse(), //
                            null == cfe ? "" : cfe.getCodeEdiCfeCompetent(), //
                            null == cfe ? "" : cfe.getCommentaire() //
                    ) //
            );
        }

        return messages;
    }

    /**
     * Build messages for every missing piece action.
     *
     * @param requete
     *            the requete
     * @return messages collection
     */
    private Collection<? extends String> buildMessagesForActionsPiecesManquantes(final MiseAJourFormaliteRequete requete) {
        final Collection<String> messages = new ArrayList<>();

        if (null != requete.getActionsPiecesManquantes()) {
            final String tpl = getMessageTemplate("action-pieces-manquantes");
            for (final ActionPieceManquante actionPieceManquante : requete.getActionsPiecesManquantes()) {
                actionPieceManquante.getAction();
                if (null != actionPieceManquante.getPieceManquante()) {
                    final PieceManquante pieceManquante = actionPieceManquante.getPieceManquante();
                    final String msg = MessageFormat.format( //
                            tpl, //
                            pieceManquante.getFamille(), //
                            pieceManquante.getNumero(), //
                            pieceManquante.getDateHeureDemande(), //
                            pieceManquante.getDateHeureReception(), //
                            pieceManquante.getCommentaire() //
                    );
                    messages.add(msg);
                }
            }
        }

        return messages;
    }

    /**
     * Retrieve all potential references from request, linking them if multiples
     * found.
     *
     * @param requete
     *            request object
     * @return reference collection
     * @throws FunctionalException
     *             the functional exception
     */
    private Collection<String> buildReferences(final MiseAJourFormaliteRequete requete) throws FunctionalException {
        final Collection<String> references = new HashSet<>();

        if (null != requete.getNumeroLiasse()) {
            references.add(requete.getNumeroLiasse());
        }

        if (null != requete.getDonneesLiasseBOM()) {
            if (null != requete.getDonneesLiasseBOM().getReferenceLiasse()) {
                references.add(requete.getDonneesLiasseBOM().getReferenceLiasse());
            }
            if (null != requete.getDonneesLiasseBOM().getReferenceDossier()) {
                references.add(requete.getDonneesLiasseBOM().getReferenceDossier());
            }
        }

        if (references.size() > 1) {
            final Iterator<String> it = references.iterator();
            final String mainReference = it.next();
            while (it.hasNext()) {
                this.referenceServiceFacade.linkReferences(it.next(), mainReference, DEFAULT_AUTHOR);
            }
        }

        return references;
    }

    /**
     * Gets the message template.
     * 
     * @param name
     *            the name
     * @return the template
     */
    private static String getMessageTemplate(final String name) {
        String tpl = templateCache.get(name);
        if (null == tpl) {
            tpl = CoreUtil.resourceAsString(name + ".message", UrssafServiceImpl.class);
            templateCache.put(name, tpl);
        }

        return tpl;
    }

}
