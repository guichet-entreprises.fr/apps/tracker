/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package fr.ge.tracker.adapter.urssaf.service.impl;

import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.verify;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import fr.extelia.mgun.ws.MGUNPortType;
import fr.extelia.mgun.ws.objet.xsd.EtatFormaliteReponse;
import fr.extelia.mgun.ws.objet.xsd.IdentifiantAppelant;
import fr.extelia.mgun.ws.objet.xsd.MiseAJourFormaliteReponse;
import fr.extelia.mgun.ws.objet.xsd.MiseAJourFormaliteRequete;
import fr.extelia.mgun.ws.objet.xsd.OperationBOMSurLiasse;
import fr.ge.tracker.data.facade.ReferenceServiceFacade;

/**
 * The Class UrssafServiceImplTest.
 *
 * @author Christian Cougourdan
 */
@RunWith(SpringJUnit4ClassRunner.class)
@DirtiesContext
@ContextConfiguration(locations = { "classpath:spring/test-context.xml" })
public class UrssafServiceImplTest {

    /** The reference service facade. */
    @Autowired
    ReferenceServiceFacade referenceServiceFacade;

    /** The urssaf client. */
    @Autowired
    @Qualifier("urssafClient")
    private MGUNPortType urssafClient;

    /**
     * Sets the up.
     *
     * @throws Exception
     *             the exception
     */
    @Before
    public void setUp() throws Exception {
        reset(this.referenceServiceFacade);
    }

    /**
     * Test get etat formalite.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    public void testGetEtatFormalite() throws Exception {
        final EtatFormaliteReponse actual = this.urssafClient.getEtatFormalite(null);

        assertNull(actual);
    }

    /**
     * Test mise A jour formalite.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    public void testMiseAJourFormalite() throws Exception {
        final String recordId = "HXXXXXXXXXXX";
        final String message = "Réception de la liasse GU dans le CFE compétent";

        final OperationBOMSurLiasse operationBOMSurLiasse = new OperationBOMSurLiasse();
        operationBOMSurLiasse.setCodeOperateur("CFE");
        operationBOMSurLiasse.setEtatFormalite("B_INTEGRE");
        operationBOMSurLiasse.setCommentaireTraitement(message);

        final IdentifiantAppelant identifiantAppelant = new IdentifiantAppelant();
        identifiantAppelant.setCodeEdi("U9301");
        identifiantAppelant.setMotDePasse("gbwBEL5");

        final MiseAJourFormaliteRequete request = new MiseAJourFormaliteRequete();
        request.setNumeroLiasse(recordId);
        request.setOperationBOMSurLiasse(operationBOMSurLiasse);
        request.setIdentifiantAppelant(identifiantAppelant);

        final MiseAJourFormaliteReponse actual = this.urssafClient.miseAJourFormalite(request);

        verify(this.referenceServiceFacade).postMessage(recordId, message, "Urssaf");
    }

}
