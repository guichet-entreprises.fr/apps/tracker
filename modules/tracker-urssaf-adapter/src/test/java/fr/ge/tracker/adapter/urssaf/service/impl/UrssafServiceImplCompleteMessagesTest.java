/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package fr.ge.tracker.adapter.urssaf.service.impl;

import static org.junit.Assert.assertNull;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.verify;

import java.util.List;
import java.util.Properties;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import fr.extelia.mgun.ws.MGUNPortType;
import fr.extelia.mgun.ws.objet.xsd.ActionPieceManquante;
import fr.extelia.mgun.ws.objet.xsd.DonneesCfeIncompetent;
import fr.extelia.mgun.ws.objet.xsd.DonneesLiasseBOM;
import fr.extelia.mgun.ws.objet.xsd.EtatFormaliteReponse;
import fr.extelia.mgun.ws.objet.xsd.IdentifiantAppelant;
import fr.extelia.mgun.ws.objet.xsd.MiseAJourFormaliteReponse;
import fr.extelia.mgun.ws.objet.xsd.MiseAJourFormaliteRequete;
import fr.extelia.mgun.ws.objet.xsd.OperationBOMSurLiasse;
import fr.extelia.mgun.ws.objet.xsd.PieceManquante;
import fr.ge.tracker.data.facade.ReferenceServiceFacade;

/**
 * The Class UrssafServiceImplCompleteMessagesTest.
 *
 * @author Christian Cougourdan
 */
@RunWith(SpringJUnit4ClassRunner.class)
@DirtiesContext
@ContextConfiguration(locations = { "classpath:spring/test-context-complete-messages.xml", "classpath:spring/test-context.xml" })
public class UrssafServiceImplCompleteMessagesTest {

    /** The reference service facade. */
    @Autowired
    ReferenceServiceFacade referenceServiceFacade;

    /** The urssaf client. */
    @Autowired
    @Qualifier("urssafClient")
    private MGUNPortType urssafClient;

    /** The app properties. */
    @Autowired
    private Properties appProperties;

    /**
     * Sets the up.
     *
     * @throws Exception
     *             the exception
     */
    @Before
    public void setUp() throws Exception {
        reset(this.referenceServiceFacade);
    }

    /**
     * Test get etat formalite.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    public void testGetEtatFormalite() throws Exception {
        final EtatFormaliteReponse actual = this.urssafClient.getEtatFormalite(null);

        assertNull(actual);
    }

    /**
     * Test mise A jour formalite messages for bom operations.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    public void testMiseAJourFormaliteMessagesForBomOperations() throws Exception {

        final String recordId = "HXXXXXXXXXXX";
        final String message = "Réception de la liasse GU dans le CFE compétent";

        final OperationBOMSurLiasse operationBOMSurLiasse = new OperationBOMSurLiasse();
        operationBOMSurLiasse.setCodeOperateur("CFE");
        operationBOMSurLiasse.setEtatFormalite("B_INTEGRE");
        operationBOMSurLiasse.setCommentaireTraitement(message);

        final IdentifiantAppelant identifiantAppelant = new IdentifiantAppelant();
        identifiantAppelant.setCodeEdi("U9301");
        identifiantAppelant.setMotDePasse("gbwBEL5");

        final MiseAJourFormaliteRequete request = new MiseAJourFormaliteRequete();
        request.setNumeroLiasse(recordId);
        request.setOperationBOMSurLiasse(operationBOMSurLiasse);
        request.setIdentifiantAppelant(identifiantAppelant);

        final MiseAJourFormaliteReponse actual = this.urssafClient.miseAJourFormalite(request);

        verify(this.referenceServiceFacade).postMessage(recordId,
                "*** Opération BOM ***\n - Code opérateur : CFE\n - Etat formalité : B_INTEGRE\n - Commentaire traitement : Réception de la liasse GU dans le CFE compétent", "Urssaf");
    }

    /**
     * Test mise A jour formalite messages for donnees liasse BOM.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    public void testMiseAJourFormaliteMessagesForDonneesLiasseBOM() throws Exception {

        final String recordId = "HXXXXXXXXXXX";
        final String message = "Réception de la liasse GU dans le CFE compétent";

        final DonneesLiasseBOM donneesLiasseBOM = new DonneesLiasseBOM();
        donneesLiasseBOM.setNumeroSiren("479 766 842");
        donneesLiasseBOM.setReferenceDossier("ref dossier");
        donneesLiasseBOM.setReferenceLiasse("ref liasse");

        final DonneesCfeIncompetent donneesCfeIncompetent = new DonneesCfeIncompetent();
        donneesLiasseBOM.setDonneesCfeIncompetent(donneesCfeIncompetent);
        donneesCfeIncompetent.setCodeEdiCfeCompetent("code edi cfe");
        donneesCfeIncompetent.setCommentaire(message);

        final IdentifiantAppelant identifiantAppelant = new IdentifiantAppelant();
        identifiantAppelant.setCodeEdi("U9301");
        identifiantAppelant.setMotDePasse("gbwBEL5");

        final MiseAJourFormaliteRequete request = new MiseAJourFormaliteRequete();
        request.setNumeroLiasse(recordId);
        request.setDonneesLiasseBOM(donneesLiasseBOM);
        request.setIdentifiantAppelant(identifiantAppelant);

        final MiseAJourFormaliteReponse actual = this.urssafClient.miseAJourFormalite(request);

        verify(this.referenceServiceFacade).postMessage(recordId,
                "*** Données Liasse ***\n - Numéro SIREN : 479 766 842\n - Référence dossier : ref dossier\n - Référence liasse : ref liasse\n - CFE incompétent :\n    - Code EDI CFE compétent : code edi cfe\n    - Commentaire : Réception de la liasse GU dans le CFE compétent",
                "Urssaf");
    }

    /**
     * Test mise A jour formalite messages for donnees liasse BOM null.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    public void testMiseAJourFormaliteMessagesForDonneesLiasseBOMNull() throws Exception {

        final String recordId = "HXXXXXXXXXXX";

        final DonneesLiasseBOM donneesLiasseBOM = new DonneesLiasseBOM();

        final IdentifiantAppelant identifiantAppelant = new IdentifiantAppelant();
        identifiantAppelant.setCodeEdi("U9301");
        identifiantAppelant.setMotDePasse("gbwBEL5");

        final MiseAJourFormaliteRequete request = new MiseAJourFormaliteRequete();
        request.setNumeroLiasse(recordId);
        request.setDonneesLiasseBOM(donneesLiasseBOM);
        request.setIdentifiantAppelant(identifiantAppelant);

        final MiseAJourFormaliteReponse actual = this.urssafClient.miseAJourFormalite(request);

        verify(this.referenceServiceFacade).postMessage(recordId,
                "*** Données Liasse ***\n - Numéro SIREN : null\n - Référence dossier : null\n - Référence liasse : null\n - CFE incompétent :\n    - Code EDI CFE compétent : \n    - Commentaire : ",
                "Urssaf");
    }

    /**
     * Test mise A jour formalite messages for actions pieces manquantes.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    public void testMiseAJourFormaliteMessagesForActionsPiecesManquantes() throws Exception {

        final String recordId = "HXXXXXXXXXXX";
        final String message = "Réception de la liasse GU dans le CFE compétent";

        final ActionPieceManquante actionPieceManquante = new ActionPieceManquante();
        actionPieceManquante.setAction("action");

        final PieceManquante pieceManquante = new PieceManquante();
        actionPieceManquante.setPieceManquante(pieceManquante);
        pieceManquante.setFamille("famille");
        pieceManquante.setNumero("numero");
        pieceManquante.setCommentaire(message);

        final IdentifiantAppelant identifiantAppelant = new IdentifiantAppelant();
        identifiantAppelant.setCodeEdi("U9301");
        identifiantAppelant.setMotDePasse("gbwBEL5");

        final MiseAJourFormaliteRequete request = new MiseAJourFormaliteRequete();
        request.setNumeroLiasse(recordId);
        request.setIdentifiantAppelant(identifiantAppelant);
        final List<ActionPieceManquante> actionPieceManquantes = request.getActionsPiecesManquantes();
        actionPieceManquantes.add(actionPieceManquante);

        final MiseAJourFormaliteReponse actual = this.urssafClient.miseAJourFormalite(request);

        verify(this.referenceServiceFacade).postMessage(recordId,
                "*** Action Pieces Manquantes ***\n - Famille : famille\n - Numero : numero\n - Demande : null\n - Reception : null\n - Commentaire : Réception de la liasse GU dans le CFE compétent",
                "Urssaf");
    }

    /**
     * Test mise A jour formalite messages for actions pieces manquantes null.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    public void testMiseAJourFormaliteMessagesForActionsPiecesManquantesNull() throws Exception {

        final String recordId = "HXXXXXXXXXXX";

        final ActionPieceManquante actionPieceManquante = new ActionPieceManquante();
        actionPieceManquante.setAction("action");

        final IdentifiantAppelant identifiantAppelant = new IdentifiantAppelant();
        identifiantAppelant.setCodeEdi("U9301");
        identifiantAppelant.setMotDePasse("gbwBEL5");

        final MiseAJourFormaliteRequete request = new MiseAJourFormaliteRequete();
        request.setNumeroLiasse(recordId);
        request.setIdentifiantAppelant(identifiantAppelant);
        final List<ActionPieceManquante> actionPieceManquantes = request.getActionsPiecesManquantes();
        actionPieceManquantes.add(actionPieceManquante);

        final MiseAJourFormaliteReponse actual = this.urssafClient.miseAJourFormalite(request);

        verify(this.referenceServiceFacade, Mockito.never()).postMessage(any(String.class), any(String.class), any(String.class));
    }
}
