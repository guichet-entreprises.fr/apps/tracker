package fr.ge.tracker.ws.v2.service.impl;

import java.util.List;

import fr.ge.common.utils.bean.search.SearchQueryOrder;
import fr.ge.common.utils.bean.search.SearchResult;
import fr.ge.tracker.ws.v2.model.KeyInfo;
import fr.ge.tracker.ws.v2.service.IKeyReadService;
import fr.ge.tracker.ws.v2.service.IKeyWriteService;

/**
 *
 * @author Christian Cougourdan
 */
public class KeyServiceImpl implements IKeyWriteService, IKeyReadService {

    @Override
    public KeyInfo load(final String key) {
        return null;
    }

    @Override
    public String create(final String user) {
        return null;
    }

    @Override
    public boolean addSubKey(final String parentKey, final String subKey, final String user) {
        return false;
    }

    @Override
    public SearchResult<KeyInfo> search(final long startIndex, final long maxResults, final List<SearchQueryOrder> orders) {
        return null;
    }

}
