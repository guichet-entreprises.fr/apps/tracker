package fr.ge.tracker.ws.v2.service.impl;

import java.util.List;

import javax.ws.rs.Path;

import fr.ge.common.utils.bean.search.SearchQueryOrder;
import fr.ge.common.utils.bean.search.SearchResult;
import fr.ge.tracker.ws.v2.model.Message;
import fr.ge.tracker.ws.v2.service.IMessageReadService;
import fr.ge.tracker.ws.v2.service.IMessageWriteService;

/**
 *
 * @author Christian Cougourdan
 */
@Path("/v2")
public class MessageServiceImpl implements IMessageReadService, IMessageWriteService {

    @Override
    public void addMessage(final String key, final String message, final String user) {
    }

    @Override
    public SearchResult<Message> searchMessage(final String key, final long startIndex, final long maxResults, final List<SearchQueryOrder> orders) {
        return null;
    }

}
